from mod_pywebsocket import msgutil
from mod_pywebsocket.util import DeflateSocket
import re, socket, select
import common

SERVER_NAME = socket.getfqdn() or 'localhost'
ALLOWED_CMDS = (
	'init', 'startgame', 'move', 'feedback', 'setoption', 'getmove', 'forcemove', 'stop', 'endgame', 'quit',
	'connect', 'bestmove', 'error', 'concede', 'info',
)

def rejoin(cmd):
	return ' '.join(cmd)

def clean(x):
	if x[0] == '\x00':
		return x[1:]
	return x

def web_socket_do_extra_handshake(request):
	pass

def web_socket_transfer_data(request):
	cmd = msgutil.receive_message(request)
	print cmd
	if cmd != 'INIT':
		return

	server = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
	server.bind((SERVER_NAME, 0))
	server.listen(2)
	port = server.getsockname()[1]

	msgutil.send_message(request, '_INIT_OK %s:%d' % (SERVER_NAME, port))
	browser = request._request_handler.connection
	inn = [server, browser]
	client = None

	# serve
	while True:
		rdy, _, _ = select.select(inn, [], [])
		for n in rdy:
			if n is browser: # websocket
				cmd = msgutil.receive_message(request)
				if cmd is None: #close
					print 'BROWSER CLOSE'
					server.close()
					return
				else:
					cmd = cmd.split()
					print 'B', cmd
					cmd[0] = clean(cmd[0])
					if cmd[0] in ALLOWED_CMDS and client is not None:
						client.msg(rejoin(cmd))
			elif n is server: # new connection
				new, _ = n.accept()
				if client: # only one client at a time
					print "SERVER REJECTED (ALREADY CONNECTED)"
					new.close()
					continue
				print "SERVER CONNECTED"
				client = new
				inn.append(client)
				msgutil.send_message(request, '_SER_CON')
			else: # client
				try:
					cmd = n.parse()
					print 'S', cmd
					cmd[0] = clean(cmd[0])
					if cmd[0] in ALLOWED_CMDS:
						msgutil.send_message(request, rejoin(cmd))
				except IOError:
					print "SERVER DISCONNECTED"
					n.close()
					inn.remove(n)
					client = None
					msgutil.send_message(request, '_SER_DIS')

