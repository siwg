import socket

DELIM = '\x0A'

class MyBuf(object):
	buf = {}

	def __get__(self, obj, type=None):
		return self.buf.get(obj,'')

	def __set__(self, obj, val):
		self.buf[obj] = val

socket.socket.buf = MyBuf()

def parse(s, d=DELIM):
	while True:
		while s.buf.find(d) == -1:
			new = s.recv(4)
			if len(new) == 0:
				raise IOError
			s.buf += new
		cmd, s.buf = s.buf.split(d, 1)
		cmd = cmd.split()
		if len(cmd) > 0:
			return cmd

def msg(s, msg, d=DELIM):
	s.sendall(str(msg) + d)

socket.socket.parse = parse
socket.socket.msg = msg
