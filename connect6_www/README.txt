Aby uruchomić:
	- ustawić WS_HOST w plikach wsrun.sh i www/ws.js na nazwę/adres maszyny, na której będzie odpalony serwer Websocketów
	- na tejże maszynie odpalić wsrun.sh
	- udostępnić gdzieś (jako stronę) zawartość katalogu www

Testowanie:
dummyserver.py może służyć jako symulator sędziego:
./dummyserver.py ADRES PORT
Gdzie adres i port należy wziąć ze strony

Każda wpisana w dummyserver komenda będzie przesłana do strony. Wyświetlany zaś będzie output od interfejsu www.

