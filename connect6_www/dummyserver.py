#!/usr/bin/env python
import re, socket, select, sys
import ws.common

domain, port = sys.argv[1:3]
port = int(port)

client = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
client.connect((domain, port))
inn = [client, sys.stdin]

try:
    while True:
	    rdy, _, _ = select.select(inn, [], [])
	    for n in rdy:
		    if n is client: # engine
			    cmd = n.parse()
			    print cmd
		    else: # input
			    cmd = n.readline()
			    client.msg(cmd)
finally:
    client.close()

