var LETTER_BASE = 'a'.charCodeAt(0);

function field_click() {
	if (!GAME.in_progress() || !GAME.my_move() || !$(this).hasClass('empty'))
		return false;

	var move = $(this).attr('id').substr(2);
	GAME.make_my_move(move);
	return false;
}

function surrender_click() {
	if (!GAME.in_progress())
		return false;

	WS_HANDLER.send('concede');
	return false;
}

function generate_board() {
	var plansza = $("#plansza");
	var div = $('<div class="field empty">');

	for(i = 0; i < 19; i++)
		for(j = 0; j < 19; j++)
			plansza.append(div.clone().attr('id', 'f-'+ind2pos(i,j)));
	$(".field").click(field_click);
	$("#surrender").click(surrender_click);
}

function clean_board() {
	$(".field").removeClass('black white').addClass('empty');
}

function newFilledArray(len, val) {
    var rv = new Array(len);
    while (--len >= 0)
        rv[len] = val;
    return rv;
}

function out_of_time() {
	stopAllTimers();
	GAME.status = (this.white_to_move ? 'black' : 'white');
	GAME.display();
	$("#status").text('Koniec czasu. ' + $("#status").text());
	$("#menu").hide();
}

var TEXTS = {
	in_progress: 'Gra trwa.',
	draw: 'Remis.',
	white: 'Białe wygrały.',
	black: 'Czarne wygrały.'
}

var GAME = {
	board: null,
	empty_board: null,
	side: null, // black / white
	white_to_move: false,
	status: null, // in_progress, white, black, draw
	to_send: '',
	m_count: 0,

	in_progress: function() {
		return this.status == 'in_progress';
	},

	my_move: function() {
		return (this.side == 'white') == this.white_to_move;
	},

	display: function() {
		var text = TEXTS[this.status];
		if (this.in_progress()) {
			text += ' Ruch ' + (this.white_to_move ? 'białych' : 'czarnych') + '.';
		}
		$("#status").text(text);
		$("#color").text('Jesteś graczem ' + (this.side == 'white' ? 'białym' : 'czarnym') + '.');
	},

	board_display: function() {
		// wyswietlenie pol na planszy
		for (var i=0; i<this.board.length; i++)
			for (var j=0; j<this.board[i].length; j++) {
				var f = ind2pos(i, j);
				$('#f-'+f).removeClass('black white empty').addClass(b2side(this.board[i][j]));
			}
	},

	is_empty_board: function() {
		if(this.board == null) return false;
			for(i = 0; i < 19; i++)
				for(j = 0; j < 19; j++)
					if(this.board[i][j] != 'E') return false;
		return true;
	},

	check_empty_board: function() {
		this.empty_board = this.is_empty_board();
	},

	move: function(side, moves) {
		var b = side2b(side);
		for (var i in moves) {
			var move = moves[i];
			$('#f-'+move).removeClass('black white empty').addClass(side);
			var inds = pos2ind(move);
			this.board[inds[0]][inds[1]] = b;
			this.empty_board = false;
		}
	},

	make_my_move: function(move) {
		var emp = this.empty_board;
		this.move(this.side, [move]);
		this.to_send += move;
		this.m_count++;
		if ((!emp && this.m_count == 2) || (emp && this.m_count == 1)) {
			this.white_to_move = !this.white_to_move;
			WS_HANDLER.send('bestmove ' + this.to_send);
			this.display();
			stopTimer('r');
			stopTimer('g');
		}
	}
}


function side2b(side) {
	return side.charAt(0).toUpperCase();
}

function b2side(b) {
	return {B: 'black', W: 'white', E: 'empty'}[b];
}

function parse_pos(pos) {
	if (pos.length <= 3)
		return [pos]
	return pos.replace(/(\d)([a-z])/ig, '$1 $2').split(' ');
}

function pos2ind(pos) {
	return [pos.charCodeAt(0)-LETTER_BASE, parseInt(pos.substr(1))]
}
function ind2pos(i, j) {
	return String.fromCharCode(i+LETTER_BASE) + j;
}



/* Zegarek */
var TIMES = {r: {}, g: {}};

function pad(s, len) {
	if (len == undefined) len = 2
	if (String(s).length < len)
		return "0"+s;
	return s
}
function startTimer(place, secs, callback) {
	var s = secs % 60;
	var m = (secs-s) / 60;
	TIMES[place].sec = s;
	TIMES[place].min = m;
	TIMES[place].callback = callback;
	TIMES[place].interval = setInterval("updateTimer('" + place + "')", 1000);
	$("#"+place+"_m").text(m);
	$("#"+place+"_s").text(pad(s));
}

function stopTimer(place) {
	clearInterval(TIMES[place].interval);
}
function stopAllTimers() {
	stopTimer('r');
	stopTimer('g');
}

function updateTimer(place) {
	TIMES[place].sec--;
	if (TIMES[place].sec == -1)
	{
		TIMES[place].sec = 59;
		TIMES[place].min --;
		if (TIMES[place].min == -1)
		{
			TIMES[place].min = 0;
			TIMES[place].sec = 0;
			clearInterval(TIMES[place].interval);
			if (typeof TIMES[place].callback !== 'undefined') TIMES[place].callback();
		}
		$("#"+place+"_m").text(TIMES[place].min);
	}
	$("#"+place+"_s").text(pad(TIMES[place].sec));
}

