/* WebSocket */

WS_HOST = 'students';

WS_HANDLER = {
    active: false,
    ws: 0,
	adr: '',

    init: function(ws) {
    	this.ws = ws;
    },

    receive: function(msg){
        var cmd = msg.split(' ');
        if (!this.active && cmd[0].charAt(0) != '_')
            return;
        var handler = 'do_' + cmd[0];
        if (typeof this[handler] != 'undefined')
            this[handler](cmd);
    },

	send: function(data) {
		this.ws.send(data);
	},

	waiting: function() {
		$("#odp").html('Adres tego klienta: http://' + this.adr + '<br>Czekam na połączenie.');
	},

    // administracyjne
    do__INIT_OK: function(cmd){
        this.adr = cmd[1];
		this.waiting();
    },

    do__SER_CON: function() {
        this.active = true;
        $("#odp").text('Sędzia podłączony.');
        clean_board();
    },

    do__SER_DIS: function() {
        this.active = false;
		this.waiting();
        $("#odp").html('Sędzia rozłączony. ' + $("#odp").html());
		if (GAME.in_progress()) { // nagle zerwanie polaczenia
			$("#status").text('Nastąpiło nagłe zerwanie połączenia z sędzią.');
		}
		GAME.status = null;
		$("#menu").hide();
    },

    // polecenia od GUI
    do_init: function() {
		this.ws.send('connect WWW');
    },

    do_startgame: function(cmd) {
		GAME.board = parse_board(cmd[1]);
		if (GAME.board == null) {
			this.send('error bledny opis planszy');
			$("#status").text('Błędna plansza.');
			return;
		}

		GAME.side = cmd[2];
		GAME.status = 'in_progress';
		GAME.check_empty_board();
		GAME.white_to_move = (GAME.side != 'white'); // aby nie moc wykonac ruchu
		// kwestia zegara
		$("#r_m, #g_m").text('0');
		$("#r_s, #g_s").text('00');
		GAME.board_display();
		GAME.display();
		$("#menu").show();
    },

	do_getmove: function(cmd) {
		GAME.white_to_move = (cmd[1] == 'white');
		GAME.to_send = '';
		GAME.m_count = 0;
		// kwestia zegara
		if (cmd[2] != 'inf') {
			stopTimer('r');
			startTimer('r', parseInt(cmd[2]), out_of_time);
		}
		else $('#r_m, #r_s').text('--');
		if (cmd[3] != 'inf') {
			stopTimer('g');
			startTimer('g', parseInt(cmd[3]), out_of_time);
		}
		else $('#g_m, #g_s').text('--');
		GAME.display();
	},

	do_move: function(cmd) {
		var moves = parse_pos(cmd[2]);
		GAME.move(cmd[1], moves);
	},

	do_endgame: function(cmd) {
		stopAllTimers();
		GAME.status = cmd[1];
		GAME.display();
		$("#menu").hide();
	},

	do_forcemove: function() {
		$("#status").text('Szybko podaj ruch!').css('color', '#a00');
		setTimeout(function(){
			$("#status").css('color', '#000');
		}, 500);
	},

	do_quit: function() {
		GAME.status = null;
		$("#menu").hide();
	}
};

$(document).ready(function(){
	var ws = 0;
	generate_board();

	/* websocket connection */
	if ("WebSocket" in window){
		ws = new WebSocket("ws://" + WS_HOST + ":9998/connect6");
		WS_HANDLER.init(ws);

		ws.onopen = function() {
			ws.send("INIT");
		};
		ws.onmessage = function(evt){
			WS_HANDLER.receive(evt.data);
		};
		ws.onclose = function(){
			$("#odp").html("Połączenie odrzucone.");
		};
		window.onbeforeunload = function() {
			ws.close();
		};
	}
	else {
		$("#odp").html("Nie masz WebSocketów! Uruchom stronę w Chromie.");
	};
});
