
const MAX_BOARD = 19;

function parse_board(str) {
    var boa = new Array();
    for(var ll = 0; ll < 19; ll++)
    {
	boa[ll] = new Array(19);
    }

    str = str.replace(/[/][/].*/g,"").replace(/\s/g,"").replace(/[/][*].+?[*][/]/g,"")
    if(str.charAt(0) != '{') { return null; }; // error
    if(str.charAt(str.length-1) != '}') { return null; }; // error
    str = str.substr(1,str.length-2);
    var i = 0;
    var tab = str.split("|");
    if(tab.length != MAX_BOARD) { return null; } // error
    for(var l1 = 0; l1 < tab.length; l1++)
    {
	var line = tab[l1].split(",");
	var el = 0;
	for(var l2 = 0; l2 < line.length; l2++)
	{
	    var bas;
	    var ile;
	    if(line[l2].charAt(0) == 'E')
	    {
		if(line[l2].length > 1)
			ile = parseInt(line[l2].substr(1, line[l2].length-1));
		else ile = 1;
		for(var l3 = el; l3 < el + ile; l3++)
		{
		    if(l3 >= MAX_BOARD) { return null; } // error
		    boa[l1][l3] = 'E';
		}
		el += ile;
	    }
	    else if(line[l2].charAt(0) == 'B')
	    {
		if(line[l2].length > 1)
			ile = parseInt(line[l2].substr(1, line[l2].length-1));
		else ile = 1;
		for(var l3 = el; l3 < el + ile; l3++)
		{
		    if(l3 >= MAX_BOARD) { return null; } // error
		    boa[l1][l3] = 'B';
		}
		el += ile;
	    }
	    else if(line[l2].charAt(0) == 'W')
	    {
		if(line[l2].length > 1)
			ile = parseInt(line[l2].substr(1, line[l2].length-1));
		else ile = 1;
		for(var l3 = el; l3 < el + ile; l3++)
		{
		    if(l3 >= MAX_BOARD) { return null; } // error
		    boa[l1][l3] = 'W';
		}
		el += ile;
	    }
	    else { return null; } // error

	}
	if(el != MAX_BOARD) { return null; } // error
    }
    return boa;
}

/*
var myBoa = generate_board("{E19|E19|E19|E19|E19|E19|E10,W,B2,E6|E10,W2,E7|E6,B1,E2,W4,E6|E7,W3,B,E8|E8,W,B2,E8|E8,B,W,E9|E5,W,B4,W,E8|E6,W,B,E11|E6,B,E5,B,E6|E19|E19|E19|E19}");
var myBoa = generate_board("{E19|E19|E19|E19|E19|E19|E19|E19|E19|E19|E19|E19|E19|E19|E19|E19|E19|E19|E19}");
for(ll = 0; ll < 19; ll++)
{
document.write("<p>Wiersz (" + ll + "): " + myBoa[ll] + "</p>");
}
*/
