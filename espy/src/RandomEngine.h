#include<pthread.h>
#include<iostream>
#include<string>

#include "CPEventListener.h"
#include "ConnectSix.h"

/*****
 * Najprostsza metoda implementacji własnego silnika to implementacja metody
 *
 *  - thinking
 *
 * Kontrola czasu zaimplementowana jest w klasie Engine.
 *
 * Metoda thinking liczy tak długo, aż zmienna pause_search przyjmie
 *   wartość true.
 * 
 * Aktualny wynik szukania zapisany jest w zmiennej result
 *
 * Wybrany ruch (pv[0]) wysyłany jest automatycznie. Ustawienie pause_search
 *   na true zazwyczaj oznacza, że ruch właśnie został wysłany.
 * 
 * 
 *****/

#include<stdlib.h>
#include "Engine.h"

template <class G>
class RandomEngine : public Engine<G> {

	static const int infty = 30000;

	void init() {
		Engine<G>::init();

		cout << "connect Random Engine " << Settings::version() << ", playing " << G::name() << endl;
	}

	void think(G board, typename Engine<G>::SearchResult &result) {
		/*
		 * Ten silnik wybiera losowy ruch, a potem udaje, że myśli :).
		 */
		
		Log::debug("think() started");

		result.lock.grab();
		
		board.prepare_moves();
		vector<typename G::move> moves;

		while(board.has_next_move()) {
			moves.push_back(board.get_next_move());
		}

		int rnd = random() % moves.size();

		result.in_progress = false;
		result.resign = false;
		result.nominal_depth = 1;
		result.score_lower_bound = -infty;
		result.score_upper_bound = infty;
		result.pv.clear();
		result.pv.push_back(moves[rnd]);
		result.elapsed_time = 0;
		result.nodes_used = 1;

		result.lock.release();
		
		/*
		 * Myślimy o niebieskich migdałach.
		 */

		while (!Engine<G>::pause_search) {
		}

		Log::debug("think() ended");
	}

};
