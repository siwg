/**
 * \file
 *
 * Implementation of Domineering game.
 *
 * This file is part of espy, the game solver.
 *
 * \author Andrzej Nag�rko.
 *
 */

#ifndef __Domineering__
#define __Domineering__

#include "Game.h"
#include "Settings.h"

class Domineering : public Game {
public:
	const static int max_depth = Settings::maximum_game_length;

	const static int empty = 0;
	const static int filled = 1;
	
	const static int N = 4;

	struct board {
		int sq[N][N+2];
	} bd;

	int ply;
	int mv[max_depth];
	int played[max_depth];

public:
	typedef int move;
	
	Domineering() {
		clear();
	};

	void prepare_moves() {
		mv[ply] = 0;
	};

	bool has_next_move() {
		if (!wtm()) {
			while (bd.sq[mv[ply] % N][mv[ply] / N] != empty ||
			       bd.sq[(mv[ply] % N) + 1][mv[ply]/N] != empty) {
				mv[ply]++;
				if ((mv[ply] % N) == N - 1) mv[ply]++;
			}
			return mv[ply] < N * N - 1;
		} else {
			while (bd.sq[mv[ply] % N][mv[ply] / N] != empty ||
			       bd.sq[(mv[ply] % N)][mv[ply]/N + 1] != empty) {
				mv[ply]++;
			}
			return mv[ply] < N * (N - 1);
		}
	}
	move get_next_move() {
		if (!wtm()) {
			while (bd.sq[mv[ply] % N][mv[ply] / N] != empty ||
			       bd.sq[(mv[ply] % N) + 1][mv[ply]/N] != empty) {
				mv[ply]++;
				if ((mv[ply] % N) == N - 1) mv[ply]++;
			}
		} else {
			while (bd.sq[mv[ply] % N][mv[ply] / N] != empty ||
			       bd.sq[(mv[ply] % N)][mv[ply]/N + 1] != empty) {
				mv[ply]++;
			}
		}
		return mv[ply]++;
	}

	void clear() {
		ply = 0;
		for (int i = 0; i < N+2; i++)
			for (int j = 0; j < N; j++)
				bd.sq[i][j] = empty;
		game_state(in_progress);
	}

	void make_move(const move m) {
		bd.sq[m % N][m / N] = filled;
		if (wtm()) {
			bd.sq[m % N][m / N + 1] = filled;
		} else {
			bd.sq[(m % N) + 1][m / N] = filled;
		}

		played[ply] = m;
		++ply;

		/* set game state */

		if (has_next_move()) {
			game_state(in_progress);
		} else {
			if (wtm()) {
				game_state(black_wins);
			} else {
				game_state(white_wins);
			}
		}
	}

	void retract_move() {
		--ply;
		move m = played[ply];
		bd.sq[m % N][m / N] = empty;
		if (wtm()) {
			bd.sq[m % N][m / N + 1] = empty;
		} else {
			bd.sq[(m % N) + 1][m / N] = empty;
		}
	}

	inline int wtm() const {
		return (ply & 1) == 0;
	}

	static string name() {
		return "Domineering";
	}

	void print_board() {
		for (int i = 0; i < N; i++) {
			cout << "|";
			for (int j = 0; j < N; j++) {
				switch (bd.sq[i][j]) {
				case empty: cout << " "; break;
				case filled: cout << "#"; break;
				}
			}
			cout << "|\n";
		}
	}
};

#endif
