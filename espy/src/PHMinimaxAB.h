/* Connect six game logic.
 *
 * Author: Przemyslaw Horban (ph262940@students.mimuw.edu.pl)
 */

#ifndef PH_MINIMAX_AB_H
#define PH_MINIMAX_AB_H

#include <stack>
#include <vector>
#include <algorithm>

using namespace std;

#include "PHConnectSix.h"

// function alphabeta(node, depth, alpha, beta, Player)         
//     if  depth = 0 or node is a terminal node
//         return the heuristic value of node
//     if  Player = MaxPlayer
//         for each child of node
//             alpha := max(alpha, alphabeta(child, depth-1, alpha, beta, not(Player) ))     
//             if beta <= alpha
//                 break                             (* Beta cut-off *)
//         return alpha
//     else
//         for each child of node
//             beta := min(beta, alphabeta(child, depth-1, alpha, beta, not(Player) ))     
//             if beta <= alpha
//                 break                             (* Alpha cut-off *)
//         return beta 
// (* Initial call *)
// alphabeta(origin, depth, -infinity, +infinity, MaxPlayer)


template<typename EvalGame>
class PHMinimaxAB {
  private:
    static const int infinity = 1000000000;
  public:
    PHMinimaxAB() {}

    vector<move> seachBestMoves(EvalGame *conSix, int depth) const {
      vector<move> moves;

      conSix->prepare_moves();
      while(conSix->has_next_move()) {
        move chldMv = conSix->get_next_move();
        conSix->make_move(chldMv);
        chldMv.value = alphabeta(conSix, depth, -infinity, +infinity,
                                 conSix->wtm() ? white : black);
        conSix->retract_move();
        moves.push_back(chldMv);
      }

      reverse(moves.begin(), moves.end());

      return moves;
    }

    move seachBestMove(EvalGame *conSix, int depth) const {
      vector<move> moves = seachBestMoves(conSix, depth);

      move best;
      best.value = no_value;

      for(int i = 0; i < (int)moves.size(); i++)
        if(best.value == no_value ||
           (conSix->wtm() && best.value < moves[i].value) ||
           (!conSix->wtm() && best.value > moves[i].value))
          best = moves[i];
      return best;
    }

  private:
    int alphabeta(EvalGame *conSix, int depth, int alpha, int beta,
                  color player) const{
      if(depth == 0 || conSix->game_state() == EvalGame::draw)
        return conSix->evaluation();
      if(conSix->game_state() == EvalGame::white_wins)
        return infinity;
      else if(conSix->game_state() == EvalGame::black_wins)
        return -infinity;

      if(player == white) { // MaxPlayer
        conSix->prepare_moves();
        while(conSix->has_next_move()) {
          move chldMv = conSix->get_next_move();
          conSix->make_move(chldMv);
          alpha = max(alpha, alphabeta(conSix, depth-1, alpha, beta, black));
          conSix->retract_move();
          if(beta <= alpha)
            break;                    //         (* Beta cut-off *)
        }
        return alpha;
      }
      else {
        conSix->prepare_moves();
        while(conSix->has_next_move()) {
          move chldMv = conSix->get_next_move();
          conSix->make_move(chldMv);
          beta = min(beta, alphabeta(conSix, depth-1, alpha, beta, white));
          conSix->retract_move();
          if(beta <= alpha)
            break;                //             (* Alpha cut-off *)
        }
        return beta;
      }
    }
};


#endif  // PH_MINIMAX_AB_H
