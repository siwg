#include<pthread.h>
#include<iostream>
#include<string>
#include<stdlib.h>

#include "CPEventListener.h"
#include "ConnectSix.h"
#include "Engine.h"

template <class G>
class GreedyEngine : public Engine<G> {

	static const int infty = 30000;

	void init() {
		Engine<G>::init();

		cout << "connect Greedy Engine " << Settings::version() << ", playing " << G::name() << endl;
	}

	void think(G board, typename Engine<G>::SearchResult &result) {
		

		result.lock.grab();
		
		board.prepare_moves();
		vector<typename G::move> moves;

		while(board.has_next_move()) {
			moves.push_back(board.get_next_move());
		}

		int rnd = random() % moves.size();

		result.in_progress = false;
		result.resign = false;
		result.nominal_depth = 1;
		result.score_lower_bound = -infty;
		result.score_upper_bound = infty;
		result.pv.clear();
		result.pv.push_back(moves[rnd]);
		result.elapsed_time = 0;
		result.nodes_used = 1;

		result.lock.release();

		int wtm = board.wtm();

		struct dir { int dx; int dy; } dirs[8] = { {1,1}, {1,0}, {1,-1}, {0,-1}, {-1,-1}, {-1,0}, {-1,1}, {0,1} };

		int sc[ConnectSix::BOARD_SIZE][ConnectSix::BOARD_SIZE];

		int max = -1000000, max2 = max;
		
//		board.print_board();

		vector<ConnectSix::field> bmvs;

		for (int x = 0; x < ConnectSix::BOARD_SIZE; x++) {
			for (int y = 0; y < ConnectSix::BOARD_SIZE; y++) {
				int score = 0;

				if (board.bd.sq[x][y] != ConnectSix::EMPTY) { sc[x][y] = -1; continue; }

				for (int d = 0; d < 8; d++) {
					int nw = 0, nb = 0, k;

					for (k = 1; k < 6; k++) {
						if (x + dirs[d].dx * k < 0 || x + dirs[d].dx * k >= ConnectSix::BOARD_SIZE) { break; }
						if (y + dirs[d].dy * k < 0 || y + dirs[d].dy * k >= ConnectSix::BOARD_SIZE) { break; }

						switch (board.bd.sq[x + dirs[d].dx * k][y + dirs[d].dy * k]) {
						case ConnectSix::WHITE:
							nw++;
							if (wtm) {
								score += 1 << nw; 
							} else {
								score += 1 << nw; 
							}
							break;
						case ConnectSix::BLACK:
							nb++;
							if (wtm) {
								score += 1 << nb; 
						       
							} else {
								score += 1 < nb;
							}
							break;
						}
						if (nw > 0 && nb > 0) { break; }
					}

					if ((nw >= 4 && nb == 0 && k == 6) ||
					    (nw == 0 && nb >= 4 && k == 6)) {
						score = 10000;
					}

				}

				sc[x][y] = score;
				if (score > max2 || score > max) {
					if (max > max2) { max2 = score; } else { max = score; }

//					cout << "new max " << x << " "<< y << " max max2 " << max << " " << max2 <<  endl;

					bmvs.push_back(make_pair(x,y));
				}
					
			}
		}

		
		ConnectSix::mmove mm;
		ConnectSix::field m1 = bmvs[bmvs.size() - 1];
		ConnectSix::field m2;
		if (bmvs.size() > 1) {
			m2 = bmvs[bmvs.size() - 2];
		} else {
			m2 = m1;
		}
		if (board.move_number == 0) {
			m1 = make_pair(8,8);
			m2 = m1;
		}

		mm.first = m1; mm.second = m2;

		result.lock.grab();
		result.pv.clear();
		result.pv.push_back(board.move_to_int(mm));
		result.lock.release();

		while (!Engine<G>::pause_search) {
		}

		Log::debug("think() ended");
	}

};
