/**
 * \file
 *
 * Prototype of search algorithms implementations.
 *
 */

#ifndef __Search__
#define __Search__

#include "Game.h"

template <class G>
class Search {

public:
	void position(G &);

	void search();
	void search_single_node();

	int result();
	int nodes();
	int depth();
	int depth(int);
};

#endif /* __Search__ */
