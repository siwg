/*
 * main.cc
 *
 *  Created on: 2012-03-19
 *      Author: elshize
 */

#include <iostream>
#include "CPCommandParser.h"
#include "CPDefaultEventListener.h"

using namespace std;

int main() {
    CPDefaultEventListener listener;
    CPCommandParser parser(cin, listener);
    while (true) {
        string line = parser.nextCommand();
        try {
            parser.parse(line);
        } catch (int error) {
            if (error == 2)
              cout << "Unknown command." << endl;
            else
              cout << "Command corrupted." << endl;
        }
    }
    return 0;
}
