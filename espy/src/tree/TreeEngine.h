#include <iostream>
#include <algorithm>
#include <set>

#include "CPEventListener.h"
#include "Game.h"
#include "ConnectSix.h"
#include "EngineJacek.h"
#include "MinimaxAB.h"


const int ConnectSix::BOARD_SIZE;

template <class G>
class TreeEngine : public Engine<G> {

    static const int MM_DEPTH = 6;
    static const int infty = 500000;

    void init() {
        Engine<G>::init();
        cout << "connect Tree2 " << RAND_MAX << ", playing " << G::name() << endl;
        srand(time(NULL));
    }

    void think(G& board, typename Engine<G>::SearchResult &result, int time) {

        result.in_progress = false;
        result.resign = false;
        result.nominal_depth = 1;
        result.pv.clear();
        result.elapsed_time = 0;
        result.nodes_used = 1;

        ConnectSix::field f;

        if (board.move_number == 0) {
            f = make_pair(9, 9);
            result.pv.push_back(board.move_to_int(make_pair(f, f)));
            Log::debug("think() ended - first move");
            return;
        }

        board.prepare_moves();

        int d = MM_DEPTH;
        if (board.move_number == 2 || time <= 120) d = 5;
        if (board.move_number == 1 || time <= 60) d = 4;


        MinimaxAB<G> minmax;
        minmax.depth(d);
        minmax.position(board);
        cout << "DEPTH " << d << " REASON " << board.reasonable[board.move_number].size() << " MOVES TO SEARCH: " << board.selected[board.move_number].size() << endl;
        minmax.search();
        int score = minmax.result();
        result.pv.push_back(minmax.bestmove());
        cout << "SEARCH NODES: " << minmax.nodes() << " WITH SCORE: " << score << endl;

        Log::debug("think() ended");
    }

};
