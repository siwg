/**
 * \file
 *
 * Declaration of Game class, prototype of game definitions.
 *
 */

#ifndef __Game__
#define __Game__

#include<string>
#include "common.h"

using namespace std;

/**
 * Game class - prototype of game definitions.
 */

class Game {
	int	gs;		/**< game state */
	string	msg;		/**< game state message */

public:
	typedef int move;

	move parse_move(string mv) {
		stringstream ss;
		ss << mv;
		int m;
		ss >> m;
		return (move) m;
	}

	string print_move(move mv) {
		stringstream ss;
		ss << (int) mv;
		string s;
		ss >> s;
		return s;
	}


	/**
	 * Default constructor.
	 */
	Game()
	{
		msg = "In progress";
		gs = in_progress;
	}

	const static int in_progress = 100;	/**< %Game is in progress (constant returned by game_state()) */
	const static int white_wins = 1; 	/**< White won (constant returned by game_state()) */
	const static int black_wins = -1; 	/**< Black won (constant returned by game_state()) */
	const static int draw = 0; 		/**< %Game is drawn (constant returned by game_state()) */
	const static int illegal = 101;		/**< Current position is illegal (possible if implementation of
						           move_list() allows pseudo legal moves) (constant returend by
						           game_state()) */

        const static int unknown = 102;
        const static int minfty = -2;
        const static int pinfty = 2;

	/**
	 * Get game state.
	 */
	int game_state() { return gs; }

	/**
	 * Set (force) game state.
	 */
	int game_state(int _gs) { return gs = _gs; }

	/**
	 * Get game state message.
	 */
	string game_state_message() { return msg; }

	/**
	 * Set (force) game state message.
	 */
	string game_state_message(string s) { return msg = s; }

	/**
	 * White To Move.
	 * Returns \a true if white is on move, \a false otherwise.
	 */
	inline int wtm() const;

	/**
	 * Clear game history (start new game).
	 */
	void clear();

	void prepare_moves();
	bool has_next_move();
	move get_next_move();

	void make_move(const move);
	move retract_move();

	/**
	 * Name of the game.
	 */
	static string name() {
	  return "(guess the name)";
	}
};

#endif /* __Game__ */
