/**
 * \file
 *
 * main().
 *
 */

#include <iostream>
using namespace std;

#include "Settings.h"
#include "TreeEngine.h"
#include "ConnectSix.h"

TreeEngine<ConnectSix> engine;

int main(int argc, char **argv)
{
    engine.main();

	cout << "Bye!\n";

	return 0;
}
