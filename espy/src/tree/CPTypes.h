#ifndef CPTYPES_H_
#define CPTYPES_H_

enum CPColor {
    CP_COLOR_WHITE,
    CP_COLOR_BLACK
};

enum CPRole {
    CP_ROLE_WHITE,
    CP_ROLE_BLACK,
    CP_ROLE_ANALYST
};

enum CPResult {
    CP_RESULT_WHITE,
    CP_RESULT_BLACK,
    CP_RESULT_DRAW
};

#endif
