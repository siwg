/*------------------------------------------------------------- -*- linux-c -*- *
 | Brain - suicide chess program.						|
 *------------------------------------------------------------------------------*/

#ifndef __Settings__
#define __Settings__

/**
 */

class Settings {

public:
	/**
	 * Maximum game length in half-moves.
	 */
	static const int maximum_game_length = 1024;

	/**
	 * Program version.
	 */
	static const char* version()
	{
		return "0.1.0";
	}
};

#endif
