/*
 * CPCommandParser.h
 *
 *  Created on: 2012-03-19
 *      Author: elshize
 */

#ifndef CPCOMMANDPARSER_H_
#define CPCOMMANDPARSER_H_

#include <istream>
#include <sstream>
#include <utility>
#include "CPEventListener.h"

class CPCommandParser {
    std::istream & inputStream;
    std::stringstream commandStream;
    CPEventListener & listener;

    static const std::string CN_CONNECT;
    static const std::string CN_BESTMOVE;
    static const std::string CN_ERROR;
    static const std::string CN_CONCEDE;
    static const std::string CN_INFO;

    static const std::string CN_INIT;
    static const std::string CN_STARTGAME;
    static const std::string CN_MOVE;
    static const std::string CN_FEEDBACK;
    static const std::string CN_SETOPTION;
    static const std::string CN_GETMOVE;
    static const std::string CN_FORCEMOVE;
    static const std::string CN_STOP;
    static const std::string CN_ENDGAME;
    static const std::string CN_QUIT;

    static const std::string UNKNOWN_LABEL;
    static const std::string WHITE_LABEL;
    static const std::string BLACK_LABEL;
    static const std::string ANALYST_LABEL;
    static const std::string DRAW_LABEL;
    static const std::string INFINITY_LABEL;

    void getCommand(std::string & commandType);

    void commandConnect();
    void commandBestMove();
    void commandError();
    void commandConcede();
    void commandInfo();
    void commandInit();
    void commandStartGame();
    void commandMove();
    void commandFeedback();
    void commandSetOption();
    void commandGetMove();
    void commandForceMove();
    void commandStop();
    void commandEndGame();
    void commandQuit();

    void commandUnknown();

//     bool match(std::string & name, CommandType type);
    std::string pullNextArg();
    std::string pullPosition();
    std::string pullMove();
    CPColor colorFromString(std::string colorString);
    CPRole roleFromString(std::string roleString);
    CPResult resultFromString(std::string resultString);
    int timeFromString(std::string timeString);
	std::pair<int, int> timesFromString(std::string times);

public:
    CPCommandParser(std::istream & inputStream, CPEventListener & listener);
    std::string nextCommand();
    void parse(std::string);
//    void nextCommand(, CommandType expectedCommand);
};

#endif /* CPCOMMANDPARSER_H_ */
