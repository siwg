/**
 * \file
 *
 * Implementation of Connect Six game.
 *
 * This file is part of espy, the game solver.
 *
 * \author Jakub Tlałka, Jacek Tomaszewski.
 *
 */

#ifndef __ConnectSix__
#define __ConnectSix__

#include <utility>
#include <cstdio>
#include <set>
#include <vector>

#include "Game.h"
#include "Settings.h"

#define xx first
#define yy second
#define FIELD_OUT make_pair(-1, -1)
#define MOVE_OUT make_pair(FIELD_OUT, FIELD_OUT)

using namespace std;

class ConnectSix : public Game
{
    public:
        typedef pair<int,int> field;
        typedef pair<field,field> mmove;
        typedef int move;

        static string name() {
            return "Connect Six";
        }

	string print_move(move mv) {
	  mmove mmv = int_to_move(mv);

	  stringstream ss;

	  ss << ((char) (mmv.xx.xx + 'a'))
	     << (mmv.xx.yy)
	     << ((char) (mmv.yy.xx + 'a'))
	     << (mmv.yy.yy);

	  return ss.str();
	}

	move parse_move(string mv) {
		const char* mc = mv.c_str();

		char x1, x2;
		int y1,y2;

		int num = sscanf(mc,"%c%d%c%d",&x1,&y1,&x2,&y2);

		mmove m;
		m.xx.xx = x1 - 'a';
		m.xx.yy = y1;
		if (num == 4) {
			m.yy.xx = x2 - 'a';
			m.yy.yy = y2;
		} else {
			m.yy.xx = m.xx.xx;
			m.yy.yy = m.xx.yy;
		}

		if ((m.xx.xx == m.yy.xx && m.xx.yy > m.yy.yy)  ||
		    (m.xx.xx > m.yy.xx)) {
			field f1 = m.xx;
			field f2 = m.yy;
			m.xx = f2;
			m.yy = f1;
			cout << "switch" << endl;
		}

		return move_to_int(m);
	}

        const static int WHITE = 0;
        const static int BLACK = 1;
        const static int EMPTY = 2;
        const static int BOARD_SIZE = 19;
	    const static int MAX_DEPTH = BOARD_SIZE * BOARD_SIZE;
        const static int REASON_DIS = 1;
        const static unsigned int REASON_CUT_TRES = 30; // przy ilu *ruchach* ciac
        const static unsigned int REASON_CUT_TARG = 20; //   do ilu *ruchow*  ciac
        const static unsigned int REASON_CUT_BONUS = 5; //   ile dodatkowych czasem

        struct board
        {
            int sq[BOARD_SIZE][BOARD_SIZE];
        } bd;

        int move_number;
        /* current move on the list of moves */
        mmove moves_list[MAX_DEPTH];

        /* Keeps traction of moves */
        mmove played[MAX_DEPTH];

        /* Count reasonable fields*/
        typedef set<field>::iterator sit;
        set<field> reasonable[MAX_DEPTH];
        vector<mmove>::iterator r_it[MAX_DEPTH];
        vector<mmove> selected[MAX_DEPTH];

    public:

        void print_board()
        {
            printf("move_number: %d\n", move_number);
            for(int y = BOARD_SIZE - 1; y >= 0; y--)
            {
                for(int x = 0; x < BOARD_SIZE; x++)
                    printf("%d", bd.sq[x][y]);
                printf("\n");
            }
        }

        /* Increments field. Returns false if there is no left */
        bool inc_field(field &a)
        {
            a.yy++;
            if(a.yy == BOARD_SIZE)
            {
                a.yy = 0;
                a.xx ++;
            }
            if(a.xx == BOARD_SIZE)
                return false;
            else
                return true;
        }

        bool isfree(field a)
        {
            return a.xx >=0 && a.xx < BOARD_SIZE && a.yy >=0 && a.yy < BOARD_SIZE && bd.sq[a.xx][a.yy] == EMPTY;
        }

        // Searches for a free field greater than the given one
        field find_free_field(field f)
        {
            while(inc_field(f))
            {
                if(isfree(f))
                    return f;
            }
            return FIELD_OUT;
        }

        bool ownedby(field a, int player)
        {
            return a.xx >=0 && a.xx < BOARD_SIZE && a.yy >=0 && a.yy < BOARD_SIZE && bd.sq[a.xx][a.yy] == player;
        }

        void mark_move(const mmove m, int player)
        {
            bd.sq[m.xx.xx][m.xx.yy] = player;
            bd.sq[m.yy.xx][m.yy.yy] = player;
        }

        bool check_line(int x0, int y0, int player, int dx, int dy)
        {
            int mx = 0;
            int cur = 0;
            for(int x=x0, y=y0, i=0; i<11; i++, x += dx, y += dy)
            {
                if(ownedby(make_pair(x,y), player))
                    cur ++;
                else {
                    if (cur == 4) { // update reasonables
                        field f = make_pair(x+dx,y+dy);
                        if (isfree(f))
                            reasonable[move_number].insert(f);
                        f = make_pair(x-6*dx,y-6*dy);
                        if (isfree(f))
                            reasonable[move_number].insert(f);
                    }
                    cur = 0;
                }
                mx = max(mx, cur);
            }
            return mx >= 6;
        }

        bool game_finished_on_field(field f, int player)
        {
            if(check_line(f.xx-5, f.yy-5, player, 1, 1))
                return true;
            if(check_line(f.xx+5, f.yy-5, player, -1, 1))
                return true;
            if(check_line(f.xx, f.yy-5, player, 0, 1))
                return true;
            if(check_line(f.xx-5, f.yy, player, 1, 0))
                return true;
            return false;
        }

        bool game_finished(const mmove m, int player)
        {
            return game_finished_on_field(m.xx, player) || game_finished_on_field(m.yy, player);
        }

        field int_to_field(const int m)
        {
            return make_pair(m / BOARD_SIZE, m % BOARD_SIZE);
        }

        mmove int_to_move(const int m)
        {
            int fnum = BOARD_SIZE * BOARD_SIZE;
            return make_pair(int_to_field(m / fnum), int_to_field(m % fnum));
        }

        int field_to_int(field f)
        {
            return f.xx * BOARD_SIZE + f.yy;
        }

        int move_to_int(mmove m)
        {
            return field_to_int(m.xx) * BOARD_SIZE * BOARD_SIZE + field_to_int(m.yy);
        }

    public:
        ConnectSix()
        {
            clear();
        };

        void clear()
        {
            move_number = 0;
            for(int i = 0; i < BOARD_SIZE; i++)
                for(int j = 0; j < BOARD_SIZE; j++)
                    bd.sq[i][j] = EMPTY;
            reasonable[0].clear();
            selected[0].clear();
            score[0] = draw;
            threats[0][0] = 0;
            threats[1][0] = 0;
            game_state(in_progress);
        }

        bool first;

        void prepare_moves()
        {
            sit it, it2;

            it = it2 = reasonable[move_number].begin();
            if (game_state() != in_progress)
                it = reasonable[move_number].end();
            if (it2 != reasonable[move_number].end())
                ++it2;

            selected[move_number].clear();

            while (it != reasonable[move_number].end()) {
                mmove res = make_pair(*(it), *(it2));
                if (++it2 == reasonable[move_number].end()) {
                    ++it;
                    it2 = it;
                    if (++it2 == reasonable[move_number].end()) // not a pair
                    ++it;
                }
                selected[move_number].push_back(res);
            }
            r_it[move_number] = selected[move_number].begin();

            // przytnij drzewko
            int player = move_number & 1;
            if (selected[move_number].size() > REASON_CUT_TRES) {
                vector<mmove>::iterator it = selected[move_number].begin();
                set<pair<int, mmove> > cur;
                int cpy = score[move_number];
                int cpy1 = threats[0][move_number];
                int cpy2 = threats[1][move_number];
                vector<mmove> killers;

                if (first) {
                    // seeking killers
                    for (; it != selected[move_number].end(); ++it) {
                        update_score(*it, 1);
                        mark_move(*it, player^1);
                        update_score(*it, 0);
                        if (abs(score[move_number]) > white_wins/2)
                            killers.push_back(*it);
                        //cout << "KILLER " << print_move(move_to_int(*it)) << endl;
                        cur.insert(make_pair(score[move_number], *it));
                        score[move_number] = cpy;
                        unmark_move(*it);
                    }
                    it = selected[move_number].begin();
                }

                for (; it != selected[move_number].end(); ++it) {
                    update_score(*it, 1);
                    mark_move(*it, player);
                    update_score(*it, 0);
                    if (first) {
                        // testing killers
                        vector<mmove>::iterator it2 = killers.begin();
                        for (; it2 != killers.end(); ++it2) {
                            field f1 = it->xx, f2 = it->yy;
                            if (it2->xx != f1 && it2->xx != f2 && it2->yy != f1 && it2->yy != f2) {
                                score[move_number] += (player == WHITE ? black_wins : white_wins);
                                break;
                            }
                        }
                    }
                    //if (first) cout << "DBG " << move_number << " " << print_move(move_to_int(*it)) << " " << score[move_number] << endl;
                    cur.insert(make_pair(score[move_number], *it));
                    score[move_number] = cpy;
                    unmark_move(*it);
                }
                threats[0][move_number] = cpy1;
                threats[1][move_number] = cpy2;
                selected[move_number].clear();
                //if (first) cout << (int)threats[player][move_number] << " " << (int)threats[player^1][move_number] << endl;
                unsigned int target = REASON_CUT_TARG + ((first && threats[player^1][move_number] > 2) ? REASON_CUT_BONUS : 0);
                if (player == WHITE) {
                    set<pair<int, mmove> >::reverse_iterator cit = cur.rbegin();
                    for (unsigned int i=0; i<target; i++, ++cit)
                        selected[move_number].push_back(cit->second);
                } else {
                    set<pair<int, mmove> >::iterator cit = cur.begin();
                    for (unsigned int i=0; i<target; i++, ++cit)
                        selected[move_number].push_back(cit->second);
                }
                r_it[move_number] = selected[move_number].begin();
            }
        }

        bool has_next_move()
        {
            return r_it[move_number] != selected[move_number].end();
        }

        move get_next_move()
        {
            return move_to_int(*(r_it[move_number]++));
        }

        void update_reasonable(const field &f) {
            reasonable[move_number].erase(f);
            int km = min(f.xx+REASON_DIS+1, BOARD_SIZE+0);
            int lm = min(f.yy+REASON_DIS+1, BOARD_SIZE+0);
            for (int k=max(f.xx-REASON_DIS, 0); k<km; k++)
                for (int l=max(f.yy-REASON_DIS, 0); l<lm; l++)
                    if (bd.sq[k][l] == EMPTY)
                        reasonable[move_number].insert(make_pair(k,l));
        }

        void make_move(const move mint)
        {
            const mmove m = int_to_move(mint);
            int player = move_number & 1;
            played[move_number] = m;
            move_number ++;

            score[move_number] = score[move_number-1];
            threats[0][move_number] = threats[0][move_number-1];
            threats[1][move_number] = threats[1][move_number-1];
            update_score(m, 1); // odejmiemy sytuacje przed
            mark_move(m, player);
            update_score(m, 0); // dodamy po

            reasonable[move_number] = reasonable[move_number-1];
            update_reasonable(m.xx);
            update_reasonable(m.yy);

            /* set game state */

            game_state(in_progress);
            if(game_finished(m, player))
            {
                if(player == WHITE)
                {
                    game_state(white_wins);
                    score[move_number] = white_wins;
                }
                else
                {
                    game_state(black_wins);
                    score[move_number] = black_wins;
                }
            }
            if(move_number * 2 + 1 >= BOARD_SIZE * BOARD_SIZE)
            {
                game_state(draw);
            }

        }

        void unmark_move(const mmove m) {
            bd.sq[m.xx.xx][m.xx.yy] = EMPTY;
            bd.sq[m.yy.xx][m.yy.yy] = EMPTY;
        }

        int retract_move()
        {
            --move_number;
            mmove m = played[move_number];
            unmark_move(m);
            return move_to_int(m);
        }

        inline int wtm() const
        {
            return (move_number & 1) == 0;
        }

        /* evaluation */

        const static int white_wins = 10000;
        const static int black_wins = -10000;
        const static int draw = 0;

        const static int in_progress = 300002;
        const static int illegal = 300001;
        const static int unknown = 300000;
        const static int minfty = -200000;
        const static int pinfty = 200000;

        int score[MAX_DEPTH];
        char threats[2][MAX_DEPTH];

        const static int scores[12];

        void score_line(int x0, int y0, int player, int dx, int dy)
        {
            char blank = 0;
            char my = 0;
            char op = 0;

            char open_my = 0;
            char open_op = 0;

            char dthreat = (player == 0) ? 1 : -1;

            for (int x=x0, y=y0, i=0; i<13; i++, x += dx, y += dy)
            {
                field f = make_pair(x,y);

                if (ownedby(f, player)) {
                    my ++;
                    open_my += blank;
                    blank = op = open_op = 0;
                } else if (ownedby(f, player^1)) {
                    op ++;
                    open_op += blank;
                    blank = my = open_my = 0;
                }
                else if (isfree(f)) { // empty
                    blank ++;
                }

                // retract field
                if (i >= 6) {
                    field f = make_pair(x-6*dx, y-6*dy);
                    if (ownedby(f, player) && my>0) my--;
                    else if (ownedby(f, player^1) && op>0) op--;
                    else if (isfree(f)) {
                        if (open_my > 0) open_my--;
                        if (open_op > 0) open_op--;
                        if (blank > 5) blank--;
                    };
                }

                if (i >= 5) {
                    if (op && op + open_op + blank >= 6) {
                        score[move_number] -= scores[(op-1)*2+(blank>0?1:0)];
                        if (op > 3) threats[player^1][move_number] += dthreat;
                    }
                    if (my && my + open_my + blank >= 6) {
                        score[move_number] += scores[(my-1)*2+(blank>0?1:0)];
                        if (my > 3) threats[player][move_number] += dthreat;
                    }
                }
            }
        }

        void update_score_on_field(field f, int player)
        {
            score_line(f.xx-6, f.yy-6, player, 1, 1);
            score_line(f.xx+6, f.yy-6, player, -1, 1);
            score_line(f.xx, f.yy-6, player, 0, 1);
            score_line(f.xx-6, f.yy, player, 1, 0);
        }

        void update_score(const mmove& m, int player) {
            update_score_on_field(m.xx, player);
            update_score_on_field(m.yy, player);
        }

        int evaluate() {
            return score[move_number];
        }

        int op_threat() {
            return threats[wtm()?1:0][move_number+1];
        }
};

const int ConnectSix::scores[12] = { 0, 0, 2, 3, 6, 12, 22, 30, 18, 22, ConnectSix::white_wins, ConnectSix::white_wins };

#endif
