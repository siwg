/**
 * \file
 *
 * Implementation of minimax search with alpha-beta cut-offs.
 *
 */

#ifndef __MinimaxAB__
#define __MinimaxAB__

#include "Search.h"
#include "Settings.h"

template<class G>
class MinimaxAB : public Search<G> {

    const static int max_depth = Settings::maximum_game_length; /**< Maximum search depth. */

    G g;

    int value[max_depth];
    int alpha[max_depth];
    int beta[max_depth];
    int ply;
    int nds;
    int depth_limit;
    typename G::move bm;
    char thr;

    void init() {
        g.first = true;
        g.prepare_moves();
        g.first = false;
        ply = 0; nds = 1;
        value[ply] = g.game_state();
        alpha[ply] = G::minfty;
        beta [ply] = G::pinfty;
        if (g.game_state() == G::in_progress) {
            if (g.wtm()) {
                value[ply] = G::minfty;
            } else {
                value[ply] = G::pinfty;
            }
        }
        bm = 0;
        thr = 255;
    }

    void propagateAB() {
        if (g.wtm()) {
            if (value[ply] != G::pinfty)
                alpha[ply-1] = max(alpha[ply-1], value[ply]);
        } else
            if (value[ply] != G::minfty)
                beta[ply-1] = min(beta[ply-1], value[ply]);
    }

public:
    MinimaxAB<G>() {
        g.clear();
        depth_limit = max_depth;

        init();
    }

    void search();
    void search_single_node();

    void position(G &);
    int nodes() { return nds; }
    int depth() { return depth_limit; }
    int depth(int _dep) { return depth_limit = _dep; }

    int result() {
        if (ply > 0 || g.has_next_move()) { return G::in_progress; }

        return value[0];
    }

    typename G::move bestmove() {
        return bm;
    }
};

template<class G> void MinimaxAB<G>::position(G & position)
{
    g = position;
    init();
}

template<class G> void MinimaxAB<G>::search_single_node()
{
    bool cutoff = false;

    if (ply == depth_limit) {
        cutoff = true;
    }

    while ((cutoff || !g.has_next_move() || beta[ply] <= alpha[ply]) && ply > 0) {
        typename G::move lastm = g.retract_move();
        propagateAB();
        ply--;

        switch (value[ply+1]) {
        case G::unknown:
            break;
        default:
            //cout << "ply " << ply << " VAL " << value[ply+1] << endl;
            if (g.wtm()) {
                if (value[ply+1] > value[ply]) {
                    value[ply] = value[ply+1];
                    if (ply == 0) {
                        bm = lastm;
                        thr = g.op_threat();
                    }
                }
                else if (ply == 0 && value[ply+1] == value[ply] && thr > g.op_threat()) {
                    bm = lastm;
                    thr = g.op_threat();
                    //cout << "DESPERATION " << (int)thr << endl;
                }
                cutoff = value[ply] >= G::white_wins-depth_limit;
            } else {
                /*if (ply == 0) {
                    cout << "DBG " << g.print_move(lastm) << " " << value[ply] << " " << value[ply+1] << " THR " << (int)thr << " " << g.op_threat() << " " << (int)g.threats[0][g.move_number] << endl;
                }*/
                if (value[ply+1] < value[ply]) {
                    value[ply] = value[ply+1];
                    if (ply == 0) {
                        bm = lastm;
                        thr = g.op_threat();
                    }
                }
                else if (ply == 0 && value[ply+1] == value[ply] && thr > g.op_threat()) {
                    bm = lastm;
                    thr = g.op_threat();
                    //cout << "DESPERATION " << (int)thr << endl;
                }
                cutoff = value[ply] <= G::black_wins+depth_limit;
            }
            break;
        }
	}

    if (g.has_next_move()) {
        g.make_move(g.get_next_move()); ply++; nds++;
        g.prepare_moves();
        alpha[ply] = alpha[ply-1];
        beta[ply] = beta[ply-1];

        value[ply] = g.game_state();
        if (value[ply] == G::in_progress) {
            if (ply == depth_limit)
                value[ply] = g.evaluate();
            else if (g.wtm())
                value[ply] = G::minfty;
            else
                value[ply] = G::pinfty;
        }
        else if (value[ply] == G::white_wins)
            value[ply] -= ply;
        else if (value[ply] == G::black_wins)
            value[ply] += ply;
    }
}

template<class G> void MinimaxAB<G>::search()
{
	while (result() == G::in_progress) {
		search_single_node();
	}
}

#endif /* __MinimaxAB__ */
