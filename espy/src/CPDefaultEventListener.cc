#include "CPDefaultEventListener.h"
#include <iostream>

using namespace std;

void CPDefaultEventListener::connect(std::string name) {
  cout << "Received: connect" << endl;
}

void CPDefaultEventListener::bestMove(std::string move) {
  cout << "Received: bestMove" << endl;
}

void CPDefaultEventListener::error(std::string message) {
  cout << "Received: error" << endl;
}

void CPDefaultEventListener::concede() {
  cout << "Received: concede" << endl;
}

void CPDefaultEventListener::info(map<string, string> & options) {
  cout << "Received: info" << endl;
}

void CPDefaultEventListener::init() {
  cout << "Received: init" << endl;
}

void CPDefaultEventListener::startGame(std::string position, CPRole role,
                int gameTime, int bonusTime) {
  cout << "Received: startGame" << endl;
  cout << "\tGame time: " << gameTime << endl;
  cout << "\tBonus time: " << bonusTime << endl;
}

void CPDefaultEventListener::move(CPColor color, std::string move) {
  cout << "Received: move" << endl;
}

void CPDefaultEventListener::feedback(bool on) {
  cout << "Received: feedback" << endl;
}

void CPDefaultEventListener::setOption(map<string, string> & options) {
  cout << "Received: setOption" << endl;
}

void CPDefaultEventListener::getMove(CPColor color, int moveTime,
              int gameTime) {
  cout << "Received: getMove" << endl;
}

void CPDefaultEventListener::forceMove() {
  cout << "Received: forceMove" << endl;
}

void CPDefaultEventListener::stop() {
  cout << "Received: stop" << endl;
}

void CPDefaultEventListener::endGame(CPResult result) {
  cout << "Received: endGame" << endl;
}

void CPDefaultEventListener::quit() {
  cout << "Received: quit" << endl;
}
