/* Connect six game logic.
 *
 * Author: Przemyslaw Horban (ph262940@students.mimuw.edu.pl)
 */

#include <iostream>
using namespace std;

#include "PHConnectSix.h"

#include "CPCommandParser.h"
#include "CPEventListener.h"

class PHConnectSixEngine {
  private:
    const static int N = 19;
    StripWeigths weigths;
    PHConnectSix *conSix;

  public:
    void init() {
      int wwVals[] = {0, 2, 4, 8, 1600, 5000, 1000000000};
      int wbVals[] = {0, 2, 4, 8, 160, 500, 1000000000};
      int bwVals[] = {0, 2, 4, 8, 160, 500, 1000000000};
      int bbVals[] = {0, 2, 4, 8, 1600, 5000, 1000000000};
      vector<int> wwV(wwVals, wwVals + 7);
      vector<int> wbV(wbVals, wbVals + 7);
      vector<int> bwV(bwVals, bwVals + 7);
      vector<int> bbV(bbVals, bbVals + 7);
      weigths.weigths[WHITE_MOVE][WHITE_WEIGTH] = wwV;
      weigths.weigths[WHITE_MOVE][BLACK_WEIGTH] = wbV;
      weigths.weigths[BLACK_MOVE][WHITE_WEIGTH] = bwV;
      weigths.weigths[BLACK_MOVE][BLACK_WEIGTH] = bbV;

      conSix = new PHConnectSix(N, weigths);
    }

    void startGame() {
      conSix->clear();
    }

    void moved(string mv, color c) {
      if((conSix->wtm() && c == black) ||
         (!conSix->wtm() && c == white))
        fail("Color discrepancy in move");

      char c1, c2;
      int a1, a2=-1;
      if(mv.size() >= 4)
        sscanf(mv.c_str(), " %c%d%c%d ", &c1, &a1, &c2, &a2);
      else
        sscanf(mv.c_str(), " %c%d ", &c1, &a1);

      pos l = conSix->xyToPos(a1, c1 - 'a');
      pos r = no_pos;
      if(a2 != -1)
        r = conSix->xyToPos(a2, c2 - 'a');

      move m(l, r);
      conSix->make_move(m);
    }

    string genMove(color c) {
      if((conSix->wtm() && c == black) ||
         (!conSix->wtm() && c == white))
        fail("Color discrepancy in genMove");

      //vector<move> moves;
      //conSix->prepare_moves();
      //while(conSix->has_next_move())
      //  moves.push_back(conSix->get_next_move());

      //int best = 0;
      //if(c == black) {
      //  for(int i = 0; i < (int)moves.size(); i++)
      //    if(moves[i].value != no_value && moves[i].value < moves[best].value)
      //      best = i;
      //}
      //else {
      //  for(int i = 0; i < (int)moves.size(); i++)
      //    if(moves[i].value != no_value && moves[i].value > moves[best].value)
      //      best = i;
      //}
      //move m = moves[best];
      move m = conSix->bestMove();

      conSix->make_move(m);
      string mv;
      if(m.p1 != no_pos)
        mv += conSix->posToStr(m.p1) + "";
      if(m.p2 != no_pos)
        mv += conSix->posToStr(m.p2);
      return mv;
    }
};

class C6Listener: public CPEventListener {

public:
    virtual void connect(std::string name);
    virtual void bestMove(std::string move);
    virtual void error(std::string message);
    virtual void concede();
    virtual void info(std::map<std::string, std::string> & options);
    virtual void init();
    virtual void startGame(std::string position, CPRole role,
                   int gameTime, int bonusTime);
    virtual void move(CPColor color, std::string move);
    virtual void feedback(bool on);
    virtual void setOption(std::map<std::string, std::string> & options);
    virtual void getMove(CPColor color, int moveTime,
                 int gameTime);
    virtual void forceMove();
    virtual void stop();
    virtual void endGame(CPResult result);
    virtual void quit();

    PHConnectSixEngine engine;
};


int main(int argc, char **argv) {
  C6Listener listener;
  CPCommandParser parser(cin, listener);
  while (true) {
      string line = parser.nextCommand();
      try {
          parser.parse(line);
      } catch (int error) {
          if (error == 2)
            cout << "Unknown command." << endl;
          else
            cout << "Command corrupted: " << line << endl;
      }
  }
	return 0;
}

void C6Listener::connect(std::string name) {
  cout << "Received: connect" << endl;
}

void C6Listener::bestMove(std::string move) {
  cout << "Received: bestMove" << endl;
}

void C6Listener::error(std::string message) {
  cout << "Received: error" << endl;
}

void C6Listener::concede() {
  cout << "Received: concede" << endl;
}

void C6Listener::info(map<string, string> & options) {
  cout << "Received: info" << endl;
}

void C6Listener::init() {
  engine.init();
}

void C6Listener::startGame(std::string position, CPRole role,
                int gameTime, int bonusTime) {
  cout << "Received: startGame" << endl;
  cout << "\tGame time: " << gameTime << endl;
  cout << "\tBonus time: " << bonusTime << endl;
  cout << position << endl;
  if(position != "{E,E,E,E,E,E,E,E,E,E,E,E,E,E,E,E,E,E,E|E,E,E,E,E,E,E,E,E,E,E,E,E,E,E,E,E,E,E|E,E,E,E,E,E,E,E,E,E,E,E,E,E,E,E,E,E,E|E,E,E,E,E,E,E,E,E,E,E,E,E,E,E,E,E,E,E|E,E,E,E,E,E,E,E,E,E,E,E,E,E,E,E,E,E,E|E,E,E,E,E,E,E,E,E,E,E,E,E,E,E,E,E,E,E|E,E,E,E,E,E,E,E,E,E,E,E,E,E,E,E,E,E,E|E,E,E,E,E,E,E,E,E,E,E,E,E,E,E,E,E,E,E|E,E,E,E,E,E,E,E,E,E,E,E,E,E,E,E,E,E,E|E,E,E,E,E,E,E,E,E,E,E,E,E,E,E,E,E,E,E|E,E,E,E,E,E,E,E,E,E,E,E,E,E,E,E,E,E,E|E,E,E,E,E,E,E,E,E,E,E,E,E,E,E,E,E,E,E|E,E,E,E,E,E,E,E,E,E,E,E,E,E,E,E,E,E,E|E,E,E,E,E,E,E,E,E,E,E,E,E,E,E,E,E,E,E|E,E,E,E,E,E,E,E,E,E,E,E,E,E,E,E,E,E,E|E,E,E,E,E,E,E,E,E,E,E,E,E,E,E,E,E,E,E|E,E,E,E,E,E,E,E,E,E,E,E,E,E,E,E,E,E,E|E,E,E,E,E,E,E,E,E,E,E,E,E,E,E,E,E,E,E|E,E,E,E,E,E,E,E,E,E,E,E,E,E,E,E,E,E,E}")  {
    fail("Custom initial positions not supported yet");
  }
  engine.startGame();
}

void C6Listener::move(CPColor color, std::string move) {
  //cout << "Received: move" << endl;
  engine.moved(move, color == CP_COLOR_WHITE ? white : black);
}

void C6Listener::feedback(bool on) {
  cout << "Received: feedback" << endl;
}

void C6Listener::setOption(map<string, string> & options) {
  cout << "Received: setOption" << endl;
}

void C6Listener::getMove(CPColor color, int moveTime,
              int gameTime) {
  //cout << "Received: getMove" << endl;
  string s = engine.genMove(color == CP_COLOR_WHITE ? white : black);

  printf("bestmove %s\n", s.c_str());
  fflush(stdout);
}

void C6Listener::forceMove() {
  cout << "Received: forceMove" << endl;
}

void C6Listener::stop() {
  cout << "Received: stop" << endl;
}

void C6Listener::endGame(CPResult result) {
  cout << "Received: endGame" << endl;
}

void C6Listener::quit() {
  cout << "Received: quit" << endl;
}
