#include <vector>
#include <cstdio>
#include <iostream>
#include <cassert>
#include <cstdlib>
#include <algorithm>
#include <ctime>
using namespace std;

#include "PHConnectSixBoard.h"

#define deb(x) { cerr << #x << " = " << x << endl; }

typedef char color;

static const color no_color = 0;
static const color white = 1;
static const color black = -1;

int wwVals[] = {0, 10, 100, 1000, 10000, 100000, 1000000};
int wbVals[] = {0, 30, 300, 3000, 30000, 300000, 3000000};
int bwVals[] = {0, 50, 500, 5000, 50000, 500000, 5000000};
int bbVals[] = {0, 70, 700, 7000, 70000, 700000, 7000000};
vector<int> wwV(wwVals, wwVals + 7);
vector<int> wbV(wbVals, wbVals + 7);
vector<int> bwV(bwVals, bwVals + 7);
vector<int> bbV(bbVals, bbVals + 7);

void unitTest();
void brutTest();

class BrutNeighbours {
  private:
    int n;

    vector<color> board;
  public:
    BrutNeighbours(size_t n) {
      this->n = n;
      board.clear();
      board.resize(n * n, no_color);
    }

    color pieceAt(size_t px, size_t py) {
      return board[py * n + px];
    }

    void insertPiece(size_t px, size_t py, color c) {
      board[py * n + px] = c;
    }

    void removePiece(size_t px, size_t py, color c) {
      assert(board[py * n + px] == c);
      board[py * n + px] = no_color;
    }

    vector<short> neghbourEmptyCells() {
      vector<short> ans;
      for(int y = 0; y < n; y++)
        for(int x = 0; x < n; x++) {
          if(pieceAt(x, y) == no_color) {
            bool isNeg = false;
            for(int ox = -1; ox < 2; ox++)
              for(int oy = -1; oy < 2; oy++) {
                int nx = ox + x, ny = oy + y;
                if(0 <= nx && nx < n &&
                   0 <= ny && ny < n &&
                   pieceAt(nx, ny) != no_color)
                  isNeg = true;
              }
            if(isNeg)
              ans.push_back(y * n + x);
          }
        }
      return ans;
    }

    void printBoard() {
      for(int x = 0; x < n; x++) 
        putchar('-');
      putchar('\n');

      for(int y = 0; y < n; y++) {
        for(int x = 0; x < n; x++) {
          color c = board[y * n + x];
          if(c == no_color)
            putchar('.');
          else
            putchar(c == white ? 'W' : 'B');
        }
        putchar('\n');
      }
    }
};

int main() {
  srand(time(NULL)); // Make it non deterministic

  brutTest();
  unitTest();
  return 0;
}

void brutTest() {
  const size_t N = 20;
  for(int start = 0; start < 5; start++) {
    StripWeigths weigths;
    weigths.weigths[WHITE_MOVE][WHITE_WEIGTH] = wwV;
    weigths.weigths[WHITE_MOVE][BLACK_WEIGTH] = wbV;
    weigths.weigths[BLACK_MOVE][WHITE_WEIGTH] = bwV;
    weigths.weigths[BLACK_MOVE][BLACK_WEIGTH] = bbV;
    PHConnectSixBoard brd(N, weigths);
    BrutNeighbours bss(N);
    for(int i = 0; i < (int)(N*N*5); i++) {
      int x = rand() % N, y = rand() % N;
      color c = bss.pieceAt(x, y);
      color newC = (rand() % 2 == 0 ? white : black);
      if(c == no_color) {
        bss.insertPiece(x, y, newC);
        brd.placePiece(brd.xyToPos(x, y), newC);
      }
      else {
        bss.removePiece(x, y, c);
        brd.removePiece(brd.xyToPos(x, y));
      }

      vector<short> bssNeg = bss.neghbourEmptyCells();
      vector<short> brdNeg = brd.neghbourEmptyCells();

      sort(bssNeg.begin(), bssNeg.end());
      sort(brdNeg.begin(), brdNeg.end());
      if(bssNeg != brdNeg)
        fail("Test failed");
    }
  }
  puts("OK");
}

void unitTest() {
  StripWeigths weigths;
  weigths.weigths[WHITE_MOVE][WHITE_WEIGTH] = wwV;
  weigths.weigths[WHITE_MOVE][BLACK_WEIGTH] = wbV;
  weigths.weigths[BLACK_MOVE][WHITE_WEIGTH] = bwV;
  weigths.weigths[BLACK_MOVE][BLACK_WEIGTH] = bbV;
  PHConnectSixBoard brd(20, weigths);

  assert(brd.evaluation(white) == 0);
  assert(brd.evaluation(black) == 0);

  brd.placePiece(brd.xyToPos(6, 6), white);

  assert(brd.evaluation(white) == 24 * 10);
  assert(brd.evaluation(black) == 24 * 50);

  brd.removePiece(brd.xyToPos(6, 6));

  assert(brd.evaluation(white) == 0);
  assert(brd.evaluation(black) == 0);

  brd.placePiece(brd.xyToPos(6, 6), black);

  assert(brd.evaluation(white) == -24 * 30);
  assert(brd.evaluation(black) == -24 * 70);

  brd.placePiece(brd.xyToPos(7, 6), white);

  assert(brd.evaluation(white) ==  19 * 10 - 19 * 30);
  assert(brd.evaluation(black) ==  19 * (50 - 70));

  brd.removePiece(brd.xyToPos(7, 6));
  brd.placePiece(brd.xyToPos(7, 6), black);

  assert(brd.evaluation(white) == -(24*2-2*6+2) * 30 - 5 * 300);
  assert(brd.evaluation(black) == -(24*2-2*6+2) * 70 - 5 * 700);

  brd.placePiece(brd.xyToPos(8, 6), black);
  brd.placePiece(brd.xyToPos(9, 6), black);
  brd.placePiece(brd.xyToPos(10, 6), black);
  brd.placePiece(brd.xyToPos(11, 6), black);

  assert(brd.evaluation(white) == -(6*24-6*6+2) * 30 - 2 * 300 - 2 * 3000 - 2 * 30000 - 2 * 300000 - 3000000);
  assert(brd.evaluation(black) == -(6*24-6*6+2) * 70 - 2 * 700 - 2 * 7000 - 2 * 70000 - 2 * 700000 - 7000000);

  puts("OK");
}

