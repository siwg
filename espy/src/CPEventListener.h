#ifndef CPEVENTLISTENER_H_
#define CPEVENTLISTENER_H_

#include <string>
#include <map>
#include "CPTypes.h"

class CPEventListener {
public:
    virtual void connect(std::string name) = 0;
    virtual void bestMove(std::string move) = 0;
    virtual void error(std::string message) = 0;
    virtual void concede() = 0;
    virtual void info(std::map<std::string, std::string> & options) = 0;
    virtual void init() = 0;
    virtual void startGame(std::string position, CPRole role,
                   int gameTime, int bonusTime) = 0;
    virtual void move(CPColor color, std::string move) = 0;
    virtual void feedback(bool on) = 0;
    virtual void setOption(std::map<std::string, std::string> & options) = 0;
    virtual void getMove(CPColor color, int moveTime,
                 int gameTime) = 0;
    virtual void forceMove() = 0;
    virtual void stop() = 0;
    virtual void endGame(CPResult result) = 0;
    virtual void quit() = 0;
};

#endif
