/**
 * \file
 *
 * Implementation of Connect Six game.
 *
 * This file is part of espy, the game solver.
 *
 * \author Jakub Tlałka.
 *
 */

#ifndef __ConnectSix__
#define __ConnectSix__

#include <utility>
#include <cstdio>
#include "Game.h"
#include "Settings.h"
#define xx first
#define yy second
#define FIELD_OUT make_pair(-1, -1)
#define MOVE_OUT make_pair(FIELD_OUT, FIELD_OUT)
using namespace std;

class ConnectSix : public Game
{
    public:
        typedef pair<int,int> field;
        typedef pair<field,field> mmove;
        typedef int move;

        static string name() {
            return "Connect Six";
        }
        
	string print_move(move mv) {
	  mmove mmv = int_to_move(mv);

	  stringstream ss;

	  ss << ((char) (mmv.xx.xx + 'a'))
	     << (mmv.xx.yy)
	     << ((char) (mmv.yy.xx + 'a')) 
	     << (mmv.yy.yy);

	  return ss.str();
	}

	move parse_move(string mv) {
		const char* mc = mv.c_str();

		char x1, x2;
		int y1,y2;

		int num = sscanf(mc,"%c%d%c%d",&x1,&y1,&x2,&y2);

		mmove m;
		m.xx.xx = x1 - 'a';
		m.xx.yy = y1;
		if (num == 4) {
			m.yy.xx = x2 - 'a';
			m.yy.yy = y2;
		} else {
			m.yy.xx = m.xx.xx;
			m.yy.yy = m.xx.yy;
		}

		if ((m.xx.xx == m.yy.xx && m.xx.yy > m.yy.yy)  ||
		    (m.xx.xx > m.yy.xx)) {
			field f1 = m.xx;
			field f2 = m.yy;
			m.xx = f2;
			m.yy = f1;
			cout << "switch" << endl;
		}

		return move_to_int(m);
	}

        const static int WHITE = 0;
        const static int BLACK = 1;
        const static int EMPTY = 2;
        const static int BOARD_SIZE = 19;
	    const static int MAX_DEPTH = BOARD_SIZE * BOARD_SIZE;


        struct board
        {
            int sq[BOARD_SIZE][BOARD_SIZE];
        } bd;

        int move_number;
        /* current move on the list of moves */
        mmove moves_list[MAX_DEPTH];

        /* Keeps traction of moves */
        mmove played[MAX_DEPTH];

    public:
        
        void print_board()
        {
            printf("move_number: %d\n", move_number);
            for(int y = BOARD_SIZE - 1; y >= 0; y--)
            {
                for(int x = 0; x < BOARD_SIZE; x++)
                    printf("%d", bd.sq[x][y]);
                printf("\n");
            }
        }

        /* Increments field. Returns false if there is no left */
        bool inc_field(field &a)
        {
            a.yy++;
            if(a.yy == BOARD_SIZE)
            {
                a.yy = 0;
                a.xx ++;
            }
            if(a.xx == BOARD_SIZE)
                return false;
            else
                return true;
        }

        bool isfree(field a)
        {
            return a.xx >=0 && a.xx < BOARD_SIZE && a.yy >=0 && a.yy < BOARD_SIZE && bd.sq[a.xx][a.yy] == EMPTY;
        }

        bool ownedby(field a, int player)
        {
            return a.xx >=0 && a.xx < BOARD_SIZE && a.yy >=0 && a.yy < BOARD_SIZE && bd.sq[a.xx][a.yy] == player;
        }

        /* Searches for a free field greater than the given one */
        field find_free_field(field f)
        {
            while(inc_field(f))
            {
                if(isfree(f))
                    return f;
            }
            return FIELD_OUT;
        }

        bool move_free()
        {
            mmove cur_move = moves_list[move_number];
            return isfree(cur_move.xx) && isfree(cur_move.yy);
        }

        void mark_move(const mmove m, int player)
        {
            bd.sq[m.xx.xx][m.xx.yy] = player;
            bd.sq[m.yy.xx][m.yy.yy] = player;
        }

        void inc_move()
        {
            mmove cur_move = moves_list[move_number];
            if(move_number == 0)
            {
                field f = find_free_field(cur_move.xx);
                moves_list[move_number] = make_pair(f,f);
                return;
            }
            field field1 = cur_move.xx;
            field field2 = find_free_field(cur_move.yy);
            if(field2 == FIELD_OUT)
            {
                field1 = find_free_field(field1);
                if(field1 == FIELD_OUT)
                    field2 = FIELD_OUT;
                else
                    field2 = find_free_field(field1);
            }
            if(field2 == FIELD_OUT)
                moves_list[move_number] = MOVE_OUT;
            else
                moves_list[move_number] = make_pair(field1, field2);
        }

        bool check_line(int x0, int y0, int player, int dx, int dy)
        {
            int mx = 0;
            int cur = 0;
            for(int x=x0, y=y0, i=0; i<11; i++, x += dx, y += dy)
            {
                if(ownedby(make_pair(x,y), player))
                    cur ++;
                else
                    cur = 0;
                mx = max(mx, cur);
            }
            return mx >= 6;
        }

        bool game_finished_on_field(field f, int player)
        {
            if(check_line(f.xx-5, f.yy-5, player, 1, 1))
                return true;
            if(check_line(f.xx+5, f.yy-5, player, -1, 1))
                return true;
            if(check_line(f.xx, f.yy-5, player, 0, 1))
                return true;
            if(check_line(f.xx-5, f.yy, player, 1, 0))
                return true;
            return false;
        }

        bool game_finished(const mmove m, int player)
        {
            return game_finished_on_field(m.xx, player) || game_finished_on_field(m.yy, player);
        }

        field int_to_field(const int m)
        {
            return make_pair(m / BOARD_SIZE, m % BOARD_SIZE);
        }

        mmove int_to_move(const int m)
        {
            int fnum = BOARD_SIZE * BOARD_SIZE;
            return make_pair(int_to_field(m / fnum), int_to_field(m % fnum));
        }

        int field_to_int(field f)
        {
            return f.xx * BOARD_SIZE + f.yy;
        }

        int move_to_int(mmove m)
        {
            return field_to_int(m.xx) * BOARD_SIZE * BOARD_SIZE + field_to_int(m.yy);
        }

    public:
        ConnectSix()
        {
            clear();
        };

        void clear()
        {
            move_number = 0;
            for(int i = 0; i < BOARD_SIZE; i++)
                for(int j = 0; j < BOARD_SIZE; j++)
                    bd.sq[i][j] = EMPTY;
        }

        void prepare_moves()
        {
            moves_list[move_number].xx.xx = 0;
            moves_list[move_number].xx.yy = 0;
            moves_list[move_number].yy.xx = 0;
            moves_list[move_number].yy.yy = 0;
            if(move_number > 0)
                moves_list[move_number].yy.yy = 1;
        }

        bool has_next_move()
        {
            while(moves_list[move_number] != MOVE_OUT && !move_free())
                inc_move();
            return moves_list[move_number] != MOVE_OUT;
        }

        move get_next_move()
        {
            while(!move_free())
                inc_move();
            mmove res = moves_list[move_number];
            inc_move();
            //printf("ConnectSix: get_next_move: (%d,%d) (%d,%d)\n", res.xx.xx, res.xx.yy, res.yy.xx, res.yy.yy);
            return move_to_int(res);
        }

        void make_move(const move mint)
        {
            const mmove m = int_to_move(mint);
            int player = move_number & 1;
            played[move_number] = m; 
            mark_move(m, player);
            move_number ++;

            /* set game state */

            game_state(in_progress);
            if(game_finished(m, player))
            {
                if(player == WHITE)
                {
                    //printf("ConnectSix: white wins\n");
                    game_state(white_wins);
                }
                else
                {
                    game_state(black_wins);
                    //printf("ConnectSix: black wins\n");
                }
            }
            if(move_number * 2 + 1 >= BOARD_SIZE * BOARD_SIZE)
            {
                game_state(draw);
                //printf("ConnectSix: draw\n");
            }

        }

        void retract_move()
        {
            --move_number;
            bd.sq[played[move_number].xx.xx][played[move_number].xx.yy] = EMPTY;
            bd.sq[played[move_number].yy.xx][played[move_number].yy.yy] = EMPTY;
        }

        inline int wtm() const
        {
            return (move_number & 1) == 0;
        }
};


#endif
