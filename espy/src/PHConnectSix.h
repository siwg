/* Connect six game logic.
 *
 * Author: Przemyslaw Horban (ph262940@students.mimuw.edu.pl)
 */

#ifndef PH_CONNECT_SIX_H
#define PH_CONNECT_SIX_H

#include <stack>
#include <vector>
#include <algorithm>

using namespace std;

#include "Game.h"
#include "Settings.h"
#include "PHConnectSixBoard.h"

enum {
  no_value = -4444444
};

struct move {
  move() {}
  move(pos p1, pos p2): p1(p1), p2(p2), value(no_value) {}
  move(pos p1, pos p2, int value): p1(p1), p2(p2), value(value) {}
  pos p1, p2; // On first move p2 == no_pos;
  int value;
};

#include "PHMinimaxAB.h"

class PHConnectSix : public Game {
  public:
    const static size_t BRANCH_FACT = 8;
    const PHMinimaxAB<PHConnectSix> minimax;
    typedef ::move move;

    StripWeigths weigths;
    size_t size;
    PHConnectSixBoard board;
    bool whiteMove;

    stack<move> movesMade;
    stack<vector<move> > movesAvaliable;

    PHConnectSix(size_t size, StripWeigths weigths)
      : weigths(weigths), size(size), board(size, weigths) {
        clear();
      };

    void clear() {
      while(!movesMade.empty()) movesMade.pop();
      while(!movesAvaliable.empty()) movesAvaliable.pop();
      movesAvaliable.push(vector<move>());
      board = PHConnectSixBoard(size, weigths);
      whiteMove = true;
    }

    bool has_next_move() {
      return !movesAvaliable.top().empty();
    }

    move get_next_move() {
      move m = movesAvaliable.top().back();
      movesAvaliable.top().pop_back();
      return m;
    }

    void prepare_moves() {
      // - First we pick tree best positions to place a piece
      // - Then for each of these positions find second piece
      //   that improoves score most
      movesAvaliable.top().clear();

      if(movesMade.empty()) {
        // Special case of the first move
        movesAvaliable.top().push_back(move(board.xyToPos(size / 2, size / 2),
                                            no_pos));
        return;
      }

      bestMoveSearch.findBestMoves(board, &movesAvaliable.top(),
                                   whiteMove ? white : black);
    }

    void make_move(const move m) {
      movesMade.push(m);
      movesAvaliable.push(vector<move>());

      if(m.p1 != no_pos)
        board.placePiece(m.p1, whiteMove ? white : black);

      if(m.p2 != no_pos)
        board.placePiece(m.p2, whiteMove ? white : black);

      whiteMove = !whiteMove;

      update_game_state();
    }

    move bestMove(int minimaxDepht = 3) {
      move m = minimax.seachBestMove(this, minimaxDepht);
      return m;
    }

    void update_game_state() {
      if(board.whiteWins())
        game_state(white_wins);
      else if(board.blackWins())
        game_state(black_wins);
      else if(board.draw())
        game_state(draw);
      else
        game_state(in_progress);
    }

    void retract_move() {
      move m = movesMade.top();
      movesMade.pop();
      movesAvaliable.pop();

      if(m.p1 != no_pos)
        board.removePiece(m.p1);

      if(m.p2 != no_pos)
        board.removePiece(m.p2);

      whiteMove = !whiteMove;

      update_game_state();
    }

    pos xyToPos(int x, int y) {
      return board.xyToPos(x, y);
    }

    string posToStr(pos p) {
      size_t x, y; board.posToXY(&x, &y, p);
      string s;
      s += 'a' + y;
      char d[5];
      sprintf(d, "%d", (int)x);
      s += d;
      return s;
    }

    inline int wtm() const {
      return whiteMove;
    }

    static string name() {
      return "PHConnectSix";
    }

    int evaluation() {
      return board.evaluation(whiteMove ? white : black);
    }

    void printBoard() {
      board.printBoard();
    }

    void printMoves() {
      printf("Greedy moves\n");
      vector<pair<pos, char> > labels;
      vector<move> &mvs = movesAvaliable.top();
      for(int i = 0; i < (int)mvs.size(); i++) {
        labels.push_back(make_pair(mvs[i].p1, '0' + mvs.size() - i));
        labels.push_back(make_pair(mvs[i].p2, '0' + mvs.size() - i));
      }
      for(int i = (int)mvs.size() - 1; i >= 0; i--) {
        size_t x, y;
        board.posToXY(&x, &y, mvs[i].p1);
        printf("%d: (%zu %zu) ", (int)mvs.size() - i, x, y);
        board.posToXY(&x, &y, mvs[i].p2);
        if(mvs[i].p2 == no_pos) {
          x = -1; 
          y = -1;
        }
        printf("(%zu %zu) %d\n", x, y, mvs[i].value);
      }
      board.printBoard(labels);
    }

    void printMovesMinimax(int depth) {
      printf("Minimax moves\n");
      vector<pair<pos, char> > labels;
      vector<move> mvs = minimax.seachBestMoves(this, depth);
      for(int i = 0; i < (int)mvs.size(); i++) {
        labels.push_back(make_pair(mvs[i].p1, '0' + mvs.size() - i));
        labels.push_back(make_pair(mvs[i].p2, '0' + mvs.size() - i));
      }
      for(int i = (int)mvs.size() - 1; i >= 0; i--) {
        size_t x, y;
        board.posToXY(&x, &y, mvs[i].p1);
        printf("%d: (%zu %zu) ", (int)mvs.size() - i, x, y);
        board.posToXY(&x, &y, mvs[i].p2);
        if(mvs[i].p2 == no_pos) {
          x = -1;
          y = -1;
        }
        printf("(%zu %zu) %d\n", x, y, mvs[i].value);
      }
      board.printBoard(labels);
    }

    void printNeighbours() {
      vector<pair<pos, char> > labels;
      vector<pos> mvs = board.neghbourEmptyCells();
      for(int i = 0; i < (int)mvs.size(); i++)
        labels.push_back(make_pair(mvs[i], 'X'));
      board.printBoard(labels);
    }

  private:
    class /* bestMoveSearch */ {
      private:
        vector<pos> keySpots, secSpots;
        vector<int> values;

      public:
        // Returns the number of moves found
        void findBestMoves(PHConnectSixBoard &board,
                           vector<move> *foundMovesPtr,
                           color player) {
          if(player == no_color)
            fail("Invalid player color");

          vector<move> &foundMoves = *foundMovesPtr;
          foundMoves.clear();

          findBestSpots(board, player, BRANCH_FACT, &keySpots);

          for(int i = 0; i < (int)keySpots.size(); i++) {
            pos p1 = keySpots[i];
            board.placePiece(p1, player);
            findBestSpots(board, player, keySpots.size() * 2,
                          &secSpots, &values);
            board.removePiece(p1);

            pos p2 = no_pos;
            int value;
            for(int j = 0; j < (int)secSpots.size(); j++) {
              if(find(keySpots.begin(), keySpots.begin() + i, secSpots[j]) !=
                 keySpots.begin() + i)
                continue;
              p2 = secSpots[j];
              value = values[j];
              break;
            }

            if(p2 != no_pos)
              foundMoves.push_back(move(p1, p2, value));
          }
        }

      private:
        vector<pos> neigh;
        vector<pair<int, pos> > evals;

        void findBestSpots(PHConnectSixBoard &board, color player,
                           size_t maxSpots, vector<pos> *bestSpotsPtr,
                           vector<int> *evalValuesPtr=NULL) {
          vector<pos> &bestSpots = *bestSpotsPtr;
          bestSpots.clear();

          if(evalValuesPtr)
            evalValuesPtr->clear();

          neigh = board.neghbourEmptyCells();

          size_t chooseCnt = min(neigh.size(), maxSpots);
          if(chooseCnt == 0)
            return;

          evals.clear();
          for(size_t i = 0; i < neigh.size(); i++) {
            pos p = neigh[i];
            board.placePiece(p, player);
            // We evaluate board state where the other player makes move
            int value = board.evaluation(player == white ? black : white);
            evals.push_back(make_pair(value, p));
            board.removePiece(p);
          }

          bool maximize = player == white;
          if(maximize)
            partial_sort(evals.begin(), evals.begin() + chooseCnt, evals.end(),
                         greater<pair<int, pos> >());
          else
            partial_sort(evals.begin(), evals.begin() + chooseCnt, evals.end());

          for(int i = 0; i < (int)chooseCnt; i++) {
            if(evalValuesPtr)
              evalValuesPtr->push_back(evals[i].first);
            bestSpots.push_back(evals[i].second);
          }
        }
    } bestMoveSearch;
};

#endif  // PH_CONNECT_SIX_H
