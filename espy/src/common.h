/*------------------------------------------------------------- -*- linux-c -*- *
 | Espy			                                                        |
 |------------------------------------------------------------------------------|
 *------------------------------------------------------------------------------*/

/**
 * \file
 * Commonly used primitive types and inline functions.
 * \author Andrzej Nag�rko
 */

#ifndef __common__
#define __common__

#include <sstream>

template <class T>
bool from_string(T& t, 
                 const std::string& s)
{
  std::istringstream iss(s);
  return !(iss >> std::dec >> t).fail();
}

#endif
