#include "ConnectSixPositionParser.h"
#include "bnfc/Parser.H"
#include "bnfc/Printer.H"




vector<ConnectSixPosition*> ConnectSixPositionParser::parse(String filePath) {
    FILE *input = fopen(filePath.c_str(), "r");
    if (!input) {
        fprintf(stderr, "Error opening input file.\n");
        return vector<ConnectSixPosition*>();
    }
    ConnectSixPositions* parse_tree = pConnectSixPositions(input);
    if (parse_tree) {
        fprintf(stderr, "\nParse Succesful!\n");
        fprintf(stderr, "[Linearized Tree]\n");
        PrintAbsyn *p = new PrintAbsyn();
        fprintf(stderr, "%s\n\n", p->print(parse_tree));
        ConnectSixPositionVisitor connectSixPositionVisitor;
        connectSixPositionVisitor.visitConnectSixPositions(parse_tree);
        return connectSixPositionVisitor.getPositions();
    }
    return vector<ConnectSixPosition*>();
}