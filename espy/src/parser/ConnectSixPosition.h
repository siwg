#ifndef CONNECT_SIX_POSITION
#define CONNECT_SIX_POSITION


#include "../ConnectSix.h"


class ConnectSixPosition {
public:
    bool promotedMoves[ConnectSix::BOARD_SIZE][ConnectSix::BOARD_SIZE];
    bool wrongMoves[ConnectSix::BOARD_SIZE][ConnectSix::BOARD_SIZE];
    int board[ConnectSix::BOARD_SIZE][ConnectSix::BOARD_SIZE];

    ConnectSixPosition() {
        for (int i = 0 ; i < ConnectSix::BOARD_SIZE ; ++i) {
            for (int j = 0 ; j < ConnectSix::BOARD_SIZE ; ++j) {
                promotedMoves[i][j] = false;
                wrongMoves[i][j] = false;
                board[i][j] = ConnectSix::EMPTY;
            }
        }
    }  
    
};

#endif