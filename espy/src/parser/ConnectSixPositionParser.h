#ifndef POSITION_PARSER_HEADER
#define POSITION_PARSER_HEADER

#include <string>
#include "../ConnectSix.h"
#include "bnfc/ConnectSixPositionVisitor.h"

class ConnectSixPositionParser {
  
public:
    vector<ConnectSixPosition*> parse(String filePath);

};


#endif
