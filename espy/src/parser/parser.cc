#include <stdio.h>
#include "bnfc/Parser.H"
#include "bnfc/Printer.H"
#include "bnfc/Absyn.H"
#include "ConnectSixPositionParser.h"
#include "ConnectSixPosition.h"


void printBoard(ConnectSixPosition* position) {
    for (int i = 0 ; i < ConnectSix::BOARD_SIZE ; ++i) {
        for (int j = 0 ; j < ConnectSix::BOARD_SIZE ; ++j) {
            int temp = position->board[i][j];
            char s = '!';
            if (temp == 0) {
                s  = 'W';
            } else if (temp == 1) {
                s = 'B';
            } else if (temp == 2) {
                s = '.';
            }
            printf("%c", s);
        }
        printf("\n");
    }    
}

void printMoves(bool moves[][ConnectSix::BOARD_SIZE]) {
    for (int i = 0 ; i < ConnectSix::BOARD_SIZE ; ++i) {
        for (int j = 0 ; j < ConnectSix::BOARD_SIZE ; ++j) {
            if (moves[i][j]) {
                printf("%c", 'X');
            } else {
                printf("%c", '.');
            }            
        }
        printf("\n");
    }
}


void printPosition(ConnectSixPosition* cs) {
    printf("POSITION: \n");
    printBoard(cs);
    printf("PROMOTED MOVES \n");
    printMoves(cs->promotedMoves);
    printf("WRONG MOVES \n");
    printMoves(cs->wrongMoves);
}

int main(int argc, char ** argv)
{
    if (argc > 1)  {
	ConnectSixPositionParser parser;
	vector<ConnectSixPosition*> cs = parser.parse(argv[1]);
        for (int i = 0 ; i < cs.size() ; ++i) {
            printPosition(cs[i]);
            printf("\n");
        }
	
    } else {
	printf("Usage: ./parser position_file_path\n");      
    }
    return 0;
}

