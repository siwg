#ifndef POSITION_VISITOR_HEADER
#define POSITION_VISITOR_HEADER

#include <string>
#include <vector>
#include "Absyn.H"
#include "../../ConnectSix.h"
#include "../ConnectSixPosition.h"





class ConnectSixPositionVisitor : public Visitor {

    
public:
    ConnectSixPositionVisitor();
  
    void visitConnectSixGame(ConnectSixGame *p);
    void visitFieldContent(FieldContent *p);
    void visitFieldBlock(FieldBlock *p);
    void visitBoardRow(BoardRow *p);
    void visitBoard(Board *p);
    void visitSideToMove(SideToMove *p);
    void visitConnectSixL(ConnectSixL *p);
    void visitFieldContentEmptyL(FieldContentEmptyL *p);
    void visitFieldContentWhiteL(FieldContentWhiteL *p);
    void visitFieldContentBlackL(FieldContentBlackL *p);
    void visitFieldBlockL(FieldBlockL *p);
    void visitBoardRowL(BoardRowL *p);
    void visitBoardL(BoardL *p);
    void visitSideToMoveWL(SideToMoveWL *p);
    void visitSideToMoveWWL(SideToMoveWWL *p);
    void visitSideToMoveBL(SideToMoveBL *p);
    void visitSideToMoveBBL(SideToMoveBBL *p);
    void visitListFieldBlock(ListFieldBlock *p);
    void visitListBoardRow(ListBoardRow *p);
    void visitRowMoves(RowMoves *p);
    void visitFieldRange(FieldRange *p);
    void visitMoves(Moves *p);
    void visitFieldBlockSingleL(FieldBlockSingleL *p);
    void visitRowMovesL(RowMovesL *p);
    void visitFieldRangeSingleL(FieldRangeSingleL *p);
    void visitFieldRangeL(FieldRangeL *p);
    void visitMovesL(MovesL *p);
    void visitListFieldRange(ListFieldRange *p);
    void visitListRowMoves(ListRowMoves *p);
    void visitConnectSixPositions(ConnectSixPositions *p);
    void visitConnectSixPositionsL(ConnectSixPositionsL *p);
    void visitListConnectSixGame(ListConnectSixGame *p);
    
    void visitInteger(Integer x);
    void visitChar(Char x);
    void visitDouble(Double x);
    void visitString(String x);
    void visitIdent(Ident x);
   
    vector<ConnectSixPosition*> getPositions();

    
    enum SideToMoveEnum {W, WW, B, BB};
    

  
private:

    enum MovesType {PROMO, WRONG};
    
    vector<ConnectSixPosition*> positions;
    ConnectSixPosition* currentPosition;
    
    int currentRowNr;
    int usedRowCells;
    int whitePieceNumber;
    int blackPieceNumber;
    
    int currentPieceColor;
    
    SideToMoveEnum sideToMove;

    MovesType movesType;

    void validateRowBlock(FieldBlockL* p) const;
    void validateRowIndex(int rowIndex) const;
    void validateColIndex(int colIndex) const;
    void validateRowNumber(int rowNumber) const;
    void validateRowSize(int rowSize) const;
    void validatePieceNumber();
    void validateSideToMove();
    void addPieces(int number);
    void validateMove(int col, int row, MovesType moveType);
    void markSingleMove(int col);
    void clearState();
    void writeCurrentPositionInfo() const;
    void writeCurrentRowInfo() const;
    
    
};


#endif
