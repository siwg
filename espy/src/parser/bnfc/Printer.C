/*** BNFC-Generated Pretty Printer and Abstract Syntax Viewer ***/

#include <string>
#include "Printer.H"

//You may wish to change render
void PrintAbsyn::render(Char c)
{
  if (c == '{')
  {
     bufAppend('\n');
     indent();
     bufAppend(c);
     _n_ = _n_ + 2;
     bufAppend('\n');
     indent();
  }
  else if (c == '(' || c == '[')
     bufAppend(c);
  else if (c == ')' || c == ']')
  {
     backup();
     bufAppend(c);
     bufAppend(' ');
  }
  else if (c == '}')
  {
     _n_ = _n_ - 2;
     backup();
     backup();
     bufAppend(c);
     bufAppend('\n');
     indent();
  }
  else if (c == ',')
  {
     backup();
     bufAppend(c);
     bufAppend(' ');
  }
  else if (c == ';')
  {
     backup();
     bufAppend(c);
     bufAppend('\n');
     indent();
  }
  else if (c == 0) return;
  else
  {
     bufAppend(c);
     bufAppend(' ');
  }
}
void PrintAbsyn::render(String s_)
{
  const char *s = s_.c_str() ;
  if(strlen(s) > 0)
  {
    bufAppend(s);
    bufAppend(' ');
  }
}
void PrintAbsyn::indent()
{
  int n = _n_;
  while (n > 0)
  {
    bufAppend(' ');
    n--;
  }
}
void PrintAbsyn::backup()
{
  if (buf_[cur_ - 1] == ' ')
  {
    buf_[cur_ - 1] = 0;
    cur_--;
  }
}
PrintAbsyn::PrintAbsyn(void)
{
  _i_ = 0; _n_ = 0;
  buf_ = 0;
  bufReset();
}

PrintAbsyn::~PrintAbsyn(void)
{
}

char* PrintAbsyn::print(Visitable *v)
{
  _i_ = 0; _n_ = 0;
  bufReset();
  v->accept(this);
  return buf_;
}
void PrintAbsyn::visitConnectSixPositions(ConnectSixPositions*p) {} //abstract class

void PrintAbsyn::visitConnectSixPositionsL(ConnectSixPositionsL* p)
{
  int oldi = _i_;
  if (oldi > 0) render(_L_PAREN);

  if(p->listconnectsixgame_) {_i_ = 0; p->listconnectsixgame_->accept(this);}
  if (oldi > 0) render(_R_PAREN);

  _i_ = oldi;
}

void PrintAbsyn::visitListConnectSixGame(ListConnectSixGame *listconnectsixgame)
{
  for (ListConnectSixGame::const_iterator i = listconnectsixgame->begin() ; i != listconnectsixgame->end() ; ++i)
  {
    (*i)->accept(this);
    if (i != listconnectsixgame->end() - 1) render("");
  }
}

void PrintAbsyn::visitConnectSixGame(ConnectSixGame*p) {} //abstract class

void PrintAbsyn::visitConnectSixL(ConnectSixL* p)
{
  int oldi = _i_;
  if (oldi > 0) render(_L_PAREN);

  render('{');
  _i_ = 0; p->board_->accept(this);
  render('}');
  _i_ = 0; p->sidetomove_->accept(this);
  _i_ = 0; p->moves_1->accept(this);
  _i_ = 0; p->moves_2->accept(this);

  if (oldi > 0) render(_R_PAREN);

  _i_ = oldi;
}

void PrintAbsyn::visitFieldContent(FieldContent*p) {} //abstract class

void PrintAbsyn::visitFieldContentEmptyL(FieldContentEmptyL* p)
{
  int oldi = _i_;
  if (oldi > 0) render(_L_PAREN);

  render('E');

  if (oldi > 0) render(_R_PAREN);

  _i_ = oldi;
}

void PrintAbsyn::visitFieldContentWhiteL(FieldContentWhiteL* p)
{
  int oldi = _i_;
  if (oldi > 0) render(_L_PAREN);

  render('W');

  if (oldi > 0) render(_R_PAREN);

  _i_ = oldi;
}

void PrintAbsyn::visitFieldContentBlackL(FieldContentBlackL* p)
{
  int oldi = _i_;
  if (oldi > 0) render(_L_PAREN);

  render('B');

  if (oldi > 0) render(_R_PAREN);

  _i_ = oldi;
}

void PrintAbsyn::visitFieldBlock(FieldBlock*p) {} //abstract class

void PrintAbsyn::visitFieldBlockL(FieldBlockL* p)
{
  int oldi = _i_;
  if (oldi > 0) render(_L_PAREN);

  _i_ = 0; p->fieldcontent_->accept(this);
  visitInteger(p->integer_);

  if (oldi > 0) render(_R_PAREN);

  _i_ = oldi;
}

void PrintAbsyn::visitFieldBlockSingleL(FieldBlockSingleL* p)
{
  int oldi = _i_;
  if (oldi > 0) render(_L_PAREN);

  _i_ = 0; p->fieldcontent_->accept(this);

  if (oldi > 0) render(_R_PAREN);

  _i_ = oldi;
}

void PrintAbsyn::visitListFieldBlock(ListFieldBlock *listfieldblock)
{
  for (ListFieldBlock::const_iterator i = listfieldblock->begin() ; i != listfieldblock->end() ; ++i)
  {
    (*i)->accept(this);
    if (i != listfieldblock->end() - 1) render(',');
  }
}

void PrintAbsyn::visitBoardRow(BoardRow*p) {} //abstract class

void PrintAbsyn::visitBoardRowL(BoardRowL* p)
{
  int oldi = _i_;
  if (oldi > 0) render(_L_PAREN);

  if(p->listfieldblock_) {_i_ = 0; p->listfieldblock_->accept(this);}
  if (oldi > 0) render(_R_PAREN);

  _i_ = oldi;
}

void PrintAbsyn::visitListBoardRow(ListBoardRow *listboardrow)
{
  for (ListBoardRow::const_iterator i = listboardrow->begin() ; i != listboardrow->end() ; ++i)
  {
    (*i)->accept(this);
    if (i != listboardrow->end() - 1) render('|');
  }
}

void PrintAbsyn::visitBoard(Board*p) {} //abstract class

void PrintAbsyn::visitBoardL(BoardL* p)
{
  int oldi = _i_;
  if (oldi > 0) render(_L_PAREN);

  if(p->listboardrow_) {_i_ = 0; p->listboardrow_->accept(this);}
  if (oldi > 0) render(_R_PAREN);

  _i_ = oldi;
}

void PrintAbsyn::visitSideToMove(SideToMove*p) {} //abstract class

void PrintAbsyn::visitSideToMoveWL(SideToMoveWL* p)
{
  int oldi = _i_;
  if (oldi > 0) render(_L_PAREN);

  render('w');

  if (oldi > 0) render(_R_PAREN);

  _i_ = oldi;
}

void PrintAbsyn::visitSideToMoveWWL(SideToMoveWWL* p)
{
  int oldi = _i_;
  if (oldi > 0) render(_L_PAREN);

  render("ww");

  if (oldi > 0) render(_R_PAREN);

  _i_ = oldi;
}

void PrintAbsyn::visitSideToMoveBL(SideToMoveBL* p)
{
  int oldi = _i_;
  if (oldi > 0) render(_L_PAREN);

  render('b');

  if (oldi > 0) render(_R_PAREN);

  _i_ = oldi;
}

void PrintAbsyn::visitSideToMoveBBL(SideToMoveBBL* p)
{
  int oldi = _i_;
  if (oldi > 0) render(_L_PAREN);

  render("bb");

  if (oldi > 0) render(_R_PAREN);

  _i_ = oldi;
}

void PrintAbsyn::visitRowMoves(RowMoves*p) {} //abstract class

void PrintAbsyn::visitRowMovesL(RowMovesL* p)
{
  int oldi = _i_;
  if (oldi > 0) render(_L_PAREN);

  visitInteger(p->integer_);
  render("-->");
  if(p->listfieldrange_) {_i_ = 0; p->listfieldrange_->accept(this);}
  if (oldi > 0) render(_R_PAREN);

  _i_ = oldi;
}

void PrintAbsyn::visitFieldRange(FieldRange*p) {} //abstract class

void PrintAbsyn::visitFieldRangeSingleL(FieldRangeSingleL* p)
{
  int oldi = _i_;
  if (oldi > 0) render(_L_PAREN);

  visitInteger(p->integer_);

  if (oldi > 0) render(_R_PAREN);

  _i_ = oldi;
}

void PrintAbsyn::visitFieldRangeL(FieldRangeL* p)
{
  int oldi = _i_;
  if (oldi > 0) render(_L_PAREN);

  render('[');
  visitInteger(p->integer_1);
  render("..");
  visitInteger(p->integer_2);
  render(']');

  if (oldi > 0) render(_R_PAREN);

  _i_ = oldi;
}

void PrintAbsyn::visitListFieldRange(ListFieldRange *listfieldrange)
{
  for (ListFieldRange::const_iterator i = listfieldrange->begin() ; i != listfieldrange->end() ; ++i)
  {
    (*i)->accept(this);
    if (i != listfieldrange->end() - 1) render(',');
  }
}

void PrintAbsyn::visitMoves(Moves*p) {} //abstract class

void PrintAbsyn::visitMovesL(MovesL* p)
{
  int oldi = _i_;
  if (oldi > 0) render(_L_PAREN);

  render('{');
  if(p->listrowmoves_) {_i_ = 0; p->listrowmoves_->accept(this);}  render('}');

  if (oldi > 0) render(_R_PAREN);

  _i_ = oldi;
}

void PrintAbsyn::visitListRowMoves(ListRowMoves *listrowmoves)
{
  for (ListRowMoves::const_iterator i = listrowmoves->begin() ; i != listrowmoves->end() ; ++i)
  {
    (*i)->accept(this);
    if (i != listrowmoves->end() - 1) render('|');
  }
}

void PrintAbsyn::visitInteger(Integer i)
{
  char tmp[16];
  sprintf(tmp, "%d", i);
  bufAppend(tmp);
}
void PrintAbsyn::visitDouble(Double d)
{
  char tmp[16];
  sprintf(tmp, "%g", d);
  bufAppend(tmp);
}
void PrintAbsyn::visitChar(Char c)
{
  bufAppend('\'');
  bufAppend(c);
  bufAppend('\'');
}
void PrintAbsyn::visitString(String s_)
{
  const char *s = s_.c_str() ;
  bufAppend('\"');
  bufAppend(s);
  bufAppend('\"');
}
void PrintAbsyn::visitIdent(String s_)
{
  const char *s = s_.c_str() ;
  render(s);
}

ShowAbsyn::ShowAbsyn(void)
{
  buf_ = 0;
  bufReset();
}

ShowAbsyn::~ShowAbsyn(void)
{
}

char* ShowAbsyn::show(Visitable *v)
{
  bufReset();
  v->accept(this);
  return buf_;
}
void ShowAbsyn::visitConnectSixPositions(ConnectSixPositions* p) {} //abstract class

void ShowAbsyn::visitConnectSixPositionsL(ConnectSixPositionsL* p)
{
  bufAppend('(');
  bufAppend("ConnectSixPositionsL");
  bufAppend(' ');
  bufAppend('[');
  if (p->listconnectsixgame_)  p->listconnectsixgame_->accept(this);
  bufAppend(']');
  bufAppend(')');
}
void ShowAbsyn::visitListConnectSixGame(ListConnectSixGame *listconnectsixgame)
{
  for (ListConnectSixGame::const_iterator i = listconnectsixgame->begin() ; i != listconnectsixgame->end() ; ++i)
  {
    (*i)->accept(this);
    if (i != listconnectsixgame->end() - 1) bufAppend(", ");
  }
}

void ShowAbsyn::visitConnectSixGame(ConnectSixGame* p) {} //abstract class

void ShowAbsyn::visitConnectSixL(ConnectSixL* p)
{
  bufAppend('(');
  bufAppend("ConnectSixL");
  bufAppend(' ');
  bufAppend('[');
  if (p->board_)  p->board_->accept(this);
  bufAppend(']');
  bufAppend(' ');
  bufAppend('[');
  if (p->sidetomove_)  p->sidetomove_->accept(this);
  bufAppend(']');
  bufAppend(' ');
  p->moves_1->accept(this);
  bufAppend(' ');
  p->moves_2->accept(this);
  bufAppend(')');
}
void ShowAbsyn::visitFieldContent(FieldContent* p) {} //abstract class

void ShowAbsyn::visitFieldContentEmptyL(FieldContentEmptyL* p)
{
  bufAppend("FieldContentEmptyL");
}
void ShowAbsyn::visitFieldContentWhiteL(FieldContentWhiteL* p)
{
  bufAppend("FieldContentWhiteL");
}
void ShowAbsyn::visitFieldContentBlackL(FieldContentBlackL* p)
{
  bufAppend("FieldContentBlackL");
}
void ShowAbsyn::visitFieldBlock(FieldBlock* p) {} //abstract class

void ShowAbsyn::visitFieldBlockL(FieldBlockL* p)
{
  bufAppend('(');
  bufAppend("FieldBlockL");
  bufAppend(' ');
  bufAppend('[');
  if (p->fieldcontent_)  p->fieldcontent_->accept(this);
  bufAppend(']');
  bufAppend(' ');
  visitInteger(p->integer_);
  bufAppend(')');
}
void ShowAbsyn::visitFieldBlockSingleL(FieldBlockSingleL* p)
{
  bufAppend('(');
  bufAppend("FieldBlockSingleL");
  bufAppend(' ');
  bufAppend('[');
  if (p->fieldcontent_)  p->fieldcontent_->accept(this);
  bufAppend(']');
  bufAppend(')');
}
void ShowAbsyn::visitListFieldBlock(ListFieldBlock *listfieldblock)
{
  for (ListFieldBlock::const_iterator i = listfieldblock->begin() ; i != listfieldblock->end() ; ++i)
  {
    (*i)->accept(this);
    if (i != listfieldblock->end() - 1) bufAppend(", ");
  }
}

void ShowAbsyn::visitBoardRow(BoardRow* p) {} //abstract class

void ShowAbsyn::visitBoardRowL(BoardRowL* p)
{
  bufAppend('(');
  bufAppend("BoardRowL");
  bufAppend(' ');
  bufAppend('[');
  if (p->listfieldblock_)  p->listfieldblock_->accept(this);
  bufAppend(']');
  bufAppend(')');
}
void ShowAbsyn::visitListBoardRow(ListBoardRow *listboardrow)
{
  for (ListBoardRow::const_iterator i = listboardrow->begin() ; i != listboardrow->end() ; ++i)
  {
    (*i)->accept(this);
    if (i != listboardrow->end() - 1) bufAppend(", ");
  }
}

void ShowAbsyn::visitBoard(Board* p) {} //abstract class

void ShowAbsyn::visitBoardL(BoardL* p)
{
  bufAppend('(');
  bufAppend("BoardL");
  bufAppend(' ');
  bufAppend('[');
  if (p->listboardrow_)  p->listboardrow_->accept(this);
  bufAppend(']');
  bufAppend(')');
}
void ShowAbsyn::visitSideToMove(SideToMove* p) {} //abstract class

void ShowAbsyn::visitSideToMoveWL(SideToMoveWL* p)
{
  bufAppend("SideToMoveWL");
}
void ShowAbsyn::visitSideToMoveWWL(SideToMoveWWL* p)
{
  bufAppend("SideToMoveWWL");
}
void ShowAbsyn::visitSideToMoveBL(SideToMoveBL* p)
{
  bufAppend("SideToMoveBL");
}
void ShowAbsyn::visitSideToMoveBBL(SideToMoveBBL* p)
{
  bufAppend("SideToMoveBBL");
}
void ShowAbsyn::visitRowMoves(RowMoves* p) {} //abstract class

void ShowAbsyn::visitRowMovesL(RowMovesL* p)
{
  bufAppend('(');
  bufAppend("RowMovesL");
  bufAppend(' ');
  visitInteger(p->integer_);
  bufAppend(' ');
  bufAppend('[');
  if (p->listfieldrange_)  p->listfieldrange_->accept(this);
  bufAppend(']');
  bufAppend(')');
}
void ShowAbsyn::visitFieldRange(FieldRange* p) {} //abstract class

void ShowAbsyn::visitFieldRangeSingleL(FieldRangeSingleL* p)
{
  bufAppend('(');
  bufAppend("FieldRangeSingleL");
  bufAppend(' ');
  visitInteger(p->integer_);
  bufAppend(')');
}
void ShowAbsyn::visitFieldRangeL(FieldRangeL* p)
{
  bufAppend('(');
  bufAppend("FieldRangeL");
  bufAppend(' ');
  visitInteger(p->integer_1);
  bufAppend(' ');
  visitInteger(p->integer_2);
  bufAppend(' ');
  bufAppend(')');
}
void ShowAbsyn::visitListFieldRange(ListFieldRange *listfieldrange)
{
  for (ListFieldRange::const_iterator i = listfieldrange->begin() ; i != listfieldrange->end() ; ++i)
  {
    (*i)->accept(this);
    if (i != listfieldrange->end() - 1) bufAppend(", ");
  }
}

void ShowAbsyn::visitMoves(Moves* p) {} //abstract class

void ShowAbsyn::visitMovesL(MovesL* p)
{
  bufAppend('(');
  bufAppend("MovesL");
  bufAppend(' ');
  bufAppend('[');
  if (p->listrowmoves_)  p->listrowmoves_->accept(this);
  bufAppend(']');
  bufAppend(' ');
  bufAppend(')');
}
void ShowAbsyn::visitListRowMoves(ListRowMoves *listrowmoves)
{
  for (ListRowMoves::const_iterator i = listrowmoves->begin() ; i != listrowmoves->end() ; ++i)
  {
    (*i)->accept(this);
    if (i != listrowmoves->end() - 1) bufAppend(", ");
  }
}

void ShowAbsyn::visitInteger(Integer i)
{
  char tmp[16];
  sprintf(tmp, "%d", i);
  bufAppend(tmp);
}
void ShowAbsyn::visitDouble(Double d)
{
  char tmp[16];
  sprintf(tmp, "%g", d);
  bufAppend(tmp);
}
void ShowAbsyn::visitChar(Char c)
{
  bufAppend('\'');
  bufAppend(c);
  bufAppend('\'');
}
void ShowAbsyn::visitString(String s_)
{
  const char *s = s_.c_str() ;
  bufAppend('\"');
  bufAppend(s);
  bufAppend('\"');
}
void ShowAbsyn::visitIdent(String s_)
{
  const char *s = s_.c_str() ;
  bufAppend('\"');
  bufAppend(s);
  bufAppend('\"');
}

