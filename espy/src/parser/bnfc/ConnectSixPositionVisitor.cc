#include <cmath>
#include <stdexcept>
#include <stdio.h>
#include <stdlib.h>
#include "Printer.H"
#include "ConnectSixPositionVisitor.h"


ConnectSixPositionVisitor::ConnectSixPositionVisitor() : positions() {
    clearState();
}

void ConnectSixPositionVisitor::clearState() {
    currentPieceColor = 0;
    usedRowCells = 0;
    whitePieceNumber = 0;
    blackPieceNumber = 0;
    sideToMove = W;
    currentRowNr = 0;
    movesType = PROMO;
    currentPosition = new ConnectSixPosition();
}

void ConnectSixPositionVisitor::visitConnectSixPositions(ConnectSixPositions* p) {
    p->accept(this);
}

void ConnectSixPositionVisitor::visitConnectSixGame(ConnectSixGame* p) {
    p->accept(this);
}

void ConnectSixPositionVisitor::visitFieldContent(FieldContent* p) {
    p->accept(this);
}

void ConnectSixPositionVisitor::visitFieldBlock(FieldBlock* p) {
    p->accept(this);
}

void ConnectSixPositionVisitor::visitBoardRow(BoardRow* p) {
    p->accept(this);
}
    
void ConnectSixPositionVisitor::visitBoard(Board* p) {
    p->accept(this);
}

void ConnectSixPositionVisitor::visitSideToMove(SideToMove* p) {
    p->accept(this);
}

void ConnectSixPositionVisitor::visitRowMoves(RowMoves* p) {
    p->accept(this);
}


void ConnectSixPositionVisitor::visitFieldRange(FieldRange* p) {
    p->accept(this);
}

void ConnectSixPositionVisitor::visitMoves(Moves* p) {
    p->accept(this);
}

void ConnectSixPositionVisitor::visitConnectSixL(ConnectSixL* p) {
    p->board_->accept(this);
    p->sidetomove_->accept(this);
    validateRowNumber(currentRowNr + 1);
    currentRowNr = -1;
    validatePieceNumber();
    validateSideToMove();    
    p->moves_1->accept(this);
    p->moves_2->accept(this);
    positions.push_back(currentPosition);
    clearState();    
}

void ConnectSixPositionVisitor::visitFieldContentEmptyL(FieldContentEmptyL* p) {
    currentPieceColor = ConnectSix::EMPTY;
}

void ConnectSixPositionVisitor::visitFieldContentWhiteL(FieldContentWhiteL* p) {
    currentPieceColor = ConnectSix::WHITE;
}
    
void ConnectSixPositionVisitor::visitFieldContentBlackL(FieldContentBlackL* p) {
    currentPieceColor = ConnectSix::BLACK;
}

void ConnectSixPositionVisitor::visitFieldBlockL(FieldBlockL* p) {
    p->fieldcontent_->accept(this);
    validateRowBlock(p);
    for (int i = usedRowCells ; i < usedRowCells + p->integer_ ; ++i) {
        currentPosition->board[currentRowNr][i] = currentPieceColor;
    }
    usedRowCells += p->integer_;
    addPieces(p->integer_);
}

void ConnectSixPositionVisitor::visitBoardRowL(BoardRowL* p) {
    p->listfieldblock_->accept(this);
}

void ConnectSixPositionVisitor::visitBoardL(BoardL* p) {
    p->listboardrow_->accept(this);
}

void ConnectSixPositionVisitor::visitSideToMoveWL(SideToMoveWL* p) {
    sideToMove = W;
}
    
void ConnectSixPositionVisitor::visitSideToMoveWWL(SideToMoveWWL* p) {
    sideToMove = WW;
}

void ConnectSixPositionVisitor::visitSideToMoveBL(SideToMoveBL* p) {
    sideToMove = B;
}
    
void ConnectSixPositionVisitor::visitSideToMoveBBL(SideToMoveBBL* p) {
    sideToMove = BB;
}

void ConnectSixPositionVisitor::visitListFieldBlock(ListFieldBlock* p) {
    ListFieldBlock::iterator it;
    for (it = p->begin() ; it < p->end(); it++) {
      (*it)->accept(this); 
    }
}
    
void ConnectSixPositionVisitor::visitListBoardRow(ListBoardRow* p) {
    ListBoardRow::iterator it;
for (it = p->begin() ; it < p->end(); it++) {
        (*it)->accept(this);
        validateRowSize(usedRowCells);
        usedRowCells = 0;
        if (it < p->end() - 1) {
            ++currentRowNr;
            validateRowIndex(currentRowNr);
        }
    }
}
  
void ConnectSixPositionVisitor::validateRowSize(int rowSize) const {
    if (rowSize != ConnectSix::BOARD_SIZE) {
        char sign[100];
        sprintf(sign, "row size should be %d instead of %d\n", ConnectSix::BOARD_SIZE, rowSize);
	throw std::runtime_error(sign);
    }
}

void ConnectSixPositionVisitor::visitFieldBlockSingleL(FieldBlockSingleL* p) {
    p->fieldcontent_->accept(this);
    if (usedRowCells + 1 > ConnectSix::BOARD_SIZE) {
        char sign[100];
        sprintf(sign, "Wrong row size \n");
        throw std::runtime_error(sign);
    }
    currentPosition->board[currentRowNr][usedRowCells++] = currentPieceColor;
    addPieces(1);    
}

void ConnectSixPositionVisitor::visitRowMovesL(RowMovesL* p) {
    validateRowIndex(p->integer_ - 1);
    currentRowNr = p->integer_ - 1;
    p->listfieldrange_->accept(this);
}

void ConnectSixPositionVisitor::visitFieldRangeSingleL(FieldRangeSingleL* p) {
    markSingleMove(p->integer_ - 1);
}

void ConnectSixPositionVisitor::visitFieldRangeL(FieldRangeL* p) {
    validateColIndex(p->integer_1 - 1);
    validateColIndex(p->integer_2 - 1);
    if (p->integer_2 < p->integer_1) {
        char sign[100];
        sprintf(sign, "Wrong range [%d..%d]\n", p->integer_1, p->integer_2);
        throw new std::runtime_error(sign);
    }
    for (int i = p->integer_1 - 1 ; i < p->integer_2 ; ++i) {
        markSingleMove(i);
    }
}

void ConnectSixPositionVisitor::visitMovesL(MovesL* p) {
    p->listrowmoves_->accept(this);
    if (movesType == PROMO) {
        movesType = WRONG;
    }
}

void ConnectSixPositionVisitor::visitListFieldRange(ListFieldRange* p) {
    ListFieldRange::iterator it;
    for (it = p->begin() ; it < p->end(); it++) {
      (*it)->accept(this);
    }
}

void ConnectSixPositionVisitor::visitListRowMoves(ListRowMoves* p) {
    ListRowMoves::iterator it;
    for (it = p->begin() ; it < p->end(); it++) {
      (*it)->accept(this);
    }
}


void ConnectSixPositionVisitor::validateColIndex(int colIndex) const {
    if (colIndex < 0 || colIndex >= ConnectSix::BOARD_SIZE) {
        char sign[100];
        sprintf(sign, "Wrong column index %d\n", colIndex + 1);
        throw std::runtime_error(sign);
    }
}

void ConnectSixPositionVisitor::validateMove(int col, int row, ConnectSixPositionVisitor::MovesType moveType) {
    validateColIndex(col);
    validateRowIndex(row);
    char sign[100];
    if (moveType == PROMO) {
        if (currentPosition->wrongMoves[row][col]) {
            sprintf(sign, "Move row : %d, col : %d was already declared as wrong move \n", row + 1, col + 1);
            throw std::runtime_error(sign);
        }
    } else if (moveType == WRONG) {
        if (currentPosition->promotedMoves[row][col]) {
            sprintf(sign, "move row : %d, col : %d was already declared as promoted move \n", row + 1, col + 1);
            throw std::runtime_error(sign);
        }
    } else {
        sprintf(sign, "wrong moves type\n");
        throw std::runtime_error(sign);
    }
}

void ConnectSixPositionVisitor::markSingleMove(int col) {
    validateMove(col, currentRowNr, movesType);
    if (movesType == PROMO) {
        currentPosition->promotedMoves[currentRowNr][col] = true;
    } else if (movesType == WRONG) {
        currentPosition->wrongMoves[currentRowNr][col] = true;
    } else {
        throw std::runtime_error("wrong moves type\n");
    }
}

void ConnectSixPositionVisitor::writeCurrentPositionInfo() const {
    fprintf(stderr, "Error in Game Position nr: %d \n", (int)positions.size() + 1);
}

void ConnectSixPositionVisitor::writeCurrentRowInfo() const {
    fprintf(stderr, "Error in row nr %d, \n", currentRowNr + 1);
}

void ConnectSixPositionVisitor::visitConnectSixPositionsL(ConnectSixPositionsL* p) {
    try {
        p->listconnectsixgame_->accept(this);
    } catch (std::runtime_error &e) {
        writeCurrentPositionInfo();
        if (currentRowNr != -1) {
            writeCurrentRowInfo();
        }
        fprintf(stderr, "%s\n", e.what());
        throw e;
    }
}

void ConnectSixPositionVisitor::visitListConnectSixGame(ListConnectSixGame* p) {
    ListConnectSixGame::iterator it;
    for (it = p->begin() ; it < p->end(); it++) {
      (*it)->accept(this);
    }
}

vector< ConnectSixPosition* > ConnectSixPositionVisitor::getPositions() {
    return positions;
}

void ConnectSixPositionVisitor::visitInteger(Integer x) {
}

void ConnectSixPositionVisitor::visitChar(Char x) {
}

void ConnectSixPositionVisitor::visitDouble(Double x) {
}

void ConnectSixPositionVisitor::visitString(String x) {
}

void ConnectSixPositionVisitor::visitIdent(Ident x) {
}

void ConnectSixPositionVisitor::validateRowBlock(FieldBlockL* p) const {
    if (p->integer_ <= 0 || p->integer_ + usedRowCells > ConnectSix::BOARD_SIZE) {
        throw std::runtime_error("Field block size is incorrect\n");
    }
}

void ConnectSixPositionVisitor::validateRowIndex(int rowIndex) const {
    if (rowIndex < 0 || rowIndex >= ConnectSix::BOARD_SIZE) {
        char sign[100];
        sprintf(sign, "There are to many rows defined rowNumber =%d \n", rowIndex + 1);
        throw std::runtime_error(sign);
    }
}

void ConnectSixPositionVisitor::validateRowNumber(int rowNumber) const {
    if (rowNumber != ConnectSix::BOARD_SIZE) {
        char sign[100];
        sprintf(sign, "There is incorrect number (%d) of rows. Correct number of rows is  %d\n",rowNumber,  ConnectSix::BOARD_SIZE);
        throw std::runtime_error(sign);
    }
}

void ConnectSixPositionVisitor::validatePieceNumber() {
    char sign[150];
    if (abs(whitePieceNumber - blackPieceNumber) > 1) {
        sprintf(sign, "Incorrect piece number on the board. One player has more pieces than he should. whtie pieces :  %d, black pieces : %d \n", whitePieceNumber, blackPieceNumber);
        throw std::runtime_error(sign);

    }
    if (whitePieceNumber == 0 && blackPieceNumber == 1) {
        throw std::runtime_error("White player should move first\n");
    }
    if ((whitePieceNumber % 2 == 0 && blackPieceNumber != whitePieceNumber) ||
        (blackPieceNumber % 2 == 1 && blackPieceNumber != whitePieceNumber)) {
        sprintf(sign, "Incorrect piece number on the board. whtie pieces :  %d, black pieces : %d \n", whitePieceNumber, blackPieceNumber);
        throw std::runtime_error(sign);
    }
}

void ConnectSixPositionVisitor::validateSideToMove() {
    if (whitePieceNumber == 0 && blackPieceNumber == 0) {
        if(sideToMove != W) {
            throw std::runtime_error("Side to move should be w \n");
        }
    } else if (whitePieceNumber > blackPieceNumber && sideToMove != BB) {
        throw std::runtime_error("Side to move should be bb\n");
    } else if (blackPieceNumber > whitePieceNumber && sideToMove != WW) {
        throw std::runtime_error("Side to move should be ww\n");
    } else if (whitePieceNumber == blackPieceNumber) {
        if (whitePieceNumber % 2 == 0 && sideToMove != W) {
            throw std::runtime_error("Side to move should be w\n");
        } else if (blackPieceNumber % 2 == 1 && sideToMove != B) {
            printf("black pieces number = %d white = %d\n", blackPieceNumber, whitePieceNumber);
            throw std::runtime_error("Side to move should be b\n");
        }
    }
}

void ConnectSixPositionVisitor::addPieces(int number) {
    if (currentPieceColor == ConnectSix::BLACK) {
        blackPieceNumber += number;
    } else if (currentPieceColor == ConnectSix::WHITE) {
        whitePieceNumber += number;
    }
}


