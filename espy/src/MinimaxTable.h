/**
 * \file
 *
 * Implementation of minimax search.
 *
 */

#ifndef __MinimaxTable__
#define __MinimaxTable__

#include "Search.h"
#include "Settings.h"
#include "Transposition.h"

template<class G>
class MinimaxTable : public Search<G> {

	const static int max_depth = Settings::maximum_game_length; /**< Maximum search depth. */

	typedef typename Transposition<G>::Key Key;
	
	G g;
	Transposition<G> table;

	int value[max_depth];
	int ply;
	int nds;
	int depth_limit;

	void init() {
		g.prepare_moves();
		ply = 0; nds = 1;
		value[ply] = g.game_state();
		if (g.game_state() == G::in_progress) {
			if (g.wtm()) {
				value[ply] = G::minfty;
			} else {
				value[ply] = G::pinfty;
			}
		}
	}

public:
	MinimaxTable<G>() {
		g.clear();
		depth_limit = max_depth;

		init();
	}

	void search();
	void search_single_node();

	void position(G &);
	int nodes() { return nds; }
	int depth() { return depth_limit; }
	int depth(int _dep) { return depth_limit = _dep; }

	int result() { 
		if (ply > 0 || g.has_next_move()) { return G::in_progress; }

		return value[0]; 
	}
};

template<class G> void MinimaxTable<G>::position(G & position)
{
	g = position;
        init();
}

template<class G> void MinimaxTable<G>::search_single_node()
{
	if (ply == depth_limit) {
		value[ply] = G::unknown; 
		g.retract_move(); 
		ply--;
		
		Key k = g.get_board_hash_key();
		table[k].hash_key = k;
		table[k].type = Transposition<G>::exact_bound;
	}

	bool cutoff = false;

	while ((cutoff || !g.has_next_move()) && ply > 0) {
		g.retract_move(); ply--;

		switch (value[ply+1]) {
		case G::white_wins:
		case G::draw:
		case G::black_wins:
			if (g.wtm()) {
				value[ply] = max(value[ply], value[ply+1]);

				cutoff = value[ply] == G::white_wins;
			} else {
				value[ply] = min(value[ply], value[ply+1]);

				cutoff = value[ply] == G::black_wins;
			}
			break;
		}
		
		Key k = g.get_board_hash_key();
		table[k].hash_key = k;
		table[k].type = Transposition<G>::exact_bound;
	}

	if (g.has_next_move()) {
		g.make_move(g.get_next_move()); ply++; nds++;
		g.prepare_moves();

		Key k = g.get_board_hash_key();
		if(table.exact(k))
		{
		  value[ply] = table[k].bound;
		  g.retract_move(); ply--;
		}
		else
		{
		
		  value[ply] = g.game_state();

		  if (value[ply] == G::in_progress) {
			if (g.wtm()) {
				value[ply] = G::minfty;
			} else {
				value[ply] = G::pinfty;
			}
		  }
		}
	}
}

template<class G> void MinimaxTable<G>::search()
{
	while (result() == G::in_progress) {
		search_single_node();
	}
}

#endif /* __Perft__ */
