#ifndef CPDEFAULTEVENTLISTENER_H_
#define CPDEFAULTEVENTLISTENER_H_

#include "CPEventListener.h"

class CPDefaultEventListener: public CPEventListener {

public:
    virtual void connect(std::string name);
    virtual void bestMove(std::string move);
    virtual void error(std::string message);
    virtual void concede();
    virtual void info(std::map<std::string, std::string> & options);
    virtual void init();
    virtual void startGame(std::string position, CPRole role,
                   int gameTime, int bonusTime);
    virtual void move(CPColor color, std::string move);
    virtual void feedback(bool on);
    virtual void setOption(std::map<std::string, std::string> & options);
    virtual void getMove(CPColor color, int moveTime,
                 int gameTime);
    virtual void forceMove();
    virtual void stop();
    virtual void endGame(CPResult result);
    virtual void quit();
  
};

#endif