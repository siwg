/**
 * \file
 *
 * Implementation of Reversi game.
 *
 * This file is part of espy, the game solver.
 *
 *
 */

#ifndef __Reversi__
#define __Reversi__

#include "Game.h"
#include "Settings.h"
#include <vector>
#include <algorithm>
/*
 * Problems:
 * -ruch musi byc intem
 * -gracz moze grac kilka razy z rzedu.
 */

class Reversi : public Game {

	const static int max_depth = Settings::maximum_game_length;
	const static int black = 0;
	const static int white = 1;
	const static int empty = 2;
	const static int board_size = 4;

	int ply; /* number of moves played*/

	typedef struct mymove {
		int ST;
		int ND;
	} mymove;

	vector<mymove> possible_moves[max_depth];	
	int act_move[max_depth];
	struct board {
		int sq[board_size][board_size];
		int whites, blacks, played, nextplayer; /* who played and who is next */
	} bd;
	board history[max_depth];

public:
	Reversi(){
		clear();	
	};
	
	void clear() {
		ply = 0;
		bd.whites = 2;
		bd.blacks = 2;
		bd.played = 1;
		bd.nextplayer = 0;

		for(int i = 0; i < board_size; i++)
			for(int j = 0; j < board_size; j++)
				bd.sq[i][j] = empty;
		int middle = board_size/2;
		bd.sq[middle-1][middle-1] = white;
		bd.sq[middle][middle-1] = black;
		bd.sq[middle-1][middle] = black;
		bd.sq[middle][middle] = white;
	}
	
	/* ustawiam na poczatkowa pozycje od ktorej bedzie szukal 
	 *
	 */

	void prepare_moves() {
		possible_moves[ply].clear();
		for(int i = 0; i < board_size; ++i)
			for(int j = 0; j < board_size; ++j)
					if(could_play_at(i, j, bd.nextplayer)) {
						mymove mv;
						mv.ST = i;
						mv.ND = j;
						possible_moves[ply].push_back(mv);
					}
		act_move[ply] = 0;
		sort_prepared_moves(bd.nextplayer);
		return ;
	}

	/* sortuje wyznaczone pozycje zgodnie z heurystyką */
	void sort_prepared_moves(int pl) {
		vector< pair<int, mymove> > movesWithRank;
		for(vector<mymove>::iterator it = possible_moves[ply].begin(); it != possible_moves[ply].end(); ++it){
			movesWithRank.push_back(make_pair(calculateRank(*it, pl), *it));
		}

		std::sort(movesWithRank.begin(), movesWithRank.end(), Reversi::compareMovesRanks);

		possible_moves[ply].clear();		

		for(vector< std::pair< int, mymove > >::iterator it = movesWithRank.begin(); it != movesWithRank.end(); ++it){
			possible_moves[ply].push_back(it->second);	
			// cout << it->first << " " << it->second.ST << " " << it->second.ND << "\n";
		}
	}

	/* przechodzi po nastepnych polach az znajdzie taki na ktore moze zagrac */
	bool has_next_move() {
		return (act_move[ply] < possible_moves[ply].size());
	}
	/* to samo co wyzej tylko zwraca zagrane pole
		*/
	move get_next_move() {
		int act = act_move[ply]++;	
		return possible_moves[ply][act].ND*board_size + possible_moves[ply][act].ST;
	}
	void make_move(const move mov) {
		
	    	
		mymove m;
		get_mymove(m, mov);
		save_to_history(ply);
		
		//cout << " ----------------------------- \n";
		//print_board();
		//cout << "Ruch: " << m.ST << " " << m.ND <<  " " << bd.sq[m.ST][m.ND];
		//cout << " " << bd.nextplayer << endl;
		
		++ply;
		game_state(in_progress);
		int col = bd.nextplayer;
		bd.sq[m.ST][m.ND] = col;
		bd.played = bd.nextplayer;
		
		if (bd.played == black) {
				bd.blacks++;
		}
		else {
				bd.whites++;
		}
		int x = m.ST;
		int y = m.ND;
		int x_step[] = { 1, -1, 0, 0, 1, 1, -1, -1};
		int y_step[] = { 0, 0, 1, -1, 1, -1, 1, -1};
		for(int i=0; i < 8; ++i){
			x = m.ST+x_step[i];
			y = m.ND+y_step[i];
			
			if (!in_board(x, y)) continue;
			//check direction
			while(in_board(x, y) && (bd.sq[x][y] == 1 - col)) { 
				x += x_step[i];
				y += y_step[i];
			}
			if (!in_board(x, y) || (bd.sq[x][y] == empty)) continue;
			x -= x_step[i];
			y -= y_step[i];
			while( (x != m.ST) || (y != m.ND)) { 
				bd.sq[x][y] = col;
				x -= x_step[i];
				y -= y_step[i];
				update(col);
			}
		}	
		if (bd.whites == 0) game_state(black_wins);
		if (bd.blacks == 0) game_state(white_wins);
		if (bd.whites + bd.blacks == board_size*board_size) {
			if (bd.whites > bd.blacks)
				game_state(white_wins);
			else if (bd.whites < bd.blacks)
				game_state(black_wins);
			else 
				game_state(draw);
		
		}
		bd.nextplayer = has_next_move(1 - col) ? 1 - col : col;
		//print_board();
	}

	void retract_move() {
		--ply;
		load_from_history(ply);
		//print_board();
	}

	inline int wtm() const {
			return (bd.nextplayer);
	}
	
	void print_board() {
		cout << "Białe: " << bd.whites << " Czarne: " << bd.blacks << endl;
		for(int i = 0; i < board_size; i++) {
			for(int j = 0; j < board_size; j++)
				switch (bd.sq[i][j]) {
					case 0: cout << "o "; break;
					case 1: cout << "x "; break;
					case 2: cout << "_ "; break;
					default: break;
				}
			cout << endl;
		}
		cout << endl;
	}
private:
	bool has_next_move(int pl) {
		for(int i = 0; i < board_size; i++)
			for(int j = 0; j < board_size; j++) {
				if (could_play_at(i, j, pl)) return true;
			}
		return false;
	}
	bool could_play_at(int x, int y, int pl) {

		if (bd.sq[x][y] != empty) return false;	
		int x_step[] = { 1, -1, 0, 0, 1, 1, -1, -1};
		int y_step[] = { 0, 0, 1, -1, 1, -1, 1, -1};
		for(int i = 0; i < 8; ++i){
			int tmpx = x + x_step[i];
			int tmpy = y + y_step[i];
			if (!in_board(tmpx, tmpy) || (bd.sq[tmpx][tmpy] != 1-pl)) continue;
			while(in_board(tmpx += x_step[i], tmpy += y_step[i])) {
				if (bd.sq[tmpx][tmpy] == pl) return true;
			}
		}	
		return false;
	}
	inline bool in_board(int x, int y) {
		return((x >= 0) && (y >= 0) &&( x < board_size) && (y < board_size));
	}
	void get_mymove(mymove &m, move mov) {
		m.ST = mov % board_size;
		m.ND = mov / board_size;
	}
	void load_from_history(int ply) {
		bd.whites = history[ply].whites;
		bd.blacks = history[ply].blacks;
		bd.played = history[ply].played;
		bd.nextplayer = history[ply].nextplayer;
		for(int i = 0; i < board_size; i++)
			for(int j = 0; j < board_size; j++)
				bd.sq[i][j] = history[ply].sq[i][j];
	}
	void save_to_history(int ply){
		history[ply].whites = bd.whites;
		history[ply].blacks = bd.blacks;
		history[ply].played = bd.played;
		history[ply].nextplayer = bd.nextplayer;
		for(int i = 0; i <board_size; i++)
			for(int j = 0; j <board_size; j++)
				history[ply].sq[i][j] = bd.sq[i][j];
	}
	void update(int c) {
		if (c == black) {
			bd.blacks++;
		 bd.whites--;
		} 
		else {
			bd.whites++;
			bd.blacks--;
		}
	}

	static bool compareMovesRanks(const std::pair<int, mymove> a, const std::pair<int, mymove> b){
		return (a.first > b.first);
	}

	/* funcja obliczająca jak dobry jest dany ruch */
	int calculateRank(mymove m, int pl){
		int x = m.ST, y = m.ND;
		int changed = 0, corner = 0, side = 0;		
		if((x == 0 || x == (board_size-1)) && (y == 0 || y == (board_size-1))){
			corner = 1;	
		} else if(x == 0 || x == (board_size-1) || y == 0 || y == (board_size-1)){
				side = 1;	
		}
		int x_step[] = { 1, -1, 0, 0, 1, 1, -1, -1};
		int y_step[] = { 0, 0, 1, -1, 1, -1, 1, -1};
		for(int i = 0; i < 8; ++i){
			int tmpx = x + x_step[i];
			int tmpy = y + y_step[i];
			if (!in_board(tmpx, tmpy) || (bd.sq[tmpx][tmpy] != 1-pl)) continue;
			int tmp_changed = 1;
			while(in_board(tmpx += x_step[i], tmpy += y_step[i])) {
				if (bd.sq[tmpx][tmpy] == pl) {
					changed += tmp_changed;
					break;	
				} else {
					tmp_changed++;
				}
			}
		}	

		return 15*corner + 5*side + changed;
	}

};
#endif
