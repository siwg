/**
 * \file
 *
 * Implementation of minimax search.
 *
 */

#ifndef __ReversiHistoricMinimax__
#define __ReversiHistoricMinimax__

#include "Search.h"
#include "Settings.h"
#include "Reversi.h"

class ReversiHistoricMinimax : public Search<Reversi> {

	const static int max_depth = Settings::maximum_game_length; /**< Maximum search depth. */
 
  const static Reversi::move NO_MOVE = -1;

	Reversi g;

	int value[max_depth];
	int ply;
	int nds;
	int depth_limit;
	
	int white_best_moves[BOARD_SIZE];
	int black_best_moves[BOARD_SIZE];
		
	Reversi::move moves[max_depth];
	bool white_makes_move[max_depth];

  class HistoryComparator{
    ReversiHistoricMinimax * rhm;
  public:
    HistoryComparator(ReversiHistoricMinimax * rhm_){
      rhm = rhm_;
    }
    bool operator ()(const int m1,const int m2){
      if (rhm->g.wtm())
        return rhm->white_best_moves[m1] < rhm->white_best_moves[m2];
      else      
        return rhm->black_best_moves[m1] < rhm->black_best_moves[m2];
    }
  };

  class HistoryHeuristic: public Reversi::MoveSortHeuristic {
    ReversiHistoricMinimax * rhm;
  public:
    HistoryHeuristic(ReversiHistoricMinimax * rhm_){
      rhm = rhm_;
    }
    void sortMoves(Reversi::Board * board, int * legal, int n) {
      std::sort(legal, legal + n, HistoryComparator(rhm));
    }
  };

	void init() {
	  g.setHeuristic(new HistoryHeuristic(this));
		g.prepare_moves();
		ply = 0; nds = 1;
		value[ply] = g.game_state();
		for (int i = 0; i < BOARD_SIZE; ++i){
		  white_best_moves[i] = 0;
		  black_best_moves[i] = 0;
		}
		white_makes_move[0] = true;
		if (g.game_state() == Reversi::in_progress) {
			if (g.wtm()) {
				value[ply] = Reversi::minfty;
			} else {
				value[ply] = Reversi::pinfty;
			}
		}
	}

public:
	ReversiHistoricMinimax() {
		g.clear();
		depth_limit = max_depth;

		init();
	}

	void search();
	void search_single_node();

	void position(Reversi &);
	int nodes() { return nds; }
	int depth() { return depth_limit; }
	int depth(int _dep) { return depth_limit = _dep; }

	int result() { 
		if (ply > 0 || g.has_next_move()) { return Reversi::in_progress; }

		return value[0]; 
	}
};

void ReversiHistoricMinimax::position(Reversi & position)
{
	g = position;
        init();
}

void ReversiHistoricMinimax::search_single_node()
{
	if (ply == depth_limit) {
		value[ply] = Reversi::unknown; 
		g.retract_move(); 
		ply--;
	}

	bool cutoff = false;

	while ((cutoff || !g.has_next_move()) && ply > 0) {
		g.retract_move(); ply--;

		switch (value[ply+1]) {
		case Reversi::white_wins:
		case Reversi::draw:
		case Reversi::black_wins:
			if (g.wtm()) {
				value[ply] = max(value[ply], value[ply+1]);
				cutoff = value[ply] == Reversi::white_wins;
			} else {
				value[ply] = min(value[ply], value[ply+1]);
				cutoff = value[ply] == Reversi::black_wins;
			}
			if (cutoff){
			  if (white_makes_move[ply])
			    white_best_moves[moves[ply]]++;
			  else
			    black_best_moves[moves[ply]]++;
			}
			break;
		}
	}
	
	if (g.has_next_move()) {
	  Reversi::move m = g.get_next_move();
		moves[ply] = m;
		g.make_move(m);
		ply++; nds++;
		white_makes_move[ply] = g.wtm();
		g.prepare_moves();

		value[ply] = g.game_state();

		if (value[ply] == Reversi::in_progress) {
			if (g.wtm()) {
				value[ply] = Reversi::minfty;
			} else {
				value[ply] = Reversi::pinfty;
			}
		}
	}
}

void ReversiHistoricMinimax::search()
{
	while (result() == Reversi::in_progress) {
		search_single_node();
	}
}

#endif /* __ReversiHistoricMinimax__ */
