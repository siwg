#include "PN.h"
#include "TicTacToe.h"
#include "ConnectSix.h"
#include "ConnectFour.h"

using namespace std;

int main() {
/*    TicTacToe t = TicTacToe();
      t.prepare_moves();
    
      while (t.has_next_move()) {
      Game::move m = t.get_next_move();
      printf("Move: %d\n", m);
      t.make_move(m);
      t.prepare_moves();

      while (t.has_next_move()) {
      Game::move m = t.get_next_move();
      printf("- Move: %d\n", m);
      }        
      t.retract_move();
      }
*/  
    

	PN<TicTacToe> p;
	p.init();
	TicTacToe g;
	g.clear();

	g.print_board();
	cout << endl;

	p.position(g);

	for (int i = 0 ; i < 1000; i++) {
		p.search_single_node();
		g.print_board();

		float best = 2000000000;
		for (vector<Node>::iterator i = p.tree.children.begin(); i != p.tree.children.end(); i++) {
	  
			float pn = i -> white_moves_to_win;
			float dpn = i -> black_moves_to_win;
	  
			if (g.wtm()) {
				if (dpn == 0) continue;
				if (pn/dpn < best) {
					best = pn/dpn;
				}
			} else {
				if (pn == 0) continue;
				if (dpn/pn < best) {
					best = dpn/pn;
				}
			}
	  					
		}		     
	
		cout << "Best: " << best << "tree.white_mvs"<<p.tree.white_moves_to_win <<" tree.blk "<<p.tree.black_moves_to_win<<endl;

		for (vector<Node>::iterator i = p.tree.children.begin(); i != p.tree.children.end(); i++) {
			cout << (g.print_move(i -> move_that_got_us_here)) << " pn " << (i -> white_moves_to_win) << " dpn " 
			     << (i -> black_moves_to_win) << endl;
	    
		}		     
	}

	g.print_board();
	for (vector<Node>::iterator i = p.tree.children.begin(); i != p.tree.children.end(); i++) {
		cout << (g.print_move(i -> move_that_got_us_here)) << " pn " << (i -> white_moves_to_win) << " dpn " 
		     << (i -> black_moves_to_win) << endl;
	    
	}		     

	p.wypisz();
}
