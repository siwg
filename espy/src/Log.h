#include<iostream>
#include<string>
#include<unistd.h>
using namespace std;

class Log {
public:
	static void error(string msg) {
		cout << "error " << msg << endl;
	}

	static void log(string msg) {
		cout << msg << endl;
	}

	static void debug(string msg) {
		cout << "  DEBUG " << getpid() << " " << msg << endl;
	}
};

