/*
 * Author : Rafał Hryciuk, Jacek Karaśkiewicz
 * 
 */

#include "Chess.h"

#define NULL_MOVE -1

Chess::move Chess::generateMoveNotation(Piece sourcePiece, Piece destinationPiece,
                                 int sourceColumn, int sourceRow, int destinationColumn,
                                 int destinationRow) const {
    Chess::move result  = sourcePiece * 1000000;
    result += destinationPiece * 10000;
    result += (sourceRow * 8 + sourceColumn) * 100;
    result += (destinationRow * 8 + destinationColumn);
    return result;
}

bool Chess::isPawn(Chess::Piece piece) const {
    return piece == WP || piece == BP;
}

bool Chess::isKnight(Piece piece) const {
    return piece == WK || piece == BK;
}

bool Chess::isBishop(Piece piece) const {
    return piece == WB || piece == BB;
}

bool Chess::isRook(Piece piece) const {
    return piece == WR || piece == BR;
}

bool Chess::isQueen(Piece piece) const {
    return piece == WQ || piece == BQ;
}

bool Chess::isKing(Piece piece) const {
    return piece == WK || piece == BK;
}

bool Chess::isWhite(Piece piece) const {
    return piece == WP || piece == WK || piece == WB || piece == WR || piece == WQ
        || piece == WK;
}

bool Chess::isBlack(Piece piece)  const {
    return piece == BP || piece == BK || piece == BB || piece == BR || piece == BQ
        || piece == BK;
}



std::vector<Chess::move> Chess::generatePossibleMoves(Piece piece, int sourceColumn, int sourceRow) const {
    if (isPawn(piece)) {
        return generatePossiblePawnMoves(piece, sourceColumn, sourceRow);
    } else if (isKnight(piece)) {
        return generatePossibleKnightMoves(piece, sourceColumn, sourceRow);
    } else if (isBishop(piece)) {
        return generatePossibleBishopMoves(piece, sourceColumn, sourceRow);
    } else if (isRook(piece)) {
        return generatePossibleRookMoves(piece, sourceColumn, sourceRow);
    } else if (isQueen(piece)) {
        return generatePossibleQueenMoves(piece, sourceColumn, sourceRow);
    } else if (isKing(piece)) {
        return generatePossibleKingMoves(piece, sourceColumn, sourceRow);
    } else {
        return std::vector<Chess::move>();;
    }
}


/**
 *
 * Assumtion to the following memthods : given sourceColumn and sourceRow arguments are correct.
 * sourcePiece, sourceRow [0..7]
 * Given piece is correct. It fulfills method contract.
 * 
 */
std::vector<Chess::move> Chess::generatePossiblePawnMoves(Piece piece, int sourceColumn, int sourceRow) const {
    std::vector<Chess::move> result;
    int newRow = sourceRow + piece == WP ? -1 : 1;
    Chess::move m = changePositionMove(sourceColumn, sourceRow, sourceColumn, newRow, piece);
    if (m != NULL_MOVE) {
        result.push_back(m);
    } 
    Chess::move m1 = attackMove(sourceColumn, sourceRow, sourceColumn + 1, newRow, piece);
    if (m1 != NULL_MOVE) {
        result.push_back(m1);
    }
    Chess::move m2 = attackMove(sourceColumn, sourceRow, sourceColumn - 1, newRow, piece);
    if (m2 != NULL_MOVE) {
        result.push_back(m2);
    }
    return result;
}

/**
 * Checks if the given destination position is a correct attack move.
 * Is a destination position occupied by an enemy and is possition in the board.
 */
Chess::move Chess::attackMove(int sourceColumn, int sourceRow, int destColumn, int destRow, Piece piece) const {
    if (isPositionCorrect(destColumn, destRow)) {
        Piece p = getEnemyOccupyingField(piece, destColumn, destRow);
        if (p != 0) {
            return generateMoveNotation(piece, p, sourceColumn, sourceRow, destColumn, destRow);
        }
    }
    return NULL_MOVE;
}


Chess::move Chess::changePositionMove(int sourceColumn, int sourceRow, int destColumn, int destRow, Piece piece) const {
    if (isPositionCorrect(destColumn, destRow) && isFieldEmpty(destColumn, destRow)) {
            return generateMoveNotation(piece, getEnemyOccupyingField(piece, destColumn, destRow),
                                sourceColumn,sourceRow, destColumn, destRow);
    }
    return NULL_MOVE;
}

std::vector<Chess::move> Chess::generatePossibleKnightMoves(Piece piece, int sourceColumn, int sourceRow) const {
    std::vector<Chess::move> result;
    const int size = 2;
    int c1[2] = {2, -2};
    int c2[2] = {1, -1};
    knightMovesInOneDimension(piece, sourceColumn, sourceRow, size, c1, c2, result);
    knightMovesInOneDimension(piece, sourceColumn, sourceRow, size, c2, c1, result);
    return result;
}

//rhr : stupid name for a method. Super knight moving in many dimensions :)
void Chess::knightMovesInOneDimension(Piece piece, int sourceColumn, int sourceRow,
    int size, int array1[], int array2[], std::vector<Chess::move> moves) const {

    for (int i = 0 ; i < size ; ++i) {
        for (int j = 0 ; j < size ; ++j) {
            move m1 = changePositionMove(sourceColumn, sourceRow, sourceColumn + array1[i], sourceRow + array2[j], piece);
            if (m1 != NULL_MOVE) {
                moves.push_back(m1);
            }
            move m2 = attackMove(sourceColumn, sourceRow, sourceColumn + array1[i], sourceRow + array2[j], piece);
            if (m2 != NULL_MOVE) {
                moves.push_back(m2);
            }

        }
    }

}

std::vector<Chess::move> Chess::generatePossibleBishopMoves(Piece piece, int sourceColumn, int sourceRow) const {
    std::vector<Chess::move> result;
    generateMovesInOneDirection(piece, sourceColumn, sourceRow, 1, 1, result, 7);
    generateMovesInOneDirection(piece, sourceColumn, sourceRow, 1, -1, result, 7);
    generateMovesInOneDirection(piece, sourceColumn, sourceRow, -1, 1, result, 7);
    generateMovesInOneDirection(piece, sourceColumn, sourceRow, -1, -1, result, 7);
    return result;
}


void Chess::generateMovesInOneDirection(Piece piece, int sourceColumn, int sourceRow,
    int horizonatlFactor, int verticalFactor, std::vector<Chess::move> moves, int maxChange) const {
    for (int i = 1 ; i <= maxChange ; ++i) {
        Chess::move m1 = attackMove(sourceColumn, sourceRow,
                                    sourceColumn + i * horizonatlFactor,
                                    sourceRow + i * verticalFactor, piece);
        if (m1 != NULL_MOVE) {
            moves.push_back(m1);
            return;
        }
        Chess::move m2 = changePositionMove(sourceColumn, sourceRow,
                                    sourceColumn + i * horizonatlFactor,
                                    sourceRow + i * verticalFactor, piece);
        if (m2 != NULL_MOVE) {
            moves.push_back(m2);
        } else {
            return;            
        }
    }
}

std::vector<Chess::move> Chess::generatePossibleRookMoves(Piece piece, int sourceColumn, int sourceRow) const {
    std::vector<Chess::move> result;
    generateMovesInOneDirection(piece, sourceColumn, sourceRow, 1, 0, result, 7);
    generateMovesInOneDirection(piece, sourceColumn, sourceRow, -1, 0, result, 7);
    generateMovesInOneDirection(piece, sourceColumn, sourceRow, 0, 1, result, 7);
    generateMovesInOneDirection(piece, sourceColumn, sourceRow, 0, -1, result, 7);
    return result;
}



std::vector<Chess::move> Chess::generatePossibleQueenMoves(Piece piece, int sourceColumn, int sourceRow) const {
    std::vector<Chess::move> result = generatePossibleBishopMoves(piece, sourceColumn, sourceRow);
    std::vector<Chess::move> result2 = generatePossibleRookMoves(piece, sourceColumn, sourceRow);
    result.insert(result.end(), result2.begin(), result2.end());
    return result;
}

std::vector<Chess::move> Chess::generatePossibleKingMoves(Piece piece, int sourceColumn, int sourceRow) const {
    std::vector<Chess::move> result;
    generateMovesInOneDirection(piece, sourceColumn, sourceRow, 1, 0, result, 1);
    generateMovesInOneDirection(piece, sourceColumn, sourceRow, -1, 0, result, 1);
    generateMovesInOneDirection(piece, sourceColumn, sourceRow, 0, 1, result, 1);
    generateMovesInOneDirection(piece, sourceColumn, sourceRow, 0, -1, result, 1);
    generateMovesInOneDirection(piece, sourceColumn, sourceRow, 1, 1, result, 1);
    generateMovesInOneDirection(piece, sourceColumn, sourceRow, 1, -1, result, 1);
    generateMovesInOneDirection(piece, sourceColumn, sourceRow, -1, 1, result, 1);
    generateMovesInOneDirection(piece, sourceColumn, sourceRow, -1, -1, result, 1);
    return result;
}

/**
 * Method checs if given coordinates are in the board fields, and if
 * the field is not occupied by another piece.
 */
bool Chess::isPositionCorrect(int column, int row) const {
    return column < board_size && column >= 0 && row < board_size && row >= 0;
}

bool Chess::isFieldEmpty(int column, int row) const {
    return state.board.fields[column][row] == NULL_PIECE;    
}

Chess::Piece Chess::getEnemyOccupyingField(Piece piece, int column, int row) const {
    Piece p = state.board.fields[column][row];
    if (isWhite(piece)) {
        return isBlack(p) ? p : NULL_PIECE;
    } else if (isBlack(piece)) {
        return isWhite(p) ? p : NULL_PIECE;
    }
    return NULL_PIECE;
}

void Chess::prepare_moves() {
    preparedMoves.clear();
    for (int i = 0 ; i < board_size ; ++i) {
        for (int j = 0 ; j < board_size ; ++j) {
            Piece p = state.board.fields[i][j];
            if (p != NULL_PIECE) {
                vector<move> moves = generatePossibleMoves(p, i, j);
                preparedMoves.insert(preparedMoves.end(), moves.begin(), moves.end());
            }
        }        
    }
    movesIndex = 0;
}


bool Chess::has_next_move() {
    return movesIndex < preparedMoves.size();    
}

Chess::move Chess::get_next_move() {
    return preparedMoves[movesIndex++];
}

/* TODO:
 * Jacek:
 * - obsĹ‚uga szacha i sytuacji wyjÄ…tkowych
 * - inicjalizacja struktury State
 * - make Chess::move, retract Chess::move
 *
 * RafaĹ‚:
 * - podstawowe ruchy figur zakladajac istnienie funkcji obslugujacych sytuacje specjalne
 * - nadac struktury czemus i to cos wrzucic gdzies */

