/**
 * \file
 *
 * Implementation of Hex game.
 *
 * This file is part of espy, the game solver.
 *
 * \author Jacek Tomaszewski, Przemek Bagard.
 *
 */

#ifndef __Hex__
#define __Hex__

#include "Game.h"
#include "Settings.h"
#include <vector>
#include <stack>
#include <set>
#include <list>
#include <algorithm>
#include "Transposition.h"

class Hex : public Game {
public:
	const static int sizew = 3;
	const static int sizeh = 3;
	const static int max_depth = Settings::maximum_game_length;

	int board[sizew][sizeh]; // piersza wspolrzedna okresla kolumne, druga wiersz
	Transposition<Hex>::Key randomKeyValue[sizew][sizeh][2];	// zakodowane informacje na temat pól (empty daje 0, dla graczy 1 i 2 są losowane)
	Transposition<Hex>::Key currentKey;
	
	const static int white = 0;
	const static int black = 1;
	const static int empty = 2;
	const static int field_states = empty;
	const static unsigned int killerMax = 5;
    
    typedef std::pair<int,int> move;
    
	int ply;			// depth
	std::list<move> mv[max_depth];
	move played[max_depth];
	std::list<move> killer[max_depth];

public:
	typedef std::pair<int,int> pos;
	
	Hex() {
		clear();
	};
	
	static string name() {
	  return "Hex";
	}
	
	void prepare_moves() {
		list<move> cur_killers = killer[ply];
		mv[ply].clear();
		
		//Remove illegal killer moves.
		list<move>::iterator it = cur_killers.begin();
		while (it != cur_killers.end()) {
		    move m = *it;
		    if (board[m.first][m.second] != empty)
		        it = cur_killers.erase(it);
		    else it++;
		}
		
		for (int i = 0; i < sizew; i++)
		for (int j = 0; j < sizeh; j++) {
		  move m = move(i, j);
		  if (board[i][j] == empty) {
		    if (std::find(cur_killers.begin(), cur_killers.end(), m) == cur_killers.end())
		      mv[ply].push_back(m);
		  }
		}
		while(!cur_killers.empty())
		{
		    mv[ply].push_front(cur_killers.back());
		    cur_killers.pop_back();
		}
	};

	bool has_next_move() {
		if (game_state() != in_progress)
			return false;
		return !mv[ply].empty();
	}
	move get_next_move() {
		move m = mv[ply].front();
		mv[ply].pop_front();
		return m;
	}

	void clear() {
		ply = 0;
		currentKey = 0;
		for (int i = 0; i < sizew; i++)
		for (int j = 0; j < sizeh; j++)
		{
			board[i][j] = empty;
			randomKeyValue[i][j][0] = Transposition<Hex>::generateKey();
			randomKeyValue[i][j][1] = Transposition<Hex>::generateKey();
		}
	}

	void make_move(const move m) {
		board[m.first][m.second] = ply & 1;
		played[ply] = m;
		currentKey ^= randomKeyValue[m.first][m.second][board[m.first][m.second]];

		/* set game state */

		game_state(in_progress);
		
		//if (ply == sizew*sizeh) game_state(draw); // optymalizacja - z dowodu na wykladzie (hex nie ma remisu)

		  std::stack<pos> st;
		  if ((ply & 1) == white) {
            for(int l1 = 0; l1 < sizeh; l1++)
            {
            if(board[0][l1] == white)
              st.push(pos(0, l1));
            }
            if (dfs(st, white))
            {
              game_state(white_wins);
              newKiller();
            }
		  }
		  else {
            for(int l1 = 0; l1 < sizew; l1++)
            {
            if(board[l1][0] == black)
              st.push(pos(l1, 0));
            }
            if (dfs(st, black))
            {
              game_state(black_wins);
              newKiller();
            }
		  }
		  ply++;
	}
	
	bool dfs(std::stack<pos>& st, int player)
	{
	  std::set<pos> visited;
	  while(!st.empty())
	  {
	    pos p = st.top();
	    st.pop();
	    if(visited.find(p) != visited.end()) continue;
	    visited.insert(p);
	    
	    if (player == white && p.first == sizew-1) return true;
	    if (player == black && p.second == sizeh-1) return true;
	    
	    std::vector<pos> n = neighbours(p.first, p.second);
	    for(unsigned int i=0; i< n.size(); i++) {
	      pos r = n[i];
	      if(board[r.first][r.second] == player && visited.find(r) == visited.end()) st.push(r);
	    }
	  }
	  return false;
	}
	
	
	std::vector<pos> neighbours(int x, int y) {
	  std::vector<pos> result;
	  if (x > 0) result.push_back(pos(x-1, y));
	  if (x < sizew-1) result.push_back(pos(x+1, y));
	  if (y > 0) {
	    result.push_back(pos(x, y-1));
	    if (x < sizew-1) result.push_back(pos(x+1, y-1));
	  }
	  if (y < sizeh-1) {
	    result.push_back(pos(x, y+1));
	    if (x > 0) result.push_back(pos(x-1, y+1));
	  }
	  return result;
	}
	
	void retract_move() {
		game_state(in_progress);
		--ply;
		pos p = played[ply];
		currentKey ^= randomKeyValue[p.first][p.second][board[p.first][p.second]];

		board[p.first][p.second] = empty;
	}

	inline int wtm() const {
		return (ply & 1) == 0;
	}
	
	void newKiller()
	{
	    int i = ply;
	    while(i >= 0)
	    {
		move m = played[i];
		std::list<move>::iterator it = std::find(killer[ply].begin(), killer[ply].end(), m);
		if(it == killer[ply].end()) // nie ma jeszcze
		{
		    if(killer[ply].size() >= killerMax) killer[ply].pop_back();
		}
		else killer[ply].erase(it);
		killer[ply].push_front(m);
		i -= 2;
	    }
	}
	
	Transposition<Hex>::Key get_board_hash_key()
	{
	  return currentKey;
	}
};

#endif
