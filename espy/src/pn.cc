/**
 * \file 
 *
 * main().
 *
 */

/**
 *
 * \mainpage \c Espy Documentation
 *
 * \section Introduction
 *
 * 
 */

#include <iostream>
using namespace std;

#include "Settings.h"
#include "PNEngine.h"
#include "ConnectSix.h"
#include "ConnectFour.h"
#include "TicTacToe.h"

PNEngine<ConnectSix> engine;

int main(int argc, char **argv)
{
	engine.main();

	cout << "Bye!\n";

	return 0;
}
