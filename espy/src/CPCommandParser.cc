/*
 * CPCommandParser.cc
 *
 *  Created on: 2012-03-19
 *      Author: elshize
 */

#include "CPCommandParser.h"
#include "CPEventListener.h"
#include "CPTypes.h"
#include <istream>
#include <sstream>
#include <iostream>
#include <string>
#include <map>
#include <cstdlib>
#include <utility>

using namespace std;

const string CPCommandParser::CN_CONNECT = "connect";
const string CPCommandParser::CN_BESTMOVE = "bestmove";
const string CPCommandParser::CN_ERROR = "error";
const string CPCommandParser::CN_CONCEDE = "concede";
const string CPCommandParser::CN_INFO = "info";
const string CPCommandParser::CN_INIT = "init";
const string CPCommandParser::CN_STARTGAME = "startgame";
const string CPCommandParser::CN_MOVE = "move";
const string CPCommandParser::CN_FEEDBACK = "feedback";
const string CPCommandParser::CN_SETOPTION = "setoption";
const string CPCommandParser::CN_GETMOVE = "getmove";
const string CPCommandParser::CN_FORCEMOVE = "forcemove";
const string CPCommandParser::CN_STOP = "stop";
const string CPCommandParser::CN_ENDGAME = "endgame";
const string CPCommandParser::CN_QUIT = "quit";

const string CPCommandParser::UNKNOWN_LABEL = "unknown";
const string CPCommandParser::WHITE_LABEL = "white";
const string CPCommandParser::BLACK_LABEL = "black";
const string CPCommandParser::ANALYST_LABEL = "analyst";
const string CPCommandParser::DRAW_LABEL = "draw";
const string CPCommandParser::INFINITY_LABEL = "inf";

CPCommandParser::CPCommandParser(istream & is, CPEventListener & l):inputStream(is),
          commandStream(stringstream::in | stringstream::out), listener(l) {
    
}

string CPCommandParser::nextCommand() {
    string commandLine;
    getline(inputStream, commandLine);
//     cout << "nextCommand: " << commandLine << endl;
    return commandLine;
}

void CPCommandParser::parse(string commandLine) {
//     cout << "parse: " << commandLine << endl;
    string commandType;
    commandStream.clear();
    commandStream.str(commandLine);
//     cout << "parse: " << commandStream.str() << endl;
    commandStream >> commandType;
//     cout << "parse: " << commandType << endl;
    getCommand(commandType);
}

string CPCommandParser::pullNextArg() {
    string res;
    if (!commandStream.eof())
        commandStream >> res;
    else
        throw 1;
    return res;
}

string CPCommandParser::pullPosition() {
	// TODO:
    return pullNextArg();
}

string CPCommandParser::pullMove() {
	// TODO:
	return pullNextArg();
}

CPColor CPCommandParser::colorFromString(string colorString) {
    if (colorString == WHITE_LABEL) return CP_COLOR_WHITE;
    else if (colorString == BLACK_LABEL) return CP_COLOR_BLACK;
    else throw 1;
}

CPRole CPCommandParser::roleFromString(string roleString) {
    if (roleString == WHITE_LABEL) return CP_ROLE_WHITE;
    else if (roleString == BLACK_LABEL) return CP_ROLE_BLACK;
	else if (roleString == ANALYST_LABEL) return CP_ROLE_ANALYST;
    else throw 1;
}

CPResult CPCommandParser::resultFromString(string resultString) {
    if (resultString == WHITE_LABEL) return CP_RESULT_WHITE;
    else if (resultString == BLACK_LABEL) return CP_RESULT_BLACK;
	else if (resultString == DRAW_LABEL) return CP_RESULT_DRAW;
    else throw 1;
}

pair<int, int> CPCommandParser::timesFromString(string times) {
    size_t found = times.find(":");
    if (found == string::npos)
        throw 1;
    int firstTime = timeFromString(times.substr(0, found));
    int secondTime = timeFromString(times.substr(found + 1));
    pair<int, int> result = make_pair(firstTime, secondTime);
    return result;
}

int CPCommandParser::timeFromString(string timeString) {
    if (timeString == INFINITY_LABEL) return -1;
    else {
        int result = atoi(timeString.c_str());
        if (result == 0 && timeString != "0") throw 1;
        return result;
    }
}

void CPCommandParser::getCommand(string & commandType) {
    if (commandType == CN_CONNECT) commandConnect();
    else if (commandType == CN_BESTMOVE) commandBestMove();
    else if (commandType == CN_ERROR) commandError();
    else if (commandType == CN_CONCEDE) commandConcede();
    else if (commandType == CN_INFO) commandInfo();
    else if (commandType == CN_INIT) commandInit();
    else if (commandType == CN_STARTGAME) commandStartGame();
    else if (commandType == CN_MOVE) commandMove();
    else if (commandType == CN_FEEDBACK) commandFeedback();
    else if (commandType == CN_SETOPTION) commandSetOption();
    else if (commandType == CN_GETMOVE) commandGetMove();
    else if (commandType == CN_FORCEMOVE) commandForceMove();
    else if (commandType == CN_STOP) commandStop();
    else if (commandType == CN_ENDGAME) commandEndGame();
    else if (commandType == CN_QUIT) commandQuit();
    else commandUnknown();
}

void CPCommandParser::commandUnknown() {
    throw 2;
}

void CPCommandParser::commandConnect() {
//     cout << "connect" << endl;
    string name;
    name = pullNextArg();
    listener.connect(name);
//     cout << "connect" << endl;
}

void CPCommandParser::commandBestMove() {
    string move;
    move = pullNextArg();
    listener.bestMove(move);
}

void CPCommandParser::commandError() {
    string errorMsg = pullNextArg();
    listener.error(errorMsg);
}

void CPCommandParser::commandConcede() {
	listener.concede();
}

void CPCommandParser::commandInfo() {
	map<string, string> options;
	while (true) {
		try {
			string name = pullNextArg();
			string value = pullNextArg();
			options.insert(make_pair<string, string>(name, value));
		} catch (int code) {
			break;
		}
	}
	listener.info(options);
}

void CPCommandParser::commandInit() {
	listener.init();
}

void CPCommandParser::commandStartGame() {
    string position = pullPosition();
    string role = pullNextArg();
    string time = pullNextArg();
    
    pair<int, int> times = timesFromString(time);
    int gameTime = times.first;
    int bonusTime = times.second;
    
    listener.startGame(position, roleFromString(role), gameTime, bonusTime);
}

void CPCommandParser::commandMove() {
    string color = pullNextArg();
    string move = pullMove();
    listener.move(colorFromString(color), move);
}

void CPCommandParser::commandFeedback() {
    string turn = pullNextArg();
    bool on;
    if (turn == "on") on = true;
    else if (turn == "off") on = false;
    else throw 1;
    listener.feedback(on);
}

void CPCommandParser::commandSetOption() {
	map<string, string> options;
	while (true) {
		try {
			string name = pullNextArg();
			string value = pullNextArg();
			options.insert(make_pair<string, string>(name, value));
		} catch (int code) {
			break;
		}
	}
	listener.setOption(options);
}

void CPCommandParser::commandGetMove() {
    string color = pullNextArg();
    string moveTime = pullNextArg();
    string gameTime = pullNextArg();
    
    int mTime = timeFromString(moveTime);
    int gTime = timeFromString(gameTime);
    
    listener.getMove(colorFromString(color), mTime, gTime);
}

void CPCommandParser::commandForceMove() {
	listener.forceMove();
}

void CPCommandParser::commandStop() {
	listener.stop();
}

void CPCommandParser::commandEndGame() {
    string result = pullNextArg();
    listener.endGame(resultFromString(result));
}

void CPCommandParser::commandQuit() {
    listener.quit();
}
