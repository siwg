#include<pthread.h>
#include<iostream>
#include<string>
#include<stdlib.h>

#include<errno.h>

#include "CPEventListener.h"
#include "CPCommandParser.h"
#include "ConnectSix.h"

int ec_prep = 0;

/*****
 * Najprostsza metoda implementacji własnego silnika to modyfikacja metod 
 *
 *  - start_thinking() (wybór ruchu)
 * i
 *  - start_pondering() (myślenie w trakcie ruchu przeciwnika)
 *
 * Kontrola czasu zaimplementowana jest niżej.
 *
 * Metoda start_thinking liczy tak długo, aż zmienna pause_search przyjmie
 *   wartość true.
 * 
 * Aktualny wynik szukania zapisany jest w zmiennej 
 *   search_result
 *
 * Wybrany ruch wysyłany jest automatycznie. Ustawienie pause_search
 *   na true zazwyczaj oznacza, że ruch właśnie został wysłany.
 *
 * Początkowa pozycja do przeszukiwania jest w zmiennej official_board
 * Należy ją na początku sklonować.
 * 
 *****/

#include<vector>
using namespace std;

#include "Log.h"
#include "sys/time.h"

template <class G>
class Engine : public CPEventListener {

	/*
	 * Dostęp do danych i synchronizacja między wątkami.
	 */
	class Mutex {
		pthread_mutex_t	mutex;
		pthread_cond_t cond;	
	public:
		Mutex() {
			if (pthread_mutex_init(&mutex,NULL) != 0) 
				Log::error("pthread_mutex_init failed in Mutex class constructor");		
			if (pthread_cond_init(&cond,NULL) != 0) 
				Log::error("pthread_cond_init failed in Mutex class constructor");
		}

		~Mutex() {
			if (pthread_mutex_destroy(&mutex) != 0) 
				Log::error("pthread_mutex_destroy failed in Mutex class destructor");		
			if (pthread_cond_destroy(&cond) != 0) 
				Log::error("pthread_cond_destroy failed in Mutex class destructor");
		}

		void grab() {
			if (pthread_mutex_lock(&mutex) != 0) 
				Log::error("pthread_mutex_lock failed in Mutex.grab()");
		}

		void release() {
			if (pthread_mutex_unlock(&mutex) != 0) 
				Log::error("pthread_mutex_unlock failed in Mutex.release()");		       
		}

		void sleep() {
			if (pthread_cond_wait(&cond,&mutex) != 0) 
				Log::error("pthread_cond_wait failed in Mutex.sleep()");
		}

		void sleep(timespec &ts) {
			int x;
			if ((x = pthread_cond_timedwait(&cond,&mutex, &ts)) != 0) {
				if (x != ETIMEDOUT) {
					Log::error("pthread_cond_wait failed in method Mutex.sleep(timespec)");			
				}
			}
		}

		void wake() {
			if (pthread_cond_signal(&cond) != 0) 
				Log::error("pthread_cond_signal failed in Mutex.wake()");
		}

	};

public:
	volatile bool pause_search;
	
protected:

	class SearchResult {
	public:
		Mutex lock;

		bool in_progress;
		int nominal_depth;
		int score_lower_bound;
		int score_upper_bound;
	      
		bool resign;
		vector<typename G::move> pv;
	       
		int elapsed_time;
		int nodes_used;
		
		void init() {
			in_progress = true;
			resign = false;
			nominal_depth = -1;			
			pv.clear();
		}

		void update(SearchResult &sr) {
			lock.grab();
			in_progress = sr.in_progress;
			nominal_depth = sr.nominal_depth;
			pv = sr.pv;
			// .....
			lock.release();
		}
	};

	SearchResult search_result;


	SearchResult current_search;

private:
	G official_board;
	Mutex official_board_mutex;

	virtual void think(G, SearchResult&) = 0;

	Mutex engine_mutex;

	typedef enum { e_idle, e_white, e_black, e_analyze } mode;

	typedef enum { c_none, c_update, c_reset, c_quit, c_move } command;

	volatile command	command_to_engine;
	mode engine_mode;	
	
	void engine_sleep() {
		engine_mutex.wake();

		if (command_to_engine == c_none) {
			engine_mutex.sleep();
		}
	}

	void *engine_thread() {
		Log::debug("Engine thread started.");

 		engine_mutex.grab();
		
		while (1) {
			pause_search = false; 
			command tmp = command_to_engine; 
			command_to_engine = c_none;

			cout << "Engine loop " << tmp << endl;

			switch (tmp) {
			case c_none:
				switch(engine_mode) {
				case e_idle: engine_sleep(); break;
				case e_white:
				case e_black:
				case e_analyze:
					current_search.lock.grab();
					current_search.init();
					current_search.lock.release();

					official_board_mutex.grab();
					G board = official_board;
					official_board_mutex.release();
					
					if ((engine_mode == e_white && !board.wtm()) ||
					    (engine_mode == e_black && board.wtm()) ||
					    (board.game_state() != G::in_progress)) {

						cout << "Engine sleep" << endl;

						    engine_sleep(); break;
					}

					reset_target_time();

					engine_mutex.release();

					think(board, current_search);

				    	engine_mutex.grab();

					current_search.lock.grab();
					if (!current_search.in_progress) {
						search_result.update(current_search);						
					}
					current_search.lock.release();

					if (!pause_search  && command_to_engine == c_none) {
						command_to_engine = c_move;
					}					
				}
				break;
				
			case c_quit: 
				engine_mutex.release(); 
				
				Log::debug("Engine thread finished.");
				return NULL;
			
			case c_reset:
				// czyszczenie tablicy transpozycji itp.

			case c_update:
				pause_search = false; 
				search_result.lock.grab();
				search_result.init();
				search_result.lock.release();

				//				Log::debug("reset/pause");
				reset_target_time();
				clock_mutex.wake();

				break;

			case c_move:
			 				Log::debug("c_move entered");

				official_board_mutex.grab();
				if ((official_board.wtm() && engine_mode != e_white) ||
				    ((!official_board.wtm()) && engine_mode != e_black)) {
					official_board_mutex.release();
										Log::debug("c_move break - wrong side to move!!!");
					break;
				}

				official_board_mutex.release();

				search_result.lock.grab();
				typename G::move bestmove;

				if (search_result.pv.size() == 0 && !search_result.resign) {
				  	Log::debug("PANIC! No vaild search result found. Making random move!");
									
					bestmove = -1;
				} else {
					bestmove = search_result.pv[0];
				}
				search_result.init();
				search_result.lock.release();

				official_board_mutex.grab();
				official_board.prepare_moves();
				if (bestmove == -1 && !search_result.resign) {					
					vector<typename G::move> moves;

					while(official_board.has_next_move()) {
						moves.push_back(official_board.get_next_move());
					}
					
					if (moves.size() == 0) {
						Log::error("Move requested in a final position (with no moves available).");
					} else {
						bestmove = moves[random() % moves.size()];
					}
				}

				if (bestmove != -1) {
					official_board.make_move(bestmove);
					cout << "bestmove " << official_board.print_move(bestmove) << endl;
				} else if (search_result.resign) {
					cout << "concede" << endl;
				}

				switch (official_board.game_state()) {
				case G::white_wins:
					cout << "endgame white"  << endl;
					break;
				case G::black_wins:
					cout << "endgame black"  << endl;
					break;
				case G::draw:
					cout << "endgame draw" << endl;
					break;
				}

				official_board_mutex.release();

				break;
			default:
				Log::error("Internal error: bad command sent to the engine thread.");
			}
		}
		
		Log::debug("Engine thread finished.");
		return NULL;
	}

	static void* engine_thread_helper(void *context) {
		return ((Engine *) context) -> engine_thread();
	}

	void display_feedback() {
		cout << "info feedback XXX" << endl;
	}


	/*
	 * Wątek clock obsługuje dwa zadania:
	 *
	 *   w przypadku, gdy toczy się gra i silnik ma swój ruch decyduje, w którym momencie zakończyć myślenie i wysłać ruch
	 *   w przypadku, gdy feedback jest włączony, co sekundę wysyła informacje o wynikach szukania do interfejsu
	 *     (wtedy, gdy silnik pracuje)
	 */

	Mutex clock_mutex;

	int time;

	void set_time_for_move(int _time)
	{
		time = _time;

		clock_mutex.wake();

		cout << "Time for move: " << time << endl;
	}

	timeval target;

	bool reached_target_time()
	{
		timeval t;
		gettimeofday(&t, NULL);

		return target.tv_sec < t.tv_sec || (target.tv_sec == t.tv_sec && target.tv_usec < t.tv_usec);
	}

	void reset_target_time()
	{
		  timeval t;		  
		  gettimeofday(&t, NULL);

		  t.tv_usec += time;
		  t.tv_sec += t.tv_usec / 1000000;
		  t.tv_usec = t.tv_usec % 1000000;

		  target = t;

		  clock_mutex.wake();
	}

	timespec get_sleep_time()
	{
		  timespec ts;
		  ts.tv_sec = target.tv_sec;
		  ts.tv_nsec = target.tv_usec * 1000;

		  return ts;
	}

	void *clock_thread() {
	  Log::debug("Clock thread started.");

	  clock_mutex.grab();

	  while (1) {
		  /*
		   * Działamy
		   */

		  switch (engine_mode) {
		  case e_idle:
			  break;
		  case e_white:
		  case e_black:
			  cout << "Tik-tak " << time << endl;

			  if (!reached_target_time()) { break; }

			  cout << "Reached target time" << endl;

			  official_board_mutex.grab();

			  if ((engine_mode == e_white && official_board.wtm()) ||
			      (engine_mode == e_black && !official_board.wtm())) {

				  official_board_mutex.release();
				  
				  ec_prepare("c_move");
				  command_to_engine = c_move; pause_search = true;
				  ec_send();
			  } else {
				  official_board_mutex.release();
			  }

   
			  cout << "clock is going to sleep (pause " << pause_search << ")" << endl;
			  clock_mutex.sleep();

			  break;
		  case e_analyze:
			  display_feedback();
			  break;
		  }

		  /*
		   * Usypiamy
		   */

		  timespec ts = get_sleep_time();

		  clock_mutex.sleep(ts);

		  if (!interface_loop) {
			  break;
		  }
	  }

	  clock_mutex.release();

	  Log::debug("Clock thread finished.");
	  return NULL;
	}

	static void* clock_thread_helper(void *context) {
		return ((Engine *) context) -> clock_thread();
	}

	bool interface_loop;
	void *interface_thread() {
	  Log::debug("Interface thread started.");

	  CPCommandParser parser(cin, *this);
	  
	  while (interface_loop) {
		  string line = parser.nextCommand();
		  try {
			  parser.parse(line);
		  } catch (int error) {
			  if (error == 2)
				  cout << "Unknown command." << endl;
			  else
				  cout << "Command corrupted." << endl;
		  }
	  }
	  
	  Log::debug("Interface thread finished.");
	  return NULL;
	}

	static void* interface_thread_helper(void *context) {
		return ((Engine *) context) -> interface_thread();
	}

	void ec_prepare(string msg) {
		ec_prep++;
		cout << "ec_prepare " << msg << " (" << ec_prep << "); command_to_engine " << command_to_engine << " pause " << pause_search << endl;

		engine_mutex.grab();
		while (command_to_engine != c_none) {
			cout << "   WHI ec_prepare " << msg << " (" << ec_prep << "); command_to_engine " << command_to_engine << " pause " << pause_search << endl;
			engine_mutex.wake();
			engine_mutex.sleep();
		}
	}

	void ec_send() {
		cout << "ec_send (" << ec_prep << ")";

		ec_prep--;

		engine_mutex.release();
		engine_mutex.wake();
	}

public:

	void main() {
		interface_loop = true;
		
		pthread_attr_t attr[2];
		pthread_t thr[3];


		if (pthread_attr_init(&attr[0]) != 0 ||
		    pthread_attr_init(&attr[1]) != 0) {
			Log::error("pthread_attr_init");
		};

		if (pthread_attr_setdetachstate(&attr[0],PTHREAD_CREATE_JOINABLE) != 0 ||
		    pthread_attr_setdetachstate(&attr[1],PTHREAD_CREATE_DETACHED) != 0) {
			Log::error("pthread_attr_setdetachstate");
		};

		if (pthread_create(&thr[0],&attr[0],engine_thread_helper,this) != 0 ||
		    pthread_create(&thr[1], &attr[0], clock_thread_helper, this) != 0 ||
		    pthread_create(&thr[2], &attr[0], interface_thread_helper, this) != 0) {
			Log::error("pthread_create");
		};


		if (pthread_join(thr[0],NULL) != 0 ||
		    pthread_join(thr[1],NULL) != 0 ||
		    pthread_join(thr[2],NULL) != 0) {
			error("pthread_join");
		};

		Log::debug("Engine::main() exiting.");
	}

	/*****
	 * Implementacja metod CPEventListenera
	 * (Komunikacja GUI -> silnik.)
	 */


	/*****
	 * W metodzie init tworzymy dwa własne wątki:
	 *  engine_thread - szuka ruchu
	 *  clock_thread - pilnuje zegara i decyduje o wysłaniu ruchu do GUI
	 *
	 *
	 *****/

	void init() {
		engine_mode = e_idle;
		time = 1000000;
		official_board.clear();				
	}

	/*
	 * Wszystkie poniższe metody wywoływane są przez wątek nasłuchujący na
	 *   wejściu i nie wykonują bezpośrednio poleceń, a kolejkują je
	 *   w kolejce komend zegara lub silnika i wysyłają sygnał, że jest następne
	 *   polecenie.
	 */

	virtual void startGame(std::string position, CPRole role,
			       int gameTime, int bonusTime) {

		switch (role) {
		case CP_ROLE_WHITE: // Log::debug("startGame as white"); 
			break;
		case CP_ROLE_BLACK: //Log::debug("startGame as black"); 
			break;
		default: break;
		}

		ec_prepare("startGame");

		set_time_for_move(15000000);

		command_to_engine = c_update; pause_search = true;

		switch (role) {
		case CP_ROLE_WHITE: engine_mode = e_white; break;
		case CP_ROLE_BLACK: engine_mode = e_black; break;
		case CP_ROLE_ANALYST: engine_mode = e_analyze; break;
		default: break;
		}

		search_result.lock.grab();
		search_result.init();
		search_result.lock.release();

		official_board_mutex.grab();
		official_board.clear();
		official_board_mutex.release();

		ec_send();
//		Log::debug("startGame send");

	}

	virtual void move(CPColor color, std::string move) {
//		Log::debug("got opp move: " + move);
		
		switch (color) {
		case CP_COLOR_WHITE: 
			cout << "white moves " << move << " Im playing"; 

			if (!official_board.wtm()) cout << "!!!!!!!!!!!!! ";
			break;
		case CP_COLOR_BLACK: 
			cout << "black moves Im playing"; 
			if (official_board.wtm()) cout << "!!!!!!!!!!!!! ";
			break;
		default: break;
		}
		
		switch (engine_mode) {
		case e_white: cout << " white ";break;
		case e_black: cout << " black "; break;
		default: break;
		}
		
		if (official_board.wtm()) {
			cout << " on board white" << endl;
		} else {
			cout << " on board black" << endl;
		}

		ec_prepare("move");
		command_to_engine = c_update; pause_search = true;

		official_board_mutex.grab();
		typename G::move mv = official_board.parse_move(move);

		cout << " parsed as " << official_board.print_move(mv) << endl;
		
		official_board.prepare_moves();
		bool valid = false;
		while (official_board.has_next_move() && !valid) {
			if (official_board.get_next_move() == mv) {
				valid = true;
			}
		}
		if (valid) {
			official_board.make_move(mv);
		} else {
			Log::error("Invalid move.");
		}

		switch (official_board.game_state()) {
		case G::white_wins:
			cout << "endgame White wins"  << endl;
			break;
		case G::black_wins:
			cout << "endgame Black wins"  << endl;
			break;
		case G::draw:
			cout << "endgame Draw" << endl;
			break;
		}
		
		official_board_mutex.release();

		ec_send();
	}

	virtual void feedback(bool on) {
		Log::log("feedback not implemented.");
	}

	virtual void setOption(std::map<std::string, std::string> & options) {
		Log::log("setOption not implemented.");
	}

	virtual void getMove(CPColor color, int moveTime,
			     int gameTime) {

//		Log::debug("getMove");

		set_time_for_move(gameTime * 1000000 / 30);
		reset_target_time();

		if ((color == CP_COLOR_WHITE && engine_mode == e_white) ||
		    (color == CP_COLOR_BLACK && engine_mode == e_black)) {

			// do uzupełnienia 
			return;
		}

		ec_prepare("getMove");
		if (color == CP_COLOR_WHITE) {
			engine_mode = e_white;
		} else {
			engine_mode = e_black;
		}
		command_to_engine = c_update; pause_search = true;
		ec_send();
	}

	virtual void forceMove() {
		ec_prepare("forceMove");
		command_to_engine = c_move; pause_search = true;
		ec_send();
	}

	virtual void stop() {
		ec_prepare("stop");
		engine_mode = e_idle; pause_search = true;
		command_to_engine = c_update;
		ec_send();
	}

	virtual void endGame(CPResult result) {
		ec_prepare("endGame");
		engine_mode = e_idle; pause_search = true;
		command_to_engine = c_update;
		ec_send();
	}

	virtual void quit() {
		interface_loop = false;

		ec_prepare("quit");
		command_to_engine = c_quit; pause_search = true;
		ec_send();

		clock_mutex.wake();
	}


	virtual void connect(std::string name) {};
	virtual void bestMove(std::string move) {};
	virtual void error(std::string message) {};
	virtual void concede() {};
	virtual void info(std::map<std::string, std::string> & options) {};

};
