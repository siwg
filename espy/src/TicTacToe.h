/**
 * \file
 *
 * Implementation of Tic Tac Toe game.
 *
 * This file is part of espy, the game solver.
 *
 * \author Andrzej Nag�rko.
 *
 */

#ifndef __TicTacToe__
#define __TicTacToe__

#include "Game.h"
#include "Settings.h"

class TicTacToe : public Game {
public:
	const static int max_depth = Settings::maximum_game_length;

	const static int cross = 0;
	const static int circle = 1;
	const static int empty = 2;

	struct board {
		int sq[10];
	} bd;

	int ply;
	int mv[max_depth];
	int played[max_depth];

public:
	typedef int move;
	
	TicTacToe() {
		clear();
	};

	void prepare_moves() {
		mv[ply] = 0;
	};

	bool has_next_move() {
		while (bd.sq[mv[ply]] != empty) mv[ply]++;
		return mv[ply] < 9;
	}
	move get_next_move() {
		while (bd.sq[mv[ply]] != empty) mv[ply]++;
		return mv[ply]++;
	}

	void clear() {
		ply = 0;
		for (int i = 0; i < 10; i++)
			bd.sq[i] = empty;
	}

	void make_move(const move m) {
		bd.sq[m] = ply & 1;
		played[ply] = m;
		++ply;

		/* set game state */

		game_state(in_progress);
		
		if (ply == 9) game_state(draw);

		const int diag[8][3] = {
		    {0,1,2}, {3,4,5}, {6,7,8},{0,3,6},{1,4,7},
		    {2,5,8},{0,4,8}, {2,4,6} };

                for (int t = 0; t < 2; t++) {
                    for (int d = 0; d < 8; d++) {
                        bool w = true;
                        for (int n = 0; n < 3; n++) {
                            if (bd.sq[diag[d][n]] != t) {
                                w = false;
                                break;
                            }
                        }
                        if (w) {
                            if (t == 0) { game_state(white_wins); }
                            if (t == 1) { game_state(black_wins); }
                            break;
                        }
                    }
                }
	}

	void retract_move() {
		--ply;
		bd.sq[played[ply]] = empty;
	}

	inline int wtm() const {
		return (ply & 1) == 0;
	}

	static string name() {
		return "TicTacToe";
	}

	void print_board() {
		for (int i = 0; i < 3; i++) {
		        cout << "|";
		   	for (int j = 0; j < 3; j++) {
				switch (bd.sq[i*3+j]) {
				case cross: cout << "x"; break;
				case circle: cout << "o"; break;
				case empty: cout << "."; break;
				default: cout << "?";
				}
			}
			cout << "|";
                        if (i == 2) {
                                cout << (wtm() ? " white" : " black");
                        }
                        cout << endl;
		}
	}
};

#endif
