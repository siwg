
#ifndef __Transposition__
#define __Transposition__

//#include <unordered_map>
//#include <map>

//#include <hash_map>


template<typename G>
class Transposition {
public:
  typedef unsigned long long Key;
  typedef typename G::move move;
  
  static const int hash_power_size = 20;
  static const int hash_size = 1 << (hash_power_size - 1);
  
  static const int empty = 0;
  static const int lower_bound = 1;
  static const int exact_bound = 2;
  static const int upper_bound = 3;

  static const int rand_max = 32767;
  
  struct hash_entry {
  Key hash_key;
  move best_move;
  short int bound;
  unsigned char depth;
  unsigned char type;
  };
  
  hash_entry hash_table[hash_size];
  
  // sprawdza, czy w tablicy, element odpowiada temu kluczowi (w szczególności, jeśli hash_entry nie był nigdy nadpisywany, to zwróci false) 
  bool exact(Key k)
  {
    return (*this)[k].hash_key == k && (*this)[k].type != empty;
  }
  
  // założenie RAND_MAX jest taki jak maksymalna wartość long int
  static Key generateKey()
  {
    unsigned long long int val = 0;
    for(int l1 = 0; l1 < 4; l1++)
      //      val = val*rand_max + (rand() % rand_max);

    return val;
  }
  
  
  hash_entry& operator[](Key idx) {
    return hash_table[idx % hash_size];
    
  }
  
  const hash_entry& operator[](Key idx) const {
    return hash_table[idx % hash_size];
  }
};

#endif
