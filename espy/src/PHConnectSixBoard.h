/* Connect six board model with state value calculation.
 *
 * Author: Przemyslaw Horban (ph262940@students.mimuw.edu.pl)
 */

#ifndef PH_CONNECT_SIX_BOARD_H
#define PH_CONNECT_SIX_BOARD_H

#include <cstdlib>
#include <cstdio>
#include <vector>
#include <iostream>

#define deb(x) { cerr << #x << " = " << x << endl; }

using namespace std;

typedef short pos;
typedef char color;

static const pos no_pos = -10000;
enum {
  no_color = 0,
  white = 1,
  black = -1,
};


void fail(const char *message) {
  fprintf(stderr, message);
  fprintf(stdout, message);
  fflush(stderr);
  fflush(stdout);
  exit(1);
}

enum PlayerToMove {
  WHITE_MOVE = 0,
  BLACK_MOVE = 1,
};

enum WeigthedColor {
  WHITE_WEIGTH = 0,
  BLACK_WEIGTH = 1,
};

struct StripWeigths {
  public:
    vector<int> weigths[2][2];

    // Use enums above for arguments
    int get(PlayerToMove whosMove, int pieceCnt, WeigthedColor pieceCol) {
      return weigths[whosMove][pieceCol][pieceCnt];
    }
};

class PHConnectSixBoard {
  public:
    PHConnectSixBoard(size_t size, StripWeigths weigths)
    : n(size), boardMarks(size * size), sixStrips(size), weigths(weigths) {
      pieces.clear();
      pieces.resize(n * n, no_color);
      piecesPlaced = 0;
      neighbours.clear();
      neighbours.resize(n * n, 0);
      reevaluate();
    }

    pos xyToPos(size_t x, size_t y) {
      if(x >= n || y >= n) {
        fail("Invalid x, y position");
      }
      return y * n + x;
    }

    void posToXY(size_t *x, size_t *y, pos p) {
      *y = p / n;
      *x = p % n;
    }

    const vector<pos> &neghbourEmptyCells() {
      return boardMarks.allMarked();
    }

    void placePiece(pos p, color c) {
      if(pieces[p] != no_color)
        fail("Can't place piece on occupied position");

      pieces[p] = c;
      piecesPlaced++;

      size_t x, y; posToXY(&x, &y, p);
      sixStrips.insertPiece(x, y, c);

      reevaluate();

      updateNeighbourhood(p, +1);
    }

    void removePiece(pos p) {
      if(pieces[p] == no_color)
        fail("Can't remove piece from an empty position");

      size_t x, y; posToXY(&x, &y, p);
      sixStrips.removePiece(x, y, pieces[p]);

      pieces[p] = no_color;
      piecesPlaced--;

      reevaluate();

      updateNeighbourhood(p, -1);
    }

    int evaluation(color nextToMove) {
      if(nextToMove == white)
        return brdValueWhiteMove;
      else if(nextToMove == black)
        return brdValueBlackMove;
      fail("Invalid argument to boardValue");
      return -1;
    }

    bool whiteWins() {
      return sixStrips.howManyStripsWith(6, white) > 0;
    }

    bool blackWins() {
      return sixStrips.howManyStripsWith(6, black) > 0;
    }

    bool draw() {
      return piecesPlaced == n * n;
    }

    void printBoard(vector<pair<pos, char> > labels =
                    vector<pair<pos, char> >()) {
      for(int y = 0; y < (int)n; y++) {
        for(int x = 0; x < (int)n; x++) {
          pos p = xyToPos(x, y);
          bool label = false;
          for(int i = 0; i < (int)labels.size(); i++) {
            if(labels[i].first == p) {
              putchar(labels[i].second);
              label = true;
              break;
            }
          }
          if(label) continue;

          color c = pieces[p];
          if(c == no_color)
            putchar('.');
          else
            putchar(c == white ? 'W' : 'B');
        }
        putchar('\n');
      }
      putchar('\n');
    }

  private:
    void updateNeighbourhood(pos p, int change) {
      if(change != 1 && change != -1)
        fail("May only change by 1 or -1 in updateNeighbourhood");

      size_t sx, sy;
      posToXY(&sx, &sy, p);
      int x = sx, y = sy;

      for(int nx = max(0, x-1), ex = min((int)n-1, x+1); nx <= ex; nx++)
        for(int ny = max(0, y-1), ey = min((int)n-1, y+1); ny <= ey; ny++) {
          p = xyToPos(nx, ny);
          char &neighCnt = neighbours[p];
          neighCnt += change;

          if((neighCnt == 0 || pieces[p] != no_color)
             && boardMarks.isMarked(p)) {

            size_t xx, yy; posToXY(&xx, &yy, p);
            if(0 <= xx && xx < n && 0 <= yy && yy < n)
              boardMarks.unmark(p);
          }
          else if(neighCnt > 0 && pieces[p] == no_color
                  && !boardMarks.isMarked(p)) {
            size_t xx, yy; posToXY(&xx, &yy, p);
            if(0 <= xx && xx < n && 0 <= yy && yy < n)
              boardMarks.mark(p);
          }
        }
    }

    void reevaluate() {
      brdValueBlackMove = brdValueWhiteMove = 0;
      for(int i = 1; i <= 6; i++) {
        brdValueWhiteMove +=
          sixStrips.howManyStripsWith(i, white) *
          weigths.get(WHITE_MOVE, i, WHITE_WEIGTH) -
          sixStrips.howManyStripsWith(i, black) *
          weigths.get(WHITE_MOVE, i, BLACK_WEIGTH);

        brdValueBlackMove +=
          sixStrips.howManyStripsWith(i, white) *
          weigths.get(BLACK_MOVE, i, WHITE_WEIGTH) -
          sixStrips.howManyStripsWith(i, black) *
          weigths.get(BLACK_MOVE, i, BLACK_WEIGTH);
      }
    }

    class SixStrips {
      private:
        enum {
          HORIZONTAL = 0,
          VERTICAL = 1,
          SKEW_UP = 2,
          SKEW_DOWN = 3,
        };

        enum {
          WH_TYPE = 0,
          BL_TYPE = 1,
        };

        struct StripContents {
          StripContents() { pieceCount[0] = pieceCount[1] = 0; }
          unsigned char pieceCount[2]; // indexed by WH_TYPE, BL_TYPE
        };

        size_t n;
        vector<StripContents> strips[4];
        vector<size_t> stripBalance[2];

      public:
        SixStrips(size_t n) {
          this->n = n;

          for(int i = 0; i < 4; i++) {
            strips[i].clear();
            strips[i].resize(n * n);
          }

          for(int i = 0; i < 2; i++) {
            stripBalance[i].clear();
            stripBalance[i].resize(7);
          }
        }

        void insertPiece(size_t px, size_t py, color c) {
          if(px >= n || py >= n || c == no_color)
            fail("Invalid insertPiece arguments");
          pieceUpdate(px, py, c == white ? WH_TYPE : BL_TYPE, +1);
        }

        void removePiece(size_t px, size_t py, color c) {
          if(px >= n || py >= n || c == no_color)
            fail("Invalid remove arguments");
          pieceUpdate(px, py, c == white ? WH_TYPE : BL_TYPE, -1);
        }

        size_t howManyStripsWith(size_t pieceCount, color c) {
          if(pieceCount == 0 || pieceCount > 6 || c == no_color)
            fail("Invalid howManyStripsWith arguments");
          return stripBalance[c == white ? WH_TYPE : BL_TYPE][pieceCount];
        }

      private:
        void pieceUpdate(size_t px, size_t py, size_t pieceType, int change) {
          // Poziome - jest ich (n - 5) * n
          // Indeksowane sa [0..(n-6)][0..n-1]
          // Pozycja x y zawiera sie w paskach [y][x - 5..x]
          // (Ostatni to na pewno x, a ma ich by�6)
          for(size_t x = max(0, (int)px - 5), ex = min(px, n - 6); x <= ex; x++)
            stripChange(HORIZONTAL, x, py, pieceType, change);

          // Pionowe - jest ich n * (n - 5)
          // Indeksowane sa [0..n-1][0..(n - 6)]
          // Pozycja x y zawiera sie w paskach [y-5..y][x]
          // (Jak wyzej)
          for(size_t y = max(0, (int)py - 5), ey = min(py, n - 6); y <= ey; y++)
            stripChange(VERTICAL, px, y, pieceType, change);

          // Skosne w gore - jest ich (n - 5) * (n - 5) (n * (n - 5) w praktyce)
          // Indeksowane sa [0..n-6][5..n-1] UWAGA!!! Nie od 0.
          // Pozycja x y zawiera sie w paskach [x-i][y+i], i in [0..5] oraz w zakresie
          for(size_t i = 0; i <= 5; i++) {
            int x = (int)px - i, y = (int)py + i;
            if(0 <= x && x <= (int)n-6 && 5 <= y && y <= (int)n-1)
              stripChange(SKEW_UP, x, y, pieceType, change);
          }

          // Skosne w dol - jest ich (n - 5) * (n - 5)
          // Indeksowane sa [0..n-6][0..n-6]
          // Pozycja x y zawiera sie w paskach [x-i][y-i], i in [0..5] oraz w zakresie
          for(size_t i = 0; i <= 5; i++) {
            int x = (int)px - i, y = (int)py - i;
            if(0 <= x && x <= (int)n-6 && 0 <= y && y <= (int)n-6)
              stripChange(SKEW_DOWN, x, y, pieceType, change);
          }
        }

        void stripChange(int stripType, size_t x, size_t y,
                         int pieceType, int change) {
          StripContents &sc = strips[stripType][y * n + x];
          balanceChange(sc, -1);
          sc.pieceCount[pieceType] += change;
          balanceChange(sc, +1);
        }

        void balanceChange(const StripContents &sc, int change) {
          // We do not account for empty strips
          if(sc.pieceCount[WH_TYPE] == 0 && sc.pieceCount[BL_TYPE] == 0)
            return;
          // We do not account thoes that have collision - black & white
          if(sc.pieceCount[WH_TYPE] > 0 && sc.pieceCount[BL_TYPE] > 0)
            return;
          // sc.pieceCount[WH_TYPE] > 0 && sc.pieceCount[BL_TYPE] > 0
          if(sc.pieceCount[WH_TYPE] > 0)
            stripBalance[WH_TYPE][sc.pieceCount[WH_TYPE]] += change;
          else if(sc.pieceCount[BL_TYPE] > 0)
            stripBalance[BL_TYPE][sc.pieceCount[BL_TYPE]] += change;
        }
    };

    class BoardMarks {
      private:
        static const short EMPTY = -10000;
        vector<pos> marksSet;
        vector<short> markPosInSet;
      public:
        BoardMarks(size_t posCount) { // O(n ^ 2)
          marksSet.clear();
          markPosInSet.clear();
          markPosInSet.resize(posCount, EMPTY);
        }

        void mark(pos p) { // O(1)
          marksSet.push_back(p);
          markPosInSet[p] = marksSet.size() - 1;
        }

        bool isMarked(pos p) { // O(1)
          return markPosInSet[p] != EMPTY;
        }

        void unmark(pos p) { // O(1)
          if(markPosInSet[p] != (int)marksSet.size() - 1) {
            pos replP = marksSet.back();
            swap(marksSet[markPosInSet[p]], marksSet.back());
            markPosInSet[replP] = markPosInSet[p];
            markPosInSet[p] = (int)marksSet.size() - 1;
          }

          markPosInSet[p] = EMPTY;
          marksSet.pop_back();
        }

        // Marked positions are returned in random order
        const vector<pos> &allMarked() { // O(1)
          return marksSet;
        }
    };

  private:
    size_t n, piecesPlaced;
    vector<color> pieces;
    vector<char> neighbours;
    BoardMarks boardMarks;
    SixStrips sixStrips;
    int brdValueBlackMove, brdValueWhiteMove;
    StripWeigths weigths;
};

#endif  // PH_CONNECT_SIX_BOARD_H
