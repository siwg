/**
 * \file
 *
 * Implementation of Gomoku game.
 *
 * This file is part of espy, the game solver.
 *
 * \author Dawid Dabrowski, Sebastian Lisiewski, Paweł Walczak.
 *
 */

#ifndef __Gomoku__
#define __Gomoku__

#include "Game.h"
#include "Settings.h"
#include<utility>
#include<vector>
#include<algorithm>
#include<map>

using namespace std;

class Gomoku : public Game {
public:
    static string name() {
        return "Gomoku";
    }
    
    const static int board_size = 3;

	const static int cross = 0; // white
	const static int circle = 1; // black
	const static int empty = 2; 

	const static int in_progress = 100;
    const static int white_wins = cross;
    const static int black_wins = circle;
    const static int draw = empty;

    const static int k = 3;

    typedef pair<int, int> move;
    typedef vector<vector<int> > board_type;
    board_type board;
    map<board_type, pair<move, move> > stefan;

    int player;
    move moves[board_size * board_size + 5];
    int last_move_nr;

    int gs;

    int dx[4];
    int dy[4];

    Gomoku() {
        board = board_type(board_size, vector<int>(board_size, empty));
        dx[0] = 1;
        dx[1] = 0;
        dx[2] = 1;
        dx[3] = -1;
        dy[0] = 0;
        dy[1] = 1;
        dy[2] = 1;
        dy[3] = 1;
        clear();
    }

    void update_gs() {
        for (int i = 0; i < board_size; ++i) {
            for (int j = 0; j < board_size; ++j) {
                for (int d = 0; d < 4; ++d) {
                    
                    if (i + (k-1)*dx[d] >= board_size ||
                        j + (k-1)*dy[d] >= board_size ||
                        i + (k-1)*dx[d] < 0) {
                        break;
                    }
                    if (board[i][j] != empty) {
                        bool ok = true;
                        for (int a = 1; a < k; ++a) {
                            if (board[i][j] !=
                                board[i+a*dx[d]][j+a*dy[d]]) {
                                ok = false;
                                break;
                            }
                        }
                        if (ok) {
                            gs = board[i][j];
                            return;
                        }
                    }
                }
            }
        }
        gs = in_progress;
    }

    int game_state() {
        return gs;
    }

    int game_state(int _gs) {
        return gs = _gs;
    }

    inline int wtm() const {
        return player == cross;
    }

    void clear(){
        for (int i = 0; i < board_size; ++i) {
            for (int j = 0; j < board_size; ++j) {
                board[i][j] = empty;
            }
        }
        stefan.clear();
        player = cross;
        last_move_nr = 0;
        gs = in_progress;
    }

    void prepare_moves(){
        stefan[board].second = make_pair(0, -1);
    } 

    move increment(move m) {
        if (m.second == board_size - 1) {
            ++m.first;
            m.second = 0;
        } else {
            ++m.second;
        }
        return m;
    }

    bool has_next_move(){
        stefan[board].first = stefan[board].second;
        while (true) {
            stefan[board].first = increment(stefan[board].first);
            if (stefan[board].first.first == board_size) {
                return false;
            }
            if (board[stefan[board].first.first][stefan[board].first.second] ==
                empty) {
                return true;
            }
        }
    }

    move get_next_move(){
        stefan[board].second = stefan[board].first;
        return stefan[board].second;
    }

    void make_move(const move mv){
        moves[last_move_nr++] = mv;
        board[mv.first][mv.second] = player;
        player ^= 1;
        update_gs();
    }

    void retract_move(){
        --last_move_nr;
        board[moves[last_move_nr].first][moves[last_move_nr].second] =
            empty;
        player ^= 1;
        update_gs();
    }

};

#endif
