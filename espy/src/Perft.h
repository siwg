/**
 * \file
 *
 * Implementation of minimax search.
 *
 */

#ifndef __Perft__
#define __Perft__

#include "Search.h"
#include "Settings.h"

template<class G>
class Perft : public Search<G> {

	const static int max_depth = Settings::maximum_game_length; /**< Maximum search depth. */

	G g;

	int value[max_depth];
	int ply;
	int nds;
	int dep;
	
public:
	void search();
	void position(G &);
	int nodes() { return nds; }
	int depth() { return dep; }
	int depth(int _dep) { return dep = _dep; }

	int result() { return value[0]; }
};

template<class G> void Perft<G>::position(G & position)
{
	g = position;
	g.prepare_moves();

	ply = 0;
	value[0] = position.game_state();

	nds = 0;
}

template<class G> void Perft<G>::search()
{
	if (ply > dep) {
		value[ply] = G::draw;
		return;
	}

	nds++;

	switch (g.game_state()) {
	case G::in_progress:
		g.prepare_moves();
		
		//value[ply] = g.wtm() ? G::black_wins : G::white_wins;
        if(g.wtm())
            value[ply] = G::black_wins;
        else
            value[ply] = G::white_wins;
		
		while (g.has_next_move()) {
			typename G::move mv = g.get_next_move();
			g.make_move(mv); ply++;
			search();
			g.retract_move(); ply--;
			if (g.wtm()) {
				value[ply] = max(value[ply+1],value[ply]);
			} else {
				value[ply] = min(value[ply+1],value[ply]);
			}
		}
		break;
	case G::white_wins:
		value[ply] = G::white_wins;
		break;
	case G::black_wins:
		value[ply] = G::black_wins;
		break;
	case G::draw:
		value[ply] = G::draw;
		break;
	case G::illegal:
		break;
	}
}

#endif /* __Perft__ */
