/**
 * \file
 *
 * Header file of a Chess game class
 *
 * This file is part of espy, the game solver.
 *
 * \author Rafał Hryciuk, Jacek Karaśkiewicz
 *
 */

#ifndef __CHESS__
#define __CHESS__

#include <vector>

#include "Game.h"
#include "Settings.h"


class Chess : public Game {

    public:
        static string name() {
          return "Chess";
        }
        
        const static int max_depth = Settings::maximum_game_length;

        const static int board_size = 8;

        typedef int move;

        enum Piece {WP = 1 , WR = 2, WN = 3, WB = 4, WQ = 5, WK = 6, BP = 7,
            BR = 8, BN = 9, BB = 10, BQ = 11, BK = 12, NULL_PIECE = 0};

        typedef struct {
            Piece fields[board_size][board_size];
        } Board;

        typedef struct {
            Board board;
            bool canWhiteCastleKingside, canWhiteCastleQueenside;
            bool canBlackCastleKingside, canBlackCastleQueenside;
            bool isWhiteInCheck, isBlackInCheck;
            bool isWhiteMated, isBlackMated;
            bool isStalemate, isRepetitionDraw; /* stalemate - pat, repetition draw - remis przez trzykrotne powtĂłrzenie pozycji */
            int whiteEnpassantColumn, blackEnpassantColumn; /* -1 - cannot take en passant */
        } State;

        typedef vector<move> MovesHistory;

        void prepare_moves();
        bool has_next_move();
        move get_next_move();

    private:

        State state;

        vector<move> preparedMoves;

        std::size_t movesIndex;
        
        
        bool isPawn(Piece piece) const;

        bool isKnight(Piece piece) const;

        bool isBishop(Piece piece) const;

        bool isRook(Piece piece) const;

        bool isQueen(Piece piece) const;

        bool isKing(Piece piece) const;

        bool isWhite(Piece piece) const;

        bool isBlack(Piece piece) const;

        std::vector<move> generatePossiblePawnMoves(Piece piece, int sourceColumn, int sourceRow) const;

        std::vector<move> generatePossibleKnightMoves(Piece piece, int sourceColumn, int sourceRow) const;

        std::vector<move> generatePossibleBishopMoves(Piece piece, int sourceColumn, int sourceRow) const;

        std::vector<move> generatePossibleRookMoves(Piece piece, int sourceColumn, int sourceRow) const;

        std::vector<move> generatePossibleQueenMoves(Piece piece, int sourceColumn, int sourceRow) const;

        std::vector<move> generatePossibleKingMoves(Piece piece, int sourceColumn, int sourceRow) const;

        move generateMoveNotation(Piece sourcePiece, Piece destinationPiece,
             int sourceColumn, int sourceRow, int destinationColumn, int destinationRow) const;
             
        std::vector<move> generatePossibleMoves(Piece piece, int sourceColumn, int sourceRow) const;

        bool isPositionCorrect(int column, int row) const;

        bool isMovePossible(Piece piece, int column, int row) const;

        bool isFieldEmpty(int column, int row) const;

        Piece getEnemyOccupyingField(Piece piece, int column, int row) const;

        move attackMove(int sourceColumn, int sourceRow, int destColumn, int destRow, Piece piece) const;

        move changePositionMove(int sourceColumn, int sourceRow, int destColumn, int destRow, Piece piece) const;

        void knightMovesInOneDimension(Piece piece, int sourceColumn, int sourceRow,
            int size, int array1[], int array2[], std::vector<Chess::move> moves) const;
        void generateMovesInOneDirection(Piece piece, int sourceColumn, int sourceRow,
            int horizonatlFactor, int verticalFactor, std::vector<Chess::move> moves, int maxChange) const;

};

#endif
