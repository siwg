/**
 * \file
 *
 * Implementation of minimax search with alpha-beta cut-offs.
 *
 */

#ifndef __MinimaxAB__
#define __MinimaxAB__

#include "Search.h"
#include "Settings.h"

template<class G>
class MinimaxAB : public Search<G> {

	const static int max_depth = Settings::maximum_game_length; /**< Maximum search depth. */

	G g;

	int value[max_depth];
	int alpha[max_depth];
	int beta[max_depth];
	int ply;
	int nds;
	int depth_limit;

	void init() {
		g.prepare_moves();
		ply = 0; nds = 1;
		value[ply] = g.game_state();
		alpha[ply] = G::minfty;
		beta [ply] = G::pinfty;
		if (g.game_state() == G::in_progress) {
			if (g.wtm()) {
				value[ply] = G::minfty;
			} else {
				value[ply] = G::pinfty;
			}
		}
	}
	
	void propagateAB() {
	    if (g.wtm()) {
	        if (value[ply] != G::pinfty)
	            alpha[ply-1] = max(alpha[ply-1], value[ply]);
	    } else
	        if (value[ply] != G::minfty)
	            beta[ply-1] = min(beta[ply-1], value[ply]);
    }

public:
	MinimaxAB<G>() {
		g.clear();
		depth_limit = max_depth;

		init();
	}

	void search();
	void search_single_node();

	void position(G &);
	int nodes() { return nds; }
	int depth() { return depth_limit; }
	int depth(int _dep) { return depth_limit = _dep; }

	int result() { 
		if (ply > 0 || g.has_next_move()) { return G::in_progress; }

		return value[0]; 
	}
};

template<class G> void MinimaxAB<G>::position(G & position)
{
	g = position;
        init();
}

template<class G> void MinimaxAB<G>::search_single_node()
{
	if (ply == depth_limit) {
		value[ply] = G::unknown; 
		g.retract_move(); 
		ply--;
	}

	bool cutoff = false;

	while ((cutoff || !g.has_next_move() || beta[ply] <= alpha[ply]) && ply > 0) {
		g.retract_move();
		propagateAB();
		ply--;

		switch (value[ply+1]) {
		case G::white_wins:
		case G::draw:
		case G::black_wins:
			if (g.wtm()) {
				value[ply] = max(value[ply], value[ply+1]);
				cutoff = value[ply] == G::white_wins;
			} else {
				value[ply] = min(value[ply], value[ply+1]);
				cutoff = value[ply] == G::black_wins;
			}
			break;
		}
	}

	if (g.has_next_move()) {
		g.make_move(g.get_next_move()); ply++; nds++;
		g.prepare_moves();
		alpha[ply] = alpha[ply-1];
		beta[ply] = beta[ply-1];

		value[ply] = g.game_state();

		if (value[ply] == G::in_progress) {
			if (g.wtm()) {
				value[ply] = G::minfty;
			} else {
				value[ply] = G::pinfty;
			}
		}
	}
}

template<class G> void MinimaxAB<G>::search()
{
	while (result() == G::in_progress) {
		search_single_node();
	}
}

#endif /* __MinimaxAB__ */
