#include <vector>
#include <cstdio>
#include <cassert>
#include <cstdlib>
#include <ctime>
using namespace std;

const int white = -11;
const int black = +24;
const int no_color = +34;
typedef char color;

void fail(const char *message) {
  fprintf(stderr, message);
  exit(1);
}

class SixStrips {
  private:
    enum {
      HORIZONTAL = 0,
      VERTICAL = 1,
      SKEW_UP = 2,
      SKEW_DOWN = 3,
    };

    enum {
      WH_TYPE = 0,
      BL_TYPE = 1,
    };

    struct StripContents {
      StripContents() { pieceCount[0] = pieceCount[1] = 0; }
      unsigned char pieceCount[2]; // indexed by WH_TYPE, BL_TYPE
    };

    size_t n;
    vector<StripContents> strips[4];
    vector<size_t> stripBalance[2];

  public:
    SixStrips(size_t n) {
      this->n = n;

      for(int i = 0; i < 4; i++) {
        strips[i].clear();
        strips[i].resize(n * n);
      }

      for(int i = 0; i < 2; i++) {
        stripBalance[i].clear();
        stripBalance[i].resize(7);
      }
    }

    void insertPiece(size_t px, size_t py, color c) {
      if(px >= n || py >= n || c == no_color)
        fail("Invalid insertPiece arguments");
      pieceUpdate(px, py, c == white ? WH_TYPE : BL_TYPE, +1);
    }

    void removePiece(size_t px, size_t py, color c) {
      if(px >= n || py >= n || c == no_color)
        fail("Invalid remove arguments");
      pieceUpdate(px, py, c == white ? WH_TYPE : BL_TYPE, -1);
    }

    size_t howManyStripsWith(size_t pieceCount, color c) {
      if(pieceCount == 0 || pieceCount > 6 || c == no_color)
        fail("Invalid howManyStripsWith arguments");
      return stripBalance[c == white ? WH_TYPE : BL_TYPE][pieceCount];
    }

  private:
    void pieceUpdate(size_t px, size_t py, size_t pieceType, int change) {
      // Poziome - jest ich (n - 5) * n
      // Indeksowane sa [0..(n-6)][0..n-1]
      // Pozycja x y zawiera sie w paskach [y][x - 5..x]
      // (Ostatni to na pewno x, a ma ich by�6)
      for(size_t x = max(0, (int)px - 5), ex = min(px, n - 6); x <= ex; x++)
        stripChange(HORIZONTAL, x, py, pieceType, change);

      // Pionowe - jest ich n * (n - 5)
      // Indeksowane sa [0..n-1][0..(n - 6)]
      // Pozycja x y zawiera sie w paskach [y-5..y][x]
      // (Jak wyzej)
      for(size_t y = max(0, (int)py - 5), ey = min(py, n - 6); y <= ey; y++)
        stripChange(VERTICAL, px, y, pieceType, change);

      // Skosne w gore - jest ich (n - 5) * (n - 5) (n * (n - 5) w praktyce)
      // Indeksowane sa [0..n-6][5..n-1] UWAGA!!! Nie od 0.
      // Pozycja x y zawiera sie w paskach [x-i][y+i], i in [0..5] oraz w zakresie
      for(size_t i = 0; i <= 5; i++) {
        int x = (int)px - i, y = (int)py + i;
        if(0 <= x && x <= (int)n-6 && 5 <= y && y <= (int)n-1)
          stripChange(SKEW_UP, x, y, pieceType, change);
      }

      // Skosne w dol - jest ich (n - 5) * (n - 5)
      // Indeksowane sa [0..n-6][0..n-6]
      // Pozycja x y zawiera sie w paskach [x-i][y-i], i in [0..5] oraz w zakresie
      for(size_t i = 0; i <= 5; i++) {
        int x = (int)px - i, y = (int)py - i;
        if(0 <= x && x <= (int)n-6 && 0 <= y && y <= (int)n-6)
          stripChange(SKEW_DOWN, x, y, pieceType, change);
      }
    }

    void stripChange(int stripType, size_t x, size_t y,
                     int pieceType, int change) {
      StripContents &sc = strips[stripType][y * n + x];
      balanceChange(sc, -1);
      sc.pieceCount[pieceType] += change;
      balanceChange(sc, +1);
    }

    void balanceChange(const StripContents &sc, int change) {
      // We do not account for empty strips
      if(sc.pieceCount[WH_TYPE] == 0 && sc.pieceCount[BL_TYPE] == 0)
        return;
      // We do not account thoes that have collision - black & white
      if(sc.pieceCount[WH_TYPE] > 0 && sc.pieceCount[BL_TYPE] > 0)
        return;
      // sc.pieceCount[WH_TYPE] > 0 && sc.pieceCount[BL_TYPE] > 0
      if(sc.pieceCount[WH_TYPE] > 0)
        stripBalance[WH_TYPE][sc.pieceCount[WH_TYPE]] += change;
      else if(sc.pieceCount[BL_TYPE] > 0)
        stripBalance[BL_TYPE][sc.pieceCount[BL_TYPE]] += change;
    }
};

class BrutSixStrips {
  private:
    int n;

    vector<color> board;
  public:
    BrutSixStrips(size_t n) {
      this->n = n;
      board.clear();
      board.resize(n * n, no_color);
    }

    color pieceAt(size_t px, size_t py) {
      return board[py * n + px];
    }

    void insertPiece(size_t px, size_t py, color c) {
      board[py * n + px] = c;
    }

    void removePiece(size_t px, size_t py, color c) {
      assert(board[py * n + px] == c);
      board[py * n + px] = no_color;
    }

    size_t howManyStripsWith(size_t pieceCount, color c) {
      size_t cnt = 0;
      cnt += countBy(1, 0, pieceCount, c);
      cnt += countBy(0, 1, pieceCount, c);
      cnt += countBy(1, 1, pieceCount, c);
      cnt += countBy(1, -1, pieceCount, c);
      return cnt;
    }

    void printBoard() {
      for(int x = 0; x < n; x++) 
        putchar('-');
      putchar('\n');

      for(int y = 0; y < n; y++) {
        for(int x = 0; x < n; x++) {
          color c = board[y * n + x];
          if(c == no_color)
            putchar('.');
          else
            putchar(c == white ? 'W' : 'B');
        }
        putchar('\n');
      }
    }

  private:
    size_t countBy(int ox, int oy, size_t pieceCount, color c) {
      size_t cnt = 0;
      for(int x = 0; x < n; x++)
        for(int y = 0; y < n; y++) {
          int whCnt = 0, blCnt = 0;
          bool onBoard = true;
          for(int i = 0; i < 6; i++) {
            int nx = ox * i + x, ny = oy * i + y;
            if(!(0 <= nx && nx < n && 0 <= ny && ny < n)) {
              onBoard = false;
              break;
            }
            if(board[ny * n + nx] == white)
              whCnt++;
            if(board[ny * n + nx] == black)
              blCnt++;
          }
          if(onBoard && (whCnt == 0 || blCnt == 0)) {
            if(c == white && whCnt == (int)pieceCount)
              cnt++;
            else if(c == black && blCnt == (int)pieceCount)
              cnt++;
          }

        }
      return cnt;
    }
};

void unitTest();
void brutTest();

int main() {
  srand(time(NULL)); // Make it deterministic

  brutTest();
  unitTest();
  return 0;
}


void brutTest() {
  const size_t N = 8;
  for(int start = 0; start < 10; start++) {
    SixStrips ss(N);
    BrutSixStrips bss(N);
    for(int i = 0; i < (int)(N*N*5); i++) {
      int x = rand() % N, y = rand() % N;
      color c = bss.pieceAt(x, y);
      color newC = (rand() % 2 == 0 ? white : black);
      if(c == no_color) {
        ss.insertPiece(x, y, newC);
        bss.insertPiece(x, y, newC);
      }
      else {
        ss.removePiece(x, y, c);
        bss.removePiece(x, y, c);
      }

      for(int i = 1; i <= 6; i++) {
        if(ss.howManyStripsWith(i, white) != bss.howManyStripsWith(i, white)) {
          bss.printBoard();
          fprintf(stderr, "wh i = %d\n ss=%d bss=%d", i, (int)ss.howManyStripsWith(i, white), (int)bss.howManyStripsWith(i, white));
          fail("Assertion fail");
        }
        if(ss.howManyStripsWith(i, black) != bss.howManyStripsWith(i, black)) {
          bss.printBoard();
          fprintf(stderr, "bl i = %d\n ss=%d bss=%d", i, (int)ss.howManyStripsWith(i, black), (int)bss.howManyStripsWith(i, black));
          fail("Assertion fail");
        }
      }
    }
  }
  puts("OK");
}
void unitTest() {
  SixStrips ss(30);

  for(int i = 1; i <= 6; i++) {
    assert(ss.howManyStripsWith(i, white) == 0);
    assert(ss.howManyStripsWith(i, black) == 0);
  }

  ss.insertPiece(10, 10, white);

  assert(ss.howManyStripsWith(1, white) == 24);
  assert(ss.howManyStripsWith(1, black) == 0);
  for(int i = 2; i <= 6; i++) {
    assert(ss.howManyStripsWith(i, white) == 0);
    assert(ss.howManyStripsWith(i, black) == 0);
  }

  ss.removePiece(10, 10, white);

  for(int i = 1; i <= 6; i++) {
    assert(ss.howManyStripsWith(i, white) == 0);
    assert(ss.howManyStripsWith(i, black) == 0);
  }

  ss.insertPiece(10, 10, white);
  ss.insertPiece(11, 10, white);
  assert(ss.howManyStripsWith(1, white) == 38);
  assert(ss.howManyStripsWith(1, black) == 0);
  assert(ss.howManyStripsWith(2, white) == 5);
  assert(ss.howManyStripsWith(2, black) == 0);
  for(int i = 3; i <= 6; i++) {
    assert(ss.howManyStripsWith(i, white) == 0);
    assert(ss.howManyStripsWith(i, black) == 0);
  }
  ss.removePiece(10, 10, white);
  ss.removePiece(11, 10, white);

  ss.insertPiece(10, 10, white);
  ss.insertPiece(10, 11, white);
  assert(ss.howManyStripsWith(1, white) == 38);
  assert(ss.howManyStripsWith(1, black) == 0);
  assert(ss.howManyStripsWith(2, white) == 5);
  assert(ss.howManyStripsWith(2, black) == 0);
  for(int i = 3; i <= 6; i++) {
    assert(ss.howManyStripsWith(i, white) == 0);
    assert(ss.howManyStripsWith(i, black) == 0);
  }
  ss.removePiece(10, 10, white);
  ss.removePiece(10, 11, white);

  ss.insertPiece(10, 10, white);
  ss.insertPiece(11, 11, white);
  assert(ss.howManyStripsWith(1, white) == 38);
  assert(ss.howManyStripsWith(1, black) == 0);
  assert(ss.howManyStripsWith(2, white) == 5);
  assert(ss.howManyStripsWith(2, black) == 0);
  for(int i = 3; i <= 6; i++) {
    assert(ss.howManyStripsWith(i, white) == 0);
    assert(ss.howManyStripsWith(i, black) == 0);
  }
  ss.removePiece(10, 10, white);
  ss.removePiece(11, 11, white);

  ss.insertPiece(10, 10, white);
  ss.insertPiece(11, 9, white);
  assert(ss.howManyStripsWith(1, white) == 38);
  assert(ss.howManyStripsWith(1, black) == 0);
  assert(ss.howManyStripsWith(2, white) == 5);
  assert(ss.howManyStripsWith(2, black) == 0);
  for(int i = 3; i <= 6; i++) {
    assert(ss.howManyStripsWith(i, white) == 0);
    assert(ss.howManyStripsWith(i, black) == 0);
  }
  ss.removePiece(10, 10, white);
  ss.removePiece(11, 9, white);

  ss.insertPiece(10, 10, white);
  ss.insertPiece(11, 11, white);
  ss.insertPiece(12, 12, white);
  ss.insertPiece(13, 13, white);
  ss.insertPiece(14, 14, white);
  ss.insertPiece(15, 15, white);
  assert(ss.howManyStripsWith(1, white) == 6 * 24 - 6 * 6 + 2);
  assert(ss.howManyStripsWith(2, white) == 2);
  assert(ss.howManyStripsWith(3, white) == 2);
  assert(ss.howManyStripsWith(4, white) == 2);
  assert(ss.howManyStripsWith(5, white) == 2);
  assert(ss.howManyStripsWith(6, white) == 1);
  for(int i = 1; i <= 6; i++)
    assert(ss.howManyStripsWith(i, black) == 0);

  puts("OK");
}
