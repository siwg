/**
 * \file
 *
 * Implementation of Connect Four game.
 *
 * This file is part of espy, the game solver.
 *
 * \author XXX.
 *
 */

#ifndef __ConnectFour__
#define __ConnectFour__

#include <assert.h>

#include "Game.h"
#include "Settings.h"

class ConnectFour : public Game {
public:
	const static int max_depth = Settings::maximum_game_length;

	const static int cross = 0;
	const static int circle = 1;
	const static int empty = 2;

  const static int width = 6;
  const static int height = 5;
  const static int board_size = width * height;
  const static int crossLength = 4;


	struct board {
		int sq[board_size + 1];
	} bd;

	int ply;
	int mv[max_depth];
	int played[max_depth];

public:
	typedef int move;

	static string name() {
		return "Connect Four";
	}

	ConnectFour() {
		clear();
	};

	void prepare_moves() {
		mv[ply] = 0;
	};

	bool has_next_move() {
		while (bd.sq[mv[ply]] != empty) mv[ply]++;
		return mv[ply] < width;
	}

	move get_next_move() {
		while (bd.sq[mv[ply]] != empty) mv[ply]++;
		return mv[ply]++;
	}

	void clear() {
		ply = 0;
		for (int i = 0; i < board_size + 1; i++)
			bd.sq[i] = empty;
	}

	void make_move(const move column) {
    // Szukamy najnizszej pustej kratki w kolumnie column
    assert(0 <= column && column < width);
    assert(bd.sq[column] == empty);
    int destCell = column + width * (height - 1);
    while(bd.sq[destCell] != empty) destCell -= width;

		bd.sq[destCell] = ply & 1; // wstawiamy x lub o
		played[ply] = destCell; // gdzie sie ruszylismy
		++ply; // dorzucamy na stos

		/* set game state */

		game_state(in_progress);

		if (ply == board_size) game_state(draw);

    // Check horizontal
    for(int y = 0; y < height; y++)
      for(int x = 0; x < width - crossLength + 1; x++) {
        int lineType = checkLine(crossLength, bd.sq, width * y + x, 1);
        if(lineType == 0) {
          game_state(white_wins);
          return;
        }
        else if(lineType == 1) {
          game_state(black_wins);
          return;
        }
      }

    // Check vertical
    for(int y = 0; y < height - crossLength + 1; y++)
      for(int x = 0; x < width; x++) {
        int lineType = checkLine(crossLength, bd.sq, width * y + x, width);
        if(lineType == 0) {
          game_state(white_wins);
          return;
        }
        else if(lineType == 1) {
          game_state(black_wins);
          return;
        }
      }

    // Check bottom rigth
    for(int y = 0; y < height - crossLength + 1; y++)
      for(int x = 0; x < width - crossLength + 1; x++) {
        int lineType = checkLine(crossLength, bd.sq, width * y + x, width + 1);
        if(lineType == 0) {
          game_state(white_wins);
          return;
        }
        else if(lineType == 1) {
          game_state(black_wins);
          return;
        }
      }

    // Check top rigth
    for(int y = crossLength - 1; y < height; y++)
      for(int x = 0; x < width - crossLength + 1; x++) {
        int lineType = checkLine(crossLength, bd.sq, width * y + x, -width + 1);
        if(lineType == 0) {
          game_state(white_wins);
          return;
        }
        else if(lineType == 1) {
          game_state(black_wins);
          return;
        }
      }
	}

	void retract_move() {
		--ply;
		bd.sq[played[ply]] = empty;
	}

	inline int wtm() const {
		return (ply & 1) == 0;
	}

	void print_board() {
	        char s[3] = { 'o', 'x', ' ' };
	        
		for (int i = 0; i < height; i++) {
			cout << "|";
			for (int j = 0; j < width; j++) {
				cout << s[bd.sq[i*width+j]];
			}
			cout << "|\n";
		}
		cout << "+";
		for (int j = 0; j < width; j++) {
			cout << "-";
		}
		cout << "+\n";
	}

 private:
  // Returns empty, if there was no line of length lineLength
  // Otherwise, returns 0 or 1, depending on what values were
  // found on the line.
  //
  // Makes moves, by adding step to startPos.
  //
  // Note: Doesn't check for wrapping or overflow.
  int checkLine(int lineLength, int *board, int startPos, int step) {
    int cand = board[startPos];

    for(int i = 0; i < lineLength; i++) {
      if(board[startPos + step * i] != cand)
        return empty;
    }

    return cand;
  }
};

#endif
