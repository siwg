/**
 * \file
 *
 * Implementation of minimax search.
 *
 */

#ifndef __ReversiKillerMinimax__
#define __ReversiKillerMinimax__

#include "Search.h"
#include "Settings.h"
#include "Reversi.h"

class ReversiKillerMinimax : public Search<Reversi> {

	const static int max_depth = Settings::maximum_game_length; /**< Maximum search depth. */
 
  const static Reversi::move NO_MOVE = -1;

	Reversi g;

	int value[max_depth];
	int ply;
	int nds;
	int depth_limit;
	Reversi::move killer_move[max_depth];
	Reversi::move moves[max_depth];
	bool tried_killer[max_depth];


	void init() {
		g.prepare_moves();
		ply = 0; nds = 1;
		value[ply] = g.game_state();
		for (int i = 0; i < max_depth; ++i){
		  killer_move[i] = NO_MOVE;
		  tried_killer[i] = false;
		}
		if (g.game_state() == Reversi::in_progress) {
			if (g.wtm()) {
				value[ply] = Reversi::minfty;
			} else {
				value[ply] = Reversi::pinfty;
			}
		}
	}

public:
	ReversiKillerMinimax() {
		g.clear();
		depth_limit = max_depth;

		init();
	}

	void search();
	void search_single_node();

	void position(Reversi &);
	int nodes() { return nds; }
	int depth() { return depth_limit; }
	int depth(int _dep) { return depth_limit = _dep; }

	int result() { 
		if (ply > 0 || g.has_next_move()) { return Reversi::in_progress; }

		return value[0]; 
	}
};

void ReversiKillerMinimax::position(Reversi & position)
{
	g = position;
        init();
}

void ReversiKillerMinimax::search_single_node()
{
	if (ply == depth_limit) {
		value[ply] = Reversi::unknown; 
		g.retract_move(); 
		ply--;
	}

	bool cutoff = false;

	while ((cutoff || !g.has_next_move()) && ply > 0) {
		g.retract_move(); tried_killer[ply] = false; ply--;

		switch (value[ply+1]) {
		case Reversi::white_wins:
		case Reversi::draw:
		case Reversi::black_wins:
			if (g.wtm()) {
				value[ply] = max(value[ply], value[ply+1]);
				cutoff = value[ply] == Reversi::white_wins;
			} else {
				value[ply] = min(value[ply], value[ply+1]);
				cutoff = value[ply] == Reversi::black_wins;
			}
			if (cutoff)
			  killer_move[ply] = moves[ply];
			break;
		}
	}
	
	Reversi::move m = NO_MOVE;
	if (killer_move[ply] != NO_MOVE && !tried_killer[ply]) {
    tried_killer[ply] = true;
    if (g.checkMove(killer_move[ply])) 
      m = killer_move[ply];
  }
  if (m == NO_MOVE && g.has_next_move()){
    m = g.get_next_move();
    if (m == killer_move[ply]){
      if (g.has_next_move())
        m = g.get_next_move();
      else
        m = NO_MOVE;
    }
  }
      
  if (m != NO_MOVE) {
    g.make_move(m); moves[ply] = m; ply++; nds++;
    g.prepare_moves();

    value[ply] = g.game_state();

    if (value[ply] == Reversi::in_progress) {
      if (g.wtm()) {
        value[ply] = Reversi::minfty;
      } else {
        value[ply] = Reversi::pinfty;
      }
    }  
  }
}

void ReversiKillerMinimax::search()
{
	while (result() == Reversi::in_progress) {
		search_single_node();
	}
}

#endif /* __ReversiKillerMinimax__ */
