//WERSJA TESTOWA

/**
 * \file
 *
 * Implementation of PN search.
 *
 */

#ifndef __PN__
#define __PN__

#include <stdio.h>
#include <iostream>
#include "Search.h"
#include "Settings.h"
#include <vector>
#include "Game.h"

using namespace std;

enum color {
	black,
	white
};


int plusInfty = 2000000000;

class Node {
public:
	Node *parent;
	int white_moves_to_win,
		black_moves_to_win;    
	std::vector<Node> children;
	enum color color;
	Game::move move_that_got_us_here;
	
public:
	Node(){
	}
	
Node(int white_moves_to_win, int black_moves_to_win, enum color color)
	:white_moves_to_win(white_moves_to_win),
		black_moves_to_win(black_moves_to_win),
		color(color),
		move_that_got_us_here(-1) {
	}
		
};
	
template<class G>
class PN : public Search<G> {
public:
	Node tree;
	G g;    

	void wypisz() {
		wypisz(tree, "");
	}

	void wypisz(Node &n, std::string prefix = "") {
		string s = prefix + " " + g.print_move(n.move_that_got_us_here);

		if (n.move_that_got_us_here != -1) {
			g.make_move(n.move_that_got_us_here);
		}

		printf("%s (%d, %d, %d)\n", s.c_str(), n.white_moves_to_win, n.black_moves_to_win, n.color);
	    
		g.print_board();

		for (std::vector<Node>::iterator i = n.children.begin(); i != n.children.end(); i++) {
	    
			wypisz(*i, s);
		}

		g.retract_move();
	}

	void init() {
		tree = Node(1, 1, white);
		tree.parent = NULL;
	}
  
	void search_single_node(){        
		Node *current_node;
		int terazIle;
        
		bool fini = (result() != G::in_progress);

		current_node = &tree;

		Node *best_node = NULL;

		while (!current_node->children.empty()) {
			terazIle = plusInfty + 1;
			for (std::vector<Node>::iterator j = current_node->children.begin(); j != current_node->children.end(); j++) {
				if ((current_node->color == black) && (j->black_moves_to_win < terazIle)) {
					terazIle = j->black_moves_to_win;
					best_node = &(*j);
				}
                
				else if ((current_node->color == white) && (j->white_moves_to_win < terazIle)) {
					terazIle = j->white_moves_to_win;
					best_node = &(*j);
				}                
			}
			if (terazIle == plusInfty) { // obsługa remisów
				terazIle = 0;

				for (std::vector<Node>::iterator j = current_node->children.begin(); j != current_node->children.end(); j++) {
					if ((current_node->color == white) && (j->black_moves_to_win > terazIle)) {
						terazIle = j->black_moves_to_win;
						best_node = &(*j);
					}
                
					else if ((current_node->color == black) && (j->white_moves_to_win > terazIle)) {
						terazIle = j->white_moves_to_win;
						best_node = &(*j);
					}                
				}
			}

			if (best_node != NULL)
				current_node = best_node;
			g.make_move(current_node->move_that_got_us_here);
		}


		switch (g.game_state()) {
		case G::white_wins:
		case G::black_wins:
/*		  cout <<"search should be done \n";
		  g.print_board();
	  
		  while ((current_node = current_node->parent) != NULL) {
		    g.retract_move();
		    g.print_board();
		    cout << "wm "<<(current_node -> white_moves_to_win) <<" bm "<< (current_node->black_moves_to_win)<<endl;
		for (vector<Node>::iterator i = current_node->children.begin(); i != current_node->children.end(); i++) {
			cout << (g.print_move(i -> move_that_got_us_here)) << " pn " << (i -> white_moves_to_win) << " dpn " 
			     << (i -> black_moves_to_win) << endl;
	    
		}		     
		}*/
		  break;
		case G::draw:
//		  cout <<"search should be done (draw)\n";
			break;
		case G::in_progress:
			g.prepare_moves();

			bool no_moves = true;

			while (g.has_next_move()) {
			  no_moves = false;
				typename G::move m = g.get_next_move();
				g.make_move(m);
				Node n = Node(1, 1, current_node->color == black ? white : black);
				n.move_that_got_us_here = m;
				n.parent = current_node;
				if (g.game_state() == G::white_wins) {
					n.black_moves_to_win = plusInfty;
					n.white_moves_to_win = 0;
				}
				else if (g.game_state() == G::black_wins) {
					n.black_moves_to_win = 0;
					n.white_moves_to_win = plusInfty;
				} else if (g.game_state() != G::in_progress) {
					n.black_moves_to_win = plusInfty;
					n.white_moves_to_win = plusInfty;
				}

				current_node->children.push_back(n);
				
				g.retract_move();
			}


			if (no_moves) { cout << "no moves!" << endl; }
		     
			int new_black, new_white;

			if (current_node -> color == black) {
				new_black = plusInfty,
					new_white = 0;
				for (int i = 0; i < current_node->children.size(); i++) {
					new_black = min(new_black, current_node->children[i].black_moves_to_win);
					if (new_white != plusInfty) {
						if (current_node->children[i].white_moves_to_win == plusInfty)
							new_white = plusInfty;
						else
							new_white += current_node->children[i].white_moves_to_win;
					}
				}
			}
			else {
				new_black = 0,
					new_white = plusInfty;
                    
				for (int i = 0; i < current_node->children.size(); i++) {
					new_white = min(new_white, current_node->children[i].white_moves_to_win);
					if (new_black != plusInfty) {
						if (current_node->children[i].black_moves_to_win == plusInfty)
							new_black = plusInfty;
						else
							new_black += current_node->children[i].black_moves_to_win;
					}
				}
			}
			current_node->white_moves_to_win = new_white;
			current_node->black_moves_to_win = new_black;
		}	     

		if (current_node == NULL) return;

		while ((current_node = current_node->parent) != NULL) {
			g.retract_move();

			int new_black, new_white;

			if (current_node -> color == black) {
				new_black = plusInfty,
					new_white = 0;
                    
				for (int i = 0; i < current_node->children.size(); i++) {
					new_black = min(new_black, current_node->children[i].black_moves_to_win);
					if (new_white != plusInfty) {
						if (current_node->children[i].white_moves_to_win == plusInfty)
							new_white = plusInfty;
						else
							new_white += current_node->children[i].white_moves_to_win;
					}
				}
			}
			else {
				new_black = 0,
					new_white = plusInfty;
                    
				for (int i = 0; i < current_node->children.size(); i++) {
					new_white = min(new_white, current_node->children[i].white_moves_to_win);
					if (new_black != plusInfty) {
						if (current_node->children[i].black_moves_to_win == plusInfty)
							new_black = plusInfty;
						else
							new_black += current_node->children[i].black_moves_to_win;
					}
				}
			}
			current_node->white_moves_to_win = new_white;
			current_node->black_moves_to_win = new_black;
		} 

		if (fini != (result() != G::in_progress)) {
		  cout << "result changed!!!" << endl;
		}
        
	}
    
	int result() { 
		if (tree.white_moves_to_win == 0) {
			return G::white_wins;
		}
		if (tree.black_moves_to_win == 0) {
			return G::black_wins;
		}

		return G::in_progress; 
	}

	void position(G&);
};

template<class G> void PN<G>::position(G & position)
{
        init();
	g = position;
	tree.color = (g.wtm() ? white : black);
}


/*
  template<class G>
  class PN : public Search<G> {

  const static int max_depth = Settings::maximum_game_length; 

  G g;

  int value[max_depth];
  int ply;
  int nds;
  int depth_limit;
	


  void init() {
  g.prepare_moves();
  ply = 0; nds = 1;
  value[ply] = g.game_state();
  if (g.game_state() == G::in_progress) {
  if (g.wtm()) {
  value[ply] = G::minfty;
  } else {
  value[ply] = G::pinfty;
  }
  }
  }

  public:
  PN<G>() {
  g.clear();
  depth_limit = max_depth;

  init();
  }

  void search();
  void search_single_node();

  void position(G &);
  int nodes() { return nds; }
  int depth() { return depth_limit; }
  int depth(int _dep) { return depth_limit = _dep; }

  int result() { 
  if (ply > 0 || g.has_next_move()) { return G::in_progress; }

  return value[0]; 
  }
  };

  template<class G> void PN<G>::position(G & position)
  {
  g = position;
  init();
  }

  template<class G> void PN<G>::search_single_node()
  {
  if (ply == depth_limit) {
  value[ply] = G::unknown; 
  g.retract_move(); 
  ply--;
  }

  bool cutoff = false;

  while ((cutoff || !g.has_next_move()) && ply > 0) {
  g.retract_move(); ply--;

  switch (value[ply+1]) {
  case G::white_wins:
  case G::draw:
  case G::black_wins:
  if (g.wtm()) {
  value[ply] = max(value[ply], value[ply+1]);

  cutoff = value[ply] == G::white_wins;
  } else {
  value[ply] = min(value[ply], value[ply+1]);

  cutoff = value[ply] == G::black_wins;
  }
  break;
  }
  }

  if (g.has_next_move()) {
  g.make_move(g.get_next_move()); ply++; nds++;
  g.prepare_moves();

  value[ply] = g.game_state();

  if (value[ply] == G::in_progress) {
  if (g.wtm()) {
  value[ply] = G::minfty;
  } else {
  value[ply] = G::pinfty;
  }
  }
  }
  }

  template<class G> void PN<G>::search()
  {
  while (result() == G::in_progress) {
  search_single_node();
  }
  }*/

#endif /* __Perft__ */
