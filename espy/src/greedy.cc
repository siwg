/**
 * \file 
 *
 * main().
 *
 */

#include <iostream>
using namespace std;

#include "Settings.h"
#include "GreedyEngine.h"
#include "CPCommandParser.h"
#include "ConnectSix.h"

GreedyEngine<ConnectSix> engine;

int main(int argc, char **argv)
{
	engine.main();

	cout << "Bye!\n";

	return 0;
}
