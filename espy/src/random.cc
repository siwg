/**
 * \file 
 *
 * main().
 *
 */

/**
 *
 * \mainpage \c Espy Documentation
 *
 * \section Introduction
 *
 * 
 */

#include <iostream>
using namespace std;

#include "Settings.h"
#include "RandomEngine.h"
#include "CPCommandParser.h"
#include "Hex.h"

RandomEngine<ConnectSix> engine;

int main(int argc, char **argv)
{
	engine.main();

	cout << "Bye!\n";

	return 0;
}
