#include<pthread.h>
#include<iostream>
#include<string>
#include<stdlib.h>

#include "Engine.h"
#include "PN.h"

template <class G>
class PNEngine : public Engine<G> {

	static const int infty = 30000;

	void init() {
		Engine<G>::init();

		cout << "connect PN Engine " << Settings::version() << ", playing " << G::name() << endl;
	}

	void think(G board, typename Engine<G>::SearchResult &result) {
		/*
		 * 
		 */
		
		Log::debug("think() started");

		result.lock.grab();
		
		board.prepare_moves();
		vector<typename G::move> moves;

		while(board.has_next_move()) {
			moves.push_back(board.get_next_move());
		}

		int rnd = random() % moves.size();

		result.in_progress = false;
		result.resign = false;
		result.nominal_depth = 1;
		result.score_lower_bound = -infty;
		result.score_upper_bound = infty;
		result.pv.clear();
		result.pv.push_back(moves[rnd]);
		result.elapsed_time = 0;
		result.nodes_used = 1;

		result.lock.release();
		
		/*
		 * 
		 */

		PN<G> mm;
		mm.init();
		mm.position(board);
		
		while (!Engine<G>::pause_search) {
			
			while (mm.result() == G::in_progress && !Engine<G>::pause_search &&
				result.nodes_used < 1000000) {
				mm.search_single_node();
				result.nodes_used++;

				if ((result.nodes_used % 1) == 0 || mm.result() != G::in_progress) {
					cout << "  PN " << getpid() << " result " << mm.result() << " nodes " << result.nodes_used << endl;
				
					float best = 2000000000;

					result.lock.grab();
					//				board.print_board();
					
					for (vector<Node>::iterator i = mm.tree.children.begin(); i != mm.tree.children.end(); i++) {
						
						float pn = i -> white_moves_to_win;
						float dpn = i -> black_moves_to_win;
						
						if (board.wtm()) {
							if (dpn == 0) continue;
							if (pn/dpn < best) {
								best = pn/dpn;
								result.pv[0] = i -> move_that_got_us_here;
							}
						} else {
							if (pn == 0) continue;
							if (dpn/pn < best) {
								best = dpn/pn;
								result.pv[0] = i -> move_that_got_us_here;
							}
						}
					}		     
					result.lock.release();
				}
			}

			if (mm.result() == G::in_progress) {
				cout << "  PN " << getpid() << " result " << mm.result() << " nodes " << result.nodes_used << endl;
			}
		}	      

		Log::debug("think() ended");
	}

};
