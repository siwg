#ifndef __Reversi__
#define __Reversi__

#include "Game.h"
#include "Settings.h"

#include <algorithm>

#define BOARD_EDGE 6
#define BOARD_SIZE BOARD_EDGE * BOARD_EDGE

class Reversi : public Game {
public:
  static string name() {
    return reversi();\
  }
  
  const static int max_depth = Settings::maximum_game_length;
  //const static int BOARD_EDGE = 6;
  //const static int BOARD_SIZE = BOARD_EDGE * BOARD_EDGE;
  const static int MAX_MOVES_NUMBER = BOARD_SIZE - 4;
  
  const static int LIST_END = -1;

  enum FieldState { WHITE, BLACK, EMPTY };
  enum Player { WHITE_PLAYER, BLACK_PLAYER };
  enum Axis { VERTICAL, HORIZONTAL, DIAGONAL_INC, DIAGONAL_DEC };

  class Board;

  class MoveSortHeuristic {
  public:
    virtual void sortMoves(Board *, int *, int) = 0;
  };

  class CornerComparator{
  public:
    bool operator ()(const int m1,const int m2){
      int x1 = m1 % BOARD_EDGE;
      int y1 = m1 / BOARD_EDGE;
      int x2 = m2 % BOARD_EDGE;
      int y2 = m2 / BOARD_EDGE;
      int center = BOARD_EDGE / 2;

      return abs(x1 - center) + abs(y1 - center) < abs(x2 - center) + abs(y2 - center);
    }
  };

  class CornerHeuristic: public MoveSortHeuristic {
  public:
    void sortMoves(Board * board, int * legal, int n) {
      std::sort(legal, legal + n, CornerComparator());
    }
  };
  
  class Board {
    FieldState fields[BOARD_SIZE];
  public:
    Board() {
      clear();
    }
    
    void print(){
      for (int i = 0; i < BOARD_SIZE; ++i){
        if (i % BOARD_EDGE == 0)
          std::cout << std::endl;
        if (fields[i] == WHITE)
          std::cout << "O";
        if (fields[i] == BLACK)
          std::cout << "X";
        if (fields[i] == EMPTY)
          std::cout << " ";
      }
      std::cout << std::endl;
    }

    /*
     * Koszt liniowy ze wzgledu na dlugosc planszy (64).
     */
    void clear() {
      for (int i = 0; i < BOARD_SIZE; i++)
        fields[i] = EMPTY;
      int halfEdge = BOARD_EDGE / 2;
      fields[(halfEdge - 1) * BOARD_EDGE + halfEdge - 1] = fields[halfEdge * (BOARD_EDGE + 1)] = WHITE;
      fields[(halfEdge - 1) * BOARD_EDGE + halfEdge] = fields[halfEdge * (BOARD_EDGE + 1) - 1] = BLACK;
    }
      
    FieldState getState(int field) {
      return fields[field];
    }
    
    void setState(int field, FieldState state) {
      fields[field] = state;
    }
    
    void flipState(int field){
      if (fields[field] == WHITE)
          fields[field] = BLACK;
      else
          fields[field] = WHITE;
    }

    /*
     * Koszt liniowy ze wzgledu na dlugosc fieldsList.
     */
    void setState(int * fieldsList, FieldState state) {
      int i = 0;
      while (fieldsList[i] != LIST_END) {
        setState(fieldsList[i], state);
        ++i;
      }
    }
    
    /*
     * Koszt liniowy ze wzgledu na dlugosc toReverse
     * (na pewno nie wiecej niz 16 - w rzeczywistosci
     * pewnie istotnie mniej).
     */
    
    /* Zwraca stan nastepnego pola lezacego obok pola field w kierunku axis
     * i zapisuje jego indeks pod adresem nextField. */
    FieldState next(int field, Axis axis, int *nextField) {
      if (field >= BOARD_SIZE || field < 0)
          return EMPTY;
      switch (axis) {
        case HORIZONTAL:
          if (field % BOARD_EDGE < BOARD_EDGE - 1) {
            *nextField = field + 1;
            return fields[field + 1];
          } else
            return EMPTY;
        case VERTICAL:
          if (field / BOARD_EDGE < BOARD_EDGE - 1) {
            *nextField = field + BOARD_EDGE;
            return fields[field + BOARD_EDGE];
          } else
            return EMPTY;
        case DIAGONAL_DEC:
          if (field / BOARD_EDGE < BOARD_EDGE -1 && field % BOARD_EDGE < BOARD_EDGE - 1){
            *nextField = field + BOARD_EDGE + 1;
            return fields[field + BOARD_EDGE + 1];
          } else
            return EMPTY;
        case DIAGONAL_INC:
          if (field / BOARD_EDGE > 0 && field % BOARD_EDGE < BOARD_EDGE - 1) {
            *nextField = field - BOARD_EDGE + 1;
            return fields[field - BOARD_EDGE + 1];
          } else
            return EMPTY;
        default:
          return EMPTY;
      }
    }
    
    /* Zwraca stan poprzedniego pole lezacego obok pola field w kierunku axis
     * i zapisuje jego indeks pod adresem nextField. */
    FieldState prev(int field, Axis axis, int *nextField) {
      if (field >= BOARD_SIZE || field <= 0)
        return EMPTY;
      switch (axis) {
        case HORIZONTAL:
          if (field % BOARD_EDGE > 0){
            *nextField = field - 1;
            return fields[field - 1];
          } else
              return EMPTY;
        case VERTICAL:
          if (field / BOARD_EDGE > 0){
            *nextField = field - BOARD_EDGE;
            return fields[field - BOARD_EDGE];
          } else
            return EMPTY;
        case DIAGONAL_DEC:
          if (field / BOARD_EDGE > 0 && field % BOARD_EDGE > 0){
            *nextField = field - BOARD_EDGE - 1;
            return fields[field - BOARD_EDGE - 1];
          } else
            return EMPTY;
        case DIAGONAL_INC:
          if (field / BOARD_EDGE < BOARD_EDGE - 1 && field % BOARD_EDGE > 0){
            *nextField = field + BOARD_EDGE - 1;
            return fields[field + BOARD_EDGE - 1];
          } else
            return EMPTY;
        default:
          return EMPTY;
      }
    }
  }; //class Board

  /*
   * Dodaje do tablicy reversed pola przeznaczone do odwrocenia w wyniku ruchu m
   * w kierunku axis. Argument index wskazuje na pierwsze wolne pole tablicy reversed,
   * zwracany jest indeks pierwszego wolnego pola po zakonczeniu dzialania.
   * Koszt pesymistyczny to 16 przejsc petli.
   */
  int reverseAxis(move m, Axis axis, int * reversed, int index) {
    FieldState playerColor;
    (playerToMove[movesCount - 1] == WHITE_PLAYER) ?
      playerColor = WHITE : playerColor = BLACK;
    FieldState opponentColor;
    (playerToMove[movesCount - 1] == WHITE_PLAYER) ?
      opponentColor = BLACK : opponentColor = WHITE;

    int oldIndex;

    int mm = m;
    int nextField = 0;
    oldIndex = index;
    FieldState field = board.next(mm, axis, &nextField);
    while (field == opponentColor) {
      reversed[index++] = nextField;
      mm = nextField;
      field = board.next(mm, axis, &nextField);
    }
    if (field != playerColor)
      index = oldIndex;

    mm = m;
    oldIndex = index;
    field = board.prev(mm, axis, &nextField);
    while (field == opponentColor) {
      reversed[index++] = nextField;
      mm = nextField;
      field = board.prev(mm, axis, &nextField);
    }
    if (field != playerColor)
      index = oldIndex;

    return index;
  }
  
  /*
   * Sprawdza, czy ruch m powoduje zmiane
   * koloru pionkow przeciwnika na kierunku axis.
   * Koszt max 16.
   */
  bool checkAxis(move m, Axis axis) {
    FieldState playerColor;
    (playerToMove[movesCount] == WHITE_PLAYER) ?
      playerColor = WHITE : playerColor = BLACK;
    FieldState opponentColor;
    (playerToMove[movesCount] == WHITE_PLAYER) ?
      opponentColor = BLACK : opponentColor = WHITE;

    int count = 0;
    int mm = m;
    int nextField = 0;
    FieldState field = board.next(mm, axis, &nextField);
    while (field == opponentColor) {
      count++;
      mm = nextField;
      field = board.next(mm, axis, &nextField);
    }
    if (field == playerColor && count > 0)
      return true;

    mm = m;
    count = 0;
    field = board.prev(mm, axis, &nextField);
    while (field == opponentColor) {
      count++;
      mm = nextField;
      field = board.prev(mm, axis, &nextField);
    }
    if (field == playerColor && count > 0)
      return true;

    return false;
  }
  
  /*
   * Sprawdza, czy istnieje legalny ruch.
   * Koszt kwadratowy ze względu na długość planszy.
   */
  bool moveExists() {
    for (move i = 0; i < BOARD_SIZE; i++)
      if (checkMove(i)) return true;
    return false;
  }
  
  /*
   * Zmienia aktualnego gracza.
   */
  void flipPlayer() {
    (playerToMove[movesCount] == WHITE_PLAYER) ?
      playerToMove[movesCount] = BLACK_PLAYER : playerToMove[movesCount] = WHITE_PLAYER;
  }
  
  Player flipPlayer(Player p) {
    if (p == WHITE_PLAYER)
      return BLACK_PLAYER;
    else
      return WHITE_PLAYER;
  }

  /* Ustawia zwyciezce. */
  void declareWinner() {
    int black = 0;
    int white = 0;
    FieldState s;
    for (int i = 0; i < BOARD_SIZE; i++){
      s = board.getState(i);
      if (s == WHITE)
        ++white;
      else
        if (s == BLACK)
          ++black;
    }
    if (black != white){
      if (black > white)
        game_state(black_wins);
      else
        game_state(white_wins);
    } else
      game_state(draw);
  }
  
  // Pola klasy Reversi:

  int movesCount;
  
  int executedMove[BOARD_SIZE];
  
  int moves[BOARD_SIZE][BOARD_SIZE - 3];
  int movesIterator[BOARD_SIZE];

  bool prepared[BOARD_SIZE];
  
  int reversed[BOARD_SIZE][BOARD_SIZE / 2];
  
  Player playerToMove[BOARD_SIZE];
  Board board;
  MoveSortHeuristic * heuristic;
	
public:
  Reversi() {
    heuristic = 0;
    clear();
  }

  ~Reversi() {
//    if (heuristic != 0)
//      delete heuristic;
  }

  void setHeuristic(MoveSortHeuristic * msh){
    heuristic = msh;
  }

  /*
   * Koszt z grubsza pesymistycznie kwardatowy
   * ze wzgleduu na rozmiar planszy.
   */
  void prepare_moves() {
    if (!prepared[movesCount]) {
      int j = 0;
      movesIterator[movesCount] = 0;
      for (int i = 0; i < BOARD_SIZE; i++)
        if (checkMove(i))
          moves[movesCount][j++] = i;
      moves[movesCount][j] = LIST_END;
      if (heuristic != 0)
        heuristic->sortMoves(&board, moves[movesCount], j);
    }
    prepared[movesCount] = false;
  };

  bool has_next_move() {
    return (moves[movesCount][movesIterator[movesCount]] != LIST_END);
  }

  move get_next_move() {
    return moves[movesCount][movesIterator[movesCount]++];
  }

  /*
   * Koszt liniowy ze wzgledu na rozmiar planszy.
   */
  void clear() {
    movesCount = 0;
    executedMove[movesCount] = -1;
    playerToMove[movesCount] = WHITE_PLAYER;
    moves[movesCount][0] = LIST_END;
    movesIterator[movesCount] = 0;
    reversed[movesCount][0] = LIST_END;
    prepared[movesCount] = false;
    board.clear();
  }

  /*
   * Koszt liniowy ze wzgledu na rozmiar planszy.
   */
  void make_move(const move m) {

    game_state(in_progress);

    Player makingMove = playerToMove[movesCount];
    movesCount++;

    executedMove[movesCount] = m;


    moves[movesCount][0] = LIST_END;
    movesIterator[movesCount] = 0;

    int index = 0;
    index = reverseAxis(m, HORIZONTAL, reversed[movesCount], index);
    index = reverseAxis(m, VERTICAL, reversed[movesCount], index);
    index = reverseAxis(m, DIAGONAL_DEC, reversed[movesCount], index);
    index = reverseAxis(m, DIAGONAL_INC, reversed[movesCount], index);
    reversed[movesCount][index] = LIST_END;

    FieldState color;
    (makingMove == WHITE_PLAYER) ?
      color = WHITE : color = BLACK;
    board.setState(reversed[movesCount], color);
    board.setState(m, color);
    
    if (movesCount == MAX_MOVES_NUMBER){
      declareWinner();
      return;
    }

    playerToMove[movesCount] = makingMove;
    flipPlayer();
    /*
    * Skoro i tak muszę sprawdzić, czy jest ruch,
    * to dlaczego by od razu nie zebrać ruchów...
    */
    prepared[movesCount] = false;
    prepare_moves();
    if (has_next_move())
      prepared[movesCount] = true;
    else {
      /* Drugi gracz nie ma ruchu. Zostaje powtorzony ruch tego
         samego gracza lub gra zostaje zakonczona. */
      flipPlayer();
      prepare_moves();
      if (has_next_move())
        prepared[movesCount] = true;
      else
        declareWinner();
    }

    //board.print();
  }

  void retract_move() {
    board.setState(executedMove[movesCount], EMPTY);
    int i = 0;
    while (reversed[movesCount][i] != LIST_END)
      board.flipState(reversed[movesCount][i++]);
    --movesCount;
  }

  inline int wtm() const {
    return playerToMove[movesCount] == WHITE_PLAYER;
  }
  
    /*
   * Sprawdza, czy ruch m jest legalny (tj. czy powoduje zmiane
   * koloru pionkow przeciwnika).
   * Koszt liniowy ze wzgledu na dlugosc planszy.
   */
  bool checkMove(move m) {
    if (board.getState(m) != EMPTY)
      return false;
    return (checkAxis(m, HORIZONTAL) || checkAxis(m, VERTICAL) ||
      checkAxis(m, DIAGONAL_DEC) || checkAxis(m, DIAGONAL_INC));
  }
  
};

#endif
