import java.util.Map;


public class CPDefaultEventListener implements CPEventListener {
	
	public void connect(String name) {
		
	}
	
	public void bestMove(String move) {
		
	}
	
	public void error(String errorMessage) {
		
	}
	
	public void concede() {
		
	}
	
	public void info(Map<String, String> options) {
		
	}
	
	public void init() {
		
	}
	
	public void startGame(String position, Role role,
			int gameTime, int bonusTime) {
		System.out.println("startgame:");
		System.out.println("\tGame time: " + gameTime);
		System.out.println("\tBonus time: " + bonusTime);
	}
	
	public void move(Color color, String move) {
		
	}
	
	public void feedback(boolean on) {
		
	}
	
	public void setOption(Map<String, String> options) {
		
	}
	
	public void getMove(Color color, int moveTime, int gameTime) {
		System.out.println("getmove:");
		System.out.println("\tMove time: " + moveTime);
		System.out.println("\tGame time: " + gameTime);
	}
	
	public void forceMove() {
		
	}
	
	public void stop() {
		
	}
	
	public void endGame(Result result) {
		
	}
	
	public void quit() {
		
	}
}
