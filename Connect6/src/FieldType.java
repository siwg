
public enum FieldType {
	WHITE {
		@Override
		public String toString() {
			return "W";
		}
	},
	BLACK {
		@Override
		public String toString() {
			return "B";
		}
	},
	EMPTY {
		@Override
		public String toString() {
			return "E";
		}
	}
}
