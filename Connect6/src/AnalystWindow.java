import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.GridLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JButton;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTextArea;


public class AnalystWindow extends JPanel {

	private static final long serialVersionUID = -5918564589906124212L;
	private JTextArea jTextArea;
	private JScrollPane jScrollPane;
	private JButton wMove,
					bMove,
					fMove,
					sMove;
	private Mechanics mechanics;
	int a = 0;
	
	public AnalystWindow() {
		jTextArea = new JTextArea("Alamakota");
		wMove = new JButton("W");
		
		bMove = new JButton("B");
		fMove = new JButton("!");
		fMove.addActionListener(new ActionListener() {
			
			@Override
			public void actionPerformed(ActionEvent arg0) {
				mechanics.forceMove();
			}
		});
		sMove = new JButton("S");
		sMove.addActionListener(new ActionListener() {
			
			@Override
			public void actionPerformed(ActionEvent arg0) {
				mechanics.forceStop();
			}
		});
		setLayout(new BorderLayout());
		JPanel box = new JPanel();
		box.setLayout(new GridLayout(2, 2));
		jScrollPane= new JScrollPane(jTextArea);
		jScrollPane.setVerticalScrollBarPolicy(JScrollPane.VERTICAL_SCROLLBAR_ALWAYS);
		jScrollPane.setHorizontalScrollBarPolicy(JScrollPane.HORIZONTAL_SCROLLBAR_NEVER);
		jTextArea.setDisabledTextColor(Color.RED);
		jTextArea.setEnabled(false);
		add(jScrollPane, BorderLayout.CENTER);
		box.add(wMove);
		box.add(bMove);
		box.add(fMove);
		box.add(sMove);
		add(box, BorderLayout.EAST);
		jTextArea.setRows(4);
		
	}
	
	public void setMechanics(Mechanics mechanics) {
		this.mechanics = mechanics;
	}
	
	public void setAnalystText(String text) {
		jTextArea.append(text);
		jScrollPane.getVerticalScrollBar().setValue(jScrollPane
				.getVerticalScrollBar().getMaximum());

	}
	
}
