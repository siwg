import java.awt.BorderLayout;
import java.awt.Container;
import java.awt.Dimension;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JFrame;
import javax.swing.JMenu;
import javax.swing.JMenuBar;
import javax.swing.JMenuItem;
import javax.swing.JOptionPane;
import javax.swing.JPopupMenu;


public class GUI extends JFrame {

	private static final long serialVersionUID = 6411499808530678723L;
	private JMenuBar menuBar;
	private Mechanics mechanics;
	
	public GUI(String name) {
		super(name);
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setSize(new Dimension(800,600));
		createMenuBar();
		setJMenuBar(menuBar);
		JPopupMenu.setDefaultLightWeightPopupEnabled( false );
	}
	
	private void createMenuBar() {
		menuBar = new JMenuBar();
		JMenu menu1 = new JMenu("Game");
		JMenu menu2 = new JMenu("Mode");
		JMenu menu3 = new JMenu("Action");
		JMenu menu4 = new JMenu("Options");
		JMenu menu5 = new JMenu("Help");
		menuBar.add(menu1);
		menuBar.add(menu2);
		menuBar.add(menu3);
		menuBar.add(menu4);
		menuBar.add(menu5);
		
		JMenuItem item1 = new JMenuItem("Create Server");
		JMenuItem item2 = new JMenuItem("Join server");
		JMenuItem item3 = new JMenuItem("Disconnect");
		JMenuItem item4 = new JMenuItem("Close");
		item4.addActionListener(new ActionListener() {		
			@Override
			public void actionPerformed(ActionEvent arg0) {
				System.exit(0);
			}
		});
		menu1.add(item1);
		menu1.add(item2);
		menu1.add(item3);
		menu1.add(item4);
		
		JMenuItem item5 = new JMenuItem("Player vs player");
		item5.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent arg0) {
				Container game = new Container();
				Board board = new Board();
				ChessClockGui ccg = new ChessClockGui("Player1", "Player2", true);
				mechanics = new GameMechanics(ccg, board, true, true, null, null, 600000);
				//menuBar = new JMenuBar();
				
				game.setLayout(new BorderLayout());
				game.add(ccg, BorderLayout.NORTH);
				game.add(board, BorderLayout.CENTER);
				game.setSize(board.getSize());
				GUI.this.setContentPane(game);
				GUI.this.setSize(board.getSize());
				GUI.this.validate();
			}
		});
		JMenuItem item6 = new JMenu("Player vs cpu");
		JMenuItem item6_1 = new JMenuItem("Play as White");
		item6_1.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent arg0) {
				Container game = new Container();
				Board board = new Board();
				ChessClockGui ccg = new ChessClockGui("Player1", "CPU2", true);
				String s = (String)JOptionPane.showInputDialog(
		                board,
		                "Program path:",
		                "Customized Dialog",
		                JOptionPane.PLAIN_MESSAGE,
		                null,
		                null, "");
				if (s == null || s.equals("")) {
					return;
				}
				mechanics = new GameMechanics(ccg, board, true, false, null, s, 600000);
				//menuBar = new JMenuBar();
				
				game.setLayout(new BorderLayout());
				game.add(ccg, BorderLayout.NORTH);
				game.add(board, BorderLayout.CENTER);
				game.setSize(board.getSize());
				GUI.this.setContentPane(game);
				GUI.this.setSize(board.getSize());
				GUI.this.validate();
			}
		});
		JMenuItem item6_2 = new JMenuItem("Play as Black");
		item6_2.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent arg0) {
				Container game = new Container();
				Board board = new Board();
				ChessClockGui ccg = new ChessClockGui("CPU1", "Player2", true);
				String s = (String)JOptionPane.showInputDialog(
		                board,
		                "Program path:",
		                "Customized Dialog",
		                JOptionPane.PLAIN_MESSAGE,
		                null,
		                null, "");
				if (s == null || s.equals("")) {
					return;
				}
				mechanics = new GameMechanics(ccg, board, false, true, s, null, 600000);
				//menuBar = new JMenuBar();
				
				game.setLayout(new BorderLayout());
				game.add(ccg, BorderLayout.NORTH);
				game.add(board, BorderLayout.CENTER);
				game.setSize(board.getSize());
				GUI.this.setContentPane(game);
				GUI.this.setSize(board.getSize());
				GUI.this.validate();
			}
		});
		JMenuItem item7 = new JMenuItem("Cpu vs cpu");
		item7.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent arg0) {
				Container game = new Container();
				Board board = new Board();
				ChessClockGui ccg = new ChessClockGui("CPU1", "CPU2", true);
				String s1 = (String)JOptionPane.showInputDialog(
		                board,
		                "Program path:",
		                "Customized Dialog",
		                JOptionPane.PLAIN_MESSAGE,
		                null,
		                null, "");
				if (s1 == null || s1.equals("")) {
					return;
				}
				String s2 = (String)JOptionPane.showInputDialog(
		                board,
		                "Program path:",
		                "Customized Dialog",
		                JOptionPane.PLAIN_MESSAGE,
		                null,
		                null, "");
				if (s2 == null || s2.equals("")) {
					return;
				}
				mechanics = new GameMechanics(ccg, board, false, false, s1, s2, 600000);
				//menuBar = new JMenuBar();
				
				game.setLayout(new BorderLayout());
				game.add(ccg, BorderLayout.NORTH);
				game.add(board, BorderLayout.CENTER);
				game.setSize(board.getSize());
				GUI.this.setContentPane(game);
				GUI.this.setSize(board.getSize());
				GUI.this.validate();
			}
		});
		//JMenuItem item8 = new JMenuItem("Editing board");
		JMenuItem item14 = new JMenuItem("Analyze");
		item14.addActionListener(new ActionListener() {
			
			@Override
			public void actionPerformed(ActionEvent arg0) {
				Container analyst = new Container();
				Board board = new Board();
				AnalystWindow aw = new AnalystWindow();
				mechanics = new AnalystMechanics(board, aw);
				
				analyst.setLayout(new BorderLayout());
				analyst.add(aw, BorderLayout.NORTH);
				analyst.add(board, BorderLayout.CENTER);
				analyst.setSize(board.getSize());
				GUI.this.setContentPane(analyst);
				GUI.this.setSize(board.getSize());
				aw.setSize(board.getSize().width, 100);
				aw.setMaximumSize(aw.getSize());
				GUI.this.validate();
				//GUI.this.setComponentZOrder(menuBar, -2);
			}
		});
		menu2.add(item5);
		menu2.add(item6);
		menu2.add(item7);
		//menu2.add(item8);
		menu2.add(item14);
		
		item6.add(item6_1);
		item6.add(item6_2);
		
		JMenuItem item9 = new JMenuItem("Surrender");
		item9.addActionListener(new ActionListener() {
			
			@Override
			public void actionPerformed(ActionEvent arg0) {
				if (mechanics != null) {
					mechanics.surrenderCalled();
				}
			}
		});
		JMenuItem item13 = new JMenuItem("Force move");
		item13.addActionListener(new  ActionListener() {
			
			@Override
			public void actionPerformed(ActionEvent e) {
				if (mechanics != null) {
					mechanics.forceMove();
				}
			}
		});
		menu3.add(item9);
		menu3.add(item13);
		
		JMenuItem item10 = new JMenuItem("Whatever1");
		JMenuItem item11 = new JMenuItem("Whatever2");
		menu4.add(item10);
		menu4.add(item11);
		
		JMenuItem item12 = new JMenuItem("About");
		item12.addActionListener(new ActionListener() {
			
			@Override
			public void actionPerformed(ActionEvent arg0) {
				JOptionPane.showMessageDialog(GUI.this, "It's ugly GUI v0.2!");
			}
		});
		menu5.add(item12);	
	}

}
