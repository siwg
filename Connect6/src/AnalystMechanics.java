import java.awt.Point;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import java.io.IOException;
import java.security.acl.Owner;
import java.util.Map;

import javax.swing.ButtonGroup;
import javax.swing.JMenuItem;
import javax.swing.JPopupMenu;
import javax.swing.JRadioButtonMenuItem;


public class AnalystMechanics implements Mechanics {

	private Board board;
	private AnalystWindow analystWindow;
	private FieldType fieldType;
	private JPopupMenu popupMenu;
	private PlayerWrapper program;
	
	public AnalystMechanics(Board board, AnalystWindow analystWindow) {
		this.board = board;
		this.analystWindow = analystWindow;
		fieldType = FieldType.WHITE;
		
		analystWindow.setMechanics(this);
		popupMenu = new JPopupMenu();
		ButtonGroup radioGroup = new ButtonGroup();
		final JRadioButtonMenuItem menuItem1 = new JRadioButtonMenuItem("White");
		menuItem1.setSelected(true);
		menuItem1.addActionListener(new ActionListener() {
			
			@Override
			public void actionPerformed(ActionEvent e) {
				fieldType = FieldType.WHITE;
				//menuItem1.setSelected(true);
			}
		});
		final JRadioButtonMenuItem menuItem2 = new JRadioButtonMenuItem("Black");
		menuItem2.addActionListener(new ActionListener() {
			
			@Override
			public void actionPerformed(ActionEvent e) {
				fieldType = FieldType.BLACK;
				//menuItem2.setSelected(false);
			}
		});
		JRadioButtonMenuItem menuItem3 = new JRadioButtonMenuItem("Empty");
		menuItem3.addActionListener(new ActionListener() {
			
			@Override
			public void actionPerformed(ActionEvent e) {
				fieldType = FieldType.EMPTY;
			}
		});
		
		JMenuItem menuItem4 = new JMenuItem("Clear all");
		menuItem4.addActionListener(new ActionListener() {
			
			@Override
			public void actionPerformed(ActionEvent arg0) {
				AnalystMechanics.this.board.clear();
				AnalystMechanics.this.board.repaint();
			}
		});
		
		popupMenu.add(menuItem1);
		popupMenu.add(menuItem2);
		popupMenu.add(menuItem3);
		popupMenu.addSeparator();
		popupMenu.add(menuItem4);
		
		radioGroup.add(menuItem1);
		radioGroup.add(menuItem2);
		radioGroup.add(menuItem3);
		
		board.addMouseListener(new MouseListener() {
			
			
			@Override
			public void mouseReleased(MouseEvent arg0) {}
			
			@Override
			public void mousePressed(MouseEvent arg0) {}
			
			@Override
			public void mouseExited(MouseEvent arg0) {}
			
			@Override
			public void mouseEntered(MouseEvent arg0) {}
			
			@Override
			public void mouseClicked(MouseEvent arg0) {
				//System.out.println("Click");
				if (arg0.getButton() == MouseEvent.BUTTON1) {
					Point p = AnalystMechanics.this.board
						.getFieldPosition(arg0.getX(), arg0.getY());
					if (p != null) {
						AnalystMechanics.this.board.setField(fieldType, p.x, p.y);
						AnalystMechanics.this.board.repaint();
					}
				} else if (arg0.getButton() == MouseEvent.BUTTON3) {
					popupMenu.show(arg0.getComponent(), arg0.getX(), arg0.getY());
				}
			}
		});
	}

	@Override
	public void handleConnect(PlayerWrapper pw, String name) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void handleConcade(PlayerWrapper pw) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void handleBestMove(PlayerWrapper pw, String move) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void handleError(PlayerWrapper pw, String errorMessage) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void handleInfo(PlayerWrapper pw, Map<String, String> options) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void forceMove() {
		if (program != null)
			try {
				program.sendForceMove();
			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
	}

	@Override
	public void surrenderCalled() {
		//do nothing
	}

	@Override
	public void forceStop() {
		try {
			program.sendStop();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
}
