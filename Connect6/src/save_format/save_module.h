// System zapisu plików wersja 2 kwietnia
#include<cstdio>
#include<iostream>
#include<fstream>
#include<algorithm>
#include<cstring>
#include<vector>

#define WHITE 1
#define BLACK -1
#define NEXT 0 // Gdy następny gracz jest po prostu zgodnie z ruchami

#define ERASE true
#define APPEND false

typedef std::pair<int, int> PI;

// Pojedynczy ruch
class Vertex{
    public:
    Vertex(Vertex* parent, std::pair<PI, PI> field, int player = 0):
    parent(parent), move(field), player(player) {}

    Vertex() : parent(NULL), player(-1) {}
    int player;
    std::pair<PI, PI> move;
    std::string comment;
    Vertex* parent;
    std::vector<Vertex*> children;

    void rm_child(Vertex* child);
};

// Rozgrywka
class Game_tree{
    public:
    Game_tree(){}
    void add_move(Vertex* parent_move, std::pair<PI, PI> new_move, int player = NEXT);
    void rm_move(Vertex* removed);
    void change_move(Vertex* changed);

    void read_from_file(const char * file_path);
    void write_to_file(const char * file_path);

    void set_comment(const string comm, bool erase){
        if(erase == ERASE){
            comment = comm;
        }else{
            comment += (" "+comm);
        }
    }

    void set_player(const string name, int player){
        if(player == WHITE)player1 = name;
        if(player == BLACK)player2 = name;
    }

    Vertex* get_root();
    int get_size(Vertex* v); // Ilość synów
    std::vector<Vertex*> get_children(Vertex* v); // Pobranie wektora wszystkich synów
    Vertex* get_child(Vertex* v, int n); // Pobranie konkretnego syna

    private:
    std::string player1;
    std::string player2;
    std::string comment;
    Vertex root;
};
