// System zapisu plików wersja 2 kwietnia
#include<cstdio>
#include<iostream>
#include<fstream>
#include<algorithm>
#include<cstring>
#include<vector>
#include<stack>
#include "save_module.h"
using namespace std;

// Póki co zamienia usuwany element z ostatnim
// i usuwa ostatni - jeżeli to sprawi problemy
// to oczywiście to zmienię
void Vertex::rm_child(Vertex* child){
    int index;
    for(index = 0; index < children.size(); index++){
        if(children[index] == child)break;
    }
    swap(children[index], children[children.size()-1]);
    children.pop_back();
}

void Game_tree::add_move(Vertex* parent_move, pair<PI, PI> new_move, int player){
    parent_move->children.push_back(new Vertex(parent_move, new_move, player));
}

void Game_tree::rm_move(Vertex* removed){
    Vertex* child;
    for(vector<Vertex*>::iterator it = removed->children.begin();
    it != removed->children.end(); ++it){
        rm_move(*it);
    }

    if(removed != this->get_root()){
        removed->parent->rm_child(removed);
    }
}

void Game_tree::read_from_file(char* file_path){
    ifstream ifile;
    string line;
    bool read_game = false, read_comment = false;

    stack<Vertex*> in_progress;
    stack<int> chd_remain;
    int pos, movelen;
    Vertex* active = NULL, new_vertex = NULL;
    pair<PI, PI> new_move;

    while(!ifile.eof()){
        ifile >> line;
        if(line[0] == '{'){
            switch(line[1]){
                case 'W':
                    read_game = false;
                    read_comment = false;
                    set_player(line.substr(3), WHITE);
                    break;
                case 'B':
                    read_game = false;
                    read_comment = false;
                    set_player(line.substr(3), BLACK);
                    break;
                case 'C':
                    read_game = false;
                    read_comment = true;
                    set_comment(line.substr(3), ERASE);
                    break;
                case 'G':
                    read_game = true;
                    read_comment = false;
                    break;
            }
        }else{
            // W zależności od ustawienia booli
            // dodanie komentarza lub budowanie drzewa
            if(read_comment == true){
                set_comment(line, APPEND);
            }
            if(read_game == true){
                //TODO budowanie drzewa
                pos = 0;
                while((pos += movelen + 1) < line.length()){
                    movelen = 0;
                    while(line[pos+movelen] != ']')movelen++;
                    // Przeanalizuj nowy ruch

                }
            }
        }
    }
    ifile.open(file_path, ios::in);
    ififle.close();
}

void Game_tree::write_to_file(char* file_path){
    ofstream ofile;
    ofile.open(file_path, ios::out);
    ofile << "{W}" << player1 << endl; 
    ofile << "{B}" << player2 << endl;
    ofile << "{C}" << comment << endl;
    ofile << "{G}" << endl;

    int vnum = 0;
    stack<Vertex*> dfs_stack;
    Vertex* active;
    dfs_stack.push(this->get_root());

    while(dfs_stack.empty() == false){
        active = dfs_stack.top();

        if(active->children.size() == child_index.top()){
            child_index.pop();
            dfs_stack.pop();
        }


        // Jeśli pierwsze odwiedzenie, wypisz zawartość
        ofile << "["
              << active->move.first.first << ":"
              << active->move.first.second << ":"
              << active->move.second.first << ":"
              << active->move.second.second << ";"
              << children.size() << "]";
        if(active->comment != ""){
            ofile << "\"" << active->comment << "\"";
        }
        vnum++;
        if(vnum%10 == 0) ofile << endl;
        // Ilość synów

        //TODO odwraca kolejność
        for(vector<Vertex*>::iterator it=active->children.begin(); 
        it!=active->children.end();++it)
            dfs_stack.push(*it);
    }

    ofile.close();
}

Vertex* Game_tree::get_root(){
    return &root;
}

int Game_tree::get_size(Vertex* v){
    return v->children.size();
}

vector<Vertex*> Game_tree::get_children(Vertex* v){
    return vector<Vertex*>(v->children);
}
Vertex* get_child(Vertex* v, int n){
    return v->children[n];
}
