import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.net.Socket;
import java.net.URL;
import java.net.URLConnection;
import java.util.Map;
import Parser.CPCommandParser;
import Parser.CPEventListener;
import Parser.CorruptedCommandException;


public class PlayerWrapper {
	
	public static final int MOVE = 1;
	public static final int INFO = 2;
	
	private Process p;
	private InputStream in;
	private OutputStream out;
	private Thread t;
	private Mechanics mechanics;
	private String programName;
	
	public PlayerWrapper (Mechanics _mechanics, String programPath) throws IOException {
		programName = "unknown";
		mechanics = _mechanics;
		String[] command = {programPath};
		ProcessBuilder pb;
		
		if(programPath.toUpperCase().startsWith("HTTP://"))
		{
			String host = programPath.substring(7);
			String[] list = host.split(":");
			Socket echoSocket = new Socket(list[0], Integer.parseInt(list[list.length-1]));
			in = echoSocket.getInputStream();
			out = echoSocket.getOutputStream();
		}
		else
		{
			pb = new ProcessBuilder(command);
			p = pb.start();
			in = p.getInputStream();
			out = p.getOutputStream();	
		}
		
		t = new Thread(new Runnable() {
			
			@Override
			public void run() {
				CPCommandParser parser = new CPCommandParser(in, new CPEventListener() {
					
					//region GUI
					@Override
					public void connect(String name) {
						programName = name;
						mechanics.handleConnect(PlayerWrapper.this, name);
					}
					
					@Override
					public void concede() {
						mechanics.handleConcade(PlayerWrapper.this);		
					}
					
					@Override
					public void bestMove(String move) {
						mechanics.handleBestMove(PlayerWrapper.this, move);
					}
					
					@Override
					public void error(String errorMessage) {
						mechanics.handleError(PlayerWrapper.this, errorMessage);
					}
					
					@Override
					public void info(Map<String, String> options) {
						mechanics.handleInfo(PlayerWrapper.this, options);
					}
					//endregion GUI
					//region engine					
					@Override
					public void stop() {
						
					}
					
					@Override
					public void startGame(String position, Role role, int gameTime,
							int bonusTime) {}
					
					@Override
					public void setOption(Map<String, String> options) {}
					
					@Override
					public void quit() {}
					
					@Override
					public void move(Color color, String move) {}
					
					@Override
					public void init() {}
					
					@Override
					public void getMove(Color color, int moveTime, int gameTime) {}
					
					@Override
					public void forceMove() {}
					
					@Override
					public void feedback(boolean on) {}

					@Override
					public void endGame(Result result) {}
					//endregion engine
				});
				while (true) {
					try {
						String commandLine = parser.nextCommand();
						if (commandLine == null) throw new IOException();
						try {
							parser.parse(commandLine);
							System.out.println(programName + " -> GUI: " + commandLine);
						} catch (CorruptedCommandException e) {
							System.err.println("Command not parsed: " + commandLine);
						}
					} catch (IOException e) {
						break;
					}
				}
			}
		});
		t.start();
	}
	
	private void send(String txt) throws IOException {
		System.out.print("GUI -> " + programName + ": " + txt);
		out.write(txt.getBytes());
		out.flush();
	}

	public void shutDown() {
		try {
			send("quit\n");
			//send("x\n");
		} catch (IOException e) {
			e.printStackTrace();
		}
		t.stop();
	}

	public void sendInit() throws IOException {
		send("init\n");
	}

	public void sendStarGame(Board board, int player, int gameTime) throws IOException {
		String s = "analyst";
		if (player == Consts.WHITE)
			s = "white";
		if (player == Consts.BLACK)
			s = "black";
		send("startgame " + board.toString() + " "+ s + " 0:" + gameTime + "\n");
	}

	public void sendGetMove(int player, long moveTime, long gameTime) throws IOException {
		String s = "white";
		if (player == Consts.BLACK)
			s = "black";
		send("getmove " + s + " " + moveTime + " " + gameTime + "\n");	
	}

	public void sendForceMove() throws IOException {
		send("forcemove\n");
	}
	
	public void sendMove(int player, String move) throws IOException {
		String s = "white";
		if (player == Consts.BLACK)
			s = "black";
		send("move " + s + " " + move + "\n");
	}
	
	public void sendFeedback(boolean on) throws IOException {
		send("feedback " + (on ? "on" : "off") + "\n");
		
	}
	
	public void sendEndGame(int winner) throws IOException {
		String s = "draw";
		if (winner == Consts.WHITEWON)
			s = "white";
		if (winner == Consts.BLACKWON)
			s = "black";
		send("endgame " + s + "\n");

	}

	public void sendStop() throws IOException {
		send("stop\n");
	}
}
