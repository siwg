package Parser;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.Map;
import java.util.Scanner;
import java.util.TreeMap;

import Parser.CPEventListener.Color;
import Parser.CPEventListener.Result;
import Parser.CPEventListener.Role;


public class CPCommandParser {
	
	/**
	 * Nazwy komunikatow.
	 */
	public static String CN_CONNECT = "connect";
	public static String CN_BESTMOVE = "bestmove";
	public static String CN_ERROR = "error";
	public static String CN_CONCEDE = "concede";
	public static String CN_INFO = "info";
	public static String CN_INIT = "init";
	public static String CN_STARTGAME = "startgame";
	public static String CN_MOVE = "move";
	public static String CN_FEEDBACK = "feedback";
	public static String CN_SETOPTION = "setoption";
	public static String CN_GETMOVE = "getmove";
	public static String CN_FORCEMOVE = "forcemove";
	public static String CN_STOP = "stop";
	public static String CN_ENDGAME = "endgame";
	public static String CN_QUIT = "quit";
	
	public static String UNKNOWN_LABEL = "unknown";
	public static String INFINITY_LABEL = "inf";
	
	public static String ROLE_WHITE = "white";
	public static String ROLE_BLACK = "black";
	public static String ROLE_ANALYST = "analyst";
	
	public static String COLOR_WHITE = "white";
	public static String COLOR_BLACK = "black";
	
	public static String RESULT_WHITE = "white";
	public static String RESULT_BLACK = "black";
	public static String RESULT_DRAW = "draw";
	
	public static String FEEDBACK_ON = "on";
	public static String FEEDBACK_OFF = "off";
	
	/**
	 * Strumien wejsciowy.
	 */
	private BufferedReader reader;
	private Scanner scanner;
	private CPEventListener listener;
	
	/**
	 * Konstruktor CPCommandParser.
	 * @param inputStream
	 */
	public CPCommandParser(InputStream inputStream,
			CPEventListener listener) {
		this.reader = new BufferedReader(new InputStreamReader(inputStream));
		this.listener = listener;
	}
	
	public String nextCommand() throws IOException {
		return reader.readLine();
	}
	
	private int timeStringToInt(String timeStr) {
		if (INFINITY_LABEL.equals(timeStr))
			return -1;
		else
			return Integer.valueOf(timeStr).intValue();
	}
	
	private void commandConnect() {
		String name = UNKNOWN_LABEL;
		if (scanner.hasNext())
			name = scanner.next();
		listener.connect(name);
	}
	
	private void commandBestMove() throws CorruptedCommandException {
		if (scanner.hasNext()) {
			String move = scanner.next();
			listener.bestMove(move);
		}
		else
			throw new CorruptedCommandException();
	}
	
	private void commandError() {
		String message = UNKNOWN_LABEL;
		if (scanner.hasNext())
			message = scanner.next();
		listener.error(message);
	}
	
	private void commandConcede() {
		listener.concede();
	}
	
	private void commandInfo() {
		Map<String, String> options = new TreeMap<String, String>();
		while (scanner.hasNext()) {
			String name = scanner.next();
			if (scanner.hasNext()) {
				String value = scanner.next();
				options.put(name, value);
			}
		}
		listener.info(options);
	}
	
	private void commandInit() {
		listener.init();
	}
	
	private void commandStartGame() throws CorruptedCommandException {
		if (!scanner.hasNext())
			throw new CorruptedCommandException();
		
		// Position
		String position = scanner.next();
		
		if (!scanner.hasNext())
			throw new CorruptedCommandException();

		// Role
		String roleStr = scanner.next();
		Role role;
		if (ROLE_ANALYST.equals(roleStr)) role = Role.ANALYST;
		else if (ROLE_WHITE.equals(roleStr)) role = Role.WHITE;
		else if (ROLE_BLACK.equals(roleStr)) role = Role.BLACK;
		else throw new CorruptedCommandException();
		
		if (!scanner.hasNext())
			throw new CorruptedCommandException();
		
		// Time
		String timeString = scanner.next();
		int gameTime = timeStringToInt(timeString.replaceAll(":.*", ""));
		int bonusTime = timeStringToInt(timeString.replaceAll(".*:", ""));
		
		listener.startGame(position, role, gameTime, bonusTime);
	}
	
	private void commandMove() throws CorruptedCommandException {
		if (!scanner.hasNext())
			throw new CorruptedCommandException();
		
		// Color
		String colorStr = scanner.next();
		Color color;
		if (COLOR_BLACK.equals(colorStr)) color = Color.BLACK;
		else if (COLOR_WHITE.equals(colorStr)) color = Color.WHITE;
		else throw new CorruptedCommandException();
		
		if (!scanner.hasNext())
			throw new CorruptedCommandException();
		
		// Move
		String move = scanner.next();
		
		listener.move(color, move);
	}
	
	private void commandFeedback() throws CorruptedCommandException {
		if (!scanner.hasNext())
			throw new CorruptedCommandException();
		
		// On/off
		String turn = scanner.next();
		boolean on;
		if (FEEDBACK_ON.equals(turn)) on = true;
		else if (FEEDBACK_OFF.equals(turn)) on = false;
		else throw new CorruptedCommandException();
		
		listener.feedback(on);
	}
	
	private void commandSetOption() {
		Map<String, String> options = new TreeMap<String, String>();
		while (scanner.hasNext()) {
			String name = scanner.next();
			if (scanner.hasNext()) {
				String value = scanner.next();
				options.put(name, value);
			}
		}
		listener.setOption(options);
	}
	
	private void commandGetMove() throws CorruptedCommandException {
		if (!scanner.hasNext())
			throw new CorruptedCommandException();
		
		// Color
		String colorStr = scanner.next();
		Color color;
		if (COLOR_BLACK.equals(colorStr)) color = Color.BLACK;
		else if (COLOR_WHITE.equals(colorStr)) color = Color.WHITE;
		else throw new CorruptedCommandException();
		
		if (!scanner.hasNext())
			throw new CorruptedCommandException();
		
		// Move time
		String moveTimeStr = scanner.next();
		int moveTime = timeStringToInt(moveTimeStr);
		
		if (!scanner.hasNext())
			throw new CorruptedCommandException();
		
		// Game time
		String gameTimeStr = scanner.next();
		int gameTime = timeStringToInt(gameTimeStr);
		
		listener.getMove(color, moveTime, gameTime);
	}
	
	private void commandForceMove() {
		listener.forceMove();
	}
	
	private void commandStop() {
		listener.stop();
	}
	
	private void commandEndGame() throws CorruptedCommandException {
		if (!scanner.hasNext())
			throw new CorruptedCommandException();
		
		// Result
		String resultStr = scanner.next();
		Result result;
		if (RESULT_BLACK.equals(resultStr)) result = Result.BLACK;
		else if (RESULT_DRAW.equals(resultStr)) result = Result.DRAW;
		else if (RESULT_WHITE.equals(resultStr)) result = Result.WHITE;
		else throw new CorruptedCommandException();
		
		listener.endGame(result);
	}
	
	private void commandQuit() {
		listener.quit();
	}
	
	private void parseCommand(String commandName) throws CorruptedCommandException {
		if (CN_CONNECT.equals(commandName)) commandConnect();
		else if (CN_BESTMOVE.equals(commandName)) commandBestMove();
		else if (CN_ERROR.equals(commandName)) commandError();
		else if (CN_CONCEDE.equals(commandName)) commandConcede();
		else if (CN_INFO.equals(commandName)) commandInfo();
		else if (CN_INIT.equals(commandName)) commandInit();
		else if (CN_STARTGAME.equals(commandName)) commandStartGame();
		else if (CN_MOVE.equals(commandName)) commandMove();
		else if (CN_FEEDBACK.equals(commandName)) commandFeedback();
		else if (CN_SETOPTION.equals(commandName)) commandSetOption();
		else if (CN_GETMOVE.equals(commandName)) commandGetMove();
		else if (CN_FORCEMOVE.equals(commandName)) commandForceMove();
		else if (CN_STOP.equals(commandName)) commandStop();
		else if (CN_ENDGAME.equals(commandName)) commandEndGame();
		else if (CN_QUIT.equals(commandName)) commandQuit();
		else
			throw new CorruptedCommandException();
	}
	
	public void parse(String commandLine) throws CorruptedCommandException {
		scanner = new Scanner(commandLine);
		if (scanner.hasNext()) {
			String commandName = scanner.next();
			parseCommand(commandName);
		}
		else
			throw new CorruptedCommandException();
	}
	
	/*public static void main(String [] args) {
		CPDefaultEventListener listener = new CPDefaultEventListener();
		CPCommandParser parser = new CPCommandParser(System.in, listener);
		while (true) {
			try {
				String commandLine = parser.nextCommand();
				if (commandLine == null) throw new IOException();
				try {
					parser.parse(commandLine);
					System.out.println("parsed.");
				} catch (CorruptedCommandException e) {
					System.out.println("not parsed.");
				}
			} catch (IOException e) {
				break;
			}
		}
	}*/
	
}
