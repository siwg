package Parser;
import java.util.Map;


public interface CPEventListener {
	
	public enum Color { WHITE, BLACK };
	public enum Role { WHITE, BLACK, ANALYST };
	public enum Result { WHITE, BLACK, DRAW };
	
	/**
	 * Gracz laczy sie z GUI i przedstawia sie.
	 * @param name	nazwa gracza
	 */
	public void connect(String name);
	
	/**
	 * Gracz wysyla informacje o wybranym ruchu.
	 * @param move	opis ruchu
	 */
	public void bestMove(String move);
	
	/**
	 * Gracz powiadamia GUI o wystapieniu bledu.
	 * @param errorMessage	tresc komunikatu o bledzie
	 */
	public void error(String errorMessage);
	
	/**
	 * Gracz poddaje gre.
	 */
	public void concede();
	
	/**
	 * Gracz (silnik) wysyla informacje statystyczne dotyczace
	 * przebiegu myslenia i procesu wybierania ruchu.
	 * 
	 * Takze: wysylanie dowolnych informacji (np. tych powyzej) z GUI do graczy.
	 * 
	 * @param options
	 */
	public void info(Map<String, String> options);
	
	/**
	 * GUI wysy�a komunikat rozpczynajacy komunikacje z graczem.
	 */
	public void init();
	
	/**
	 * GUI wysy�a do gracza powiadomienie o rozpoczeciu nowej gry.
	 * @param position	pozycja poczatkowa nowej gry
	 * @param role		rola, w jakiej wystepowac ma gracz
	 * @param gameTime	limit czasu na cala rozgrywke
	 * @param bonusTime czas doliczany po kazdym ruchu
	 */
	public void startGame(String position, Role role,
			int gameTime, int bonusTime);
	
	/**
	 * GUI wysy�a powiadomienie o wykonaniu ruchu przez jednego z graczy.
	 * @param color	kolor ruszajacego sie gracza
	 * @param move  opis ruchu
	 */
	public void move(Color color, String move);
	
	/**
	 * GUI prosi o wlaczenie badz wylaczenie przesylania informacji
	 * od silnika (patrz zdarzenie "info").
	 * @param on
	 */
	public void feedback(boolean on);
	
	/**
	 * GUI wymusza u gracza ustawienie pewnych opcji silnika.
	 * @param options
	 */
	public void setOption(Map<String, String> options);
	
	/**
	 * GUI prosi gracza o wykonanie ruchu danego koloru w okreslonym czasie.
	 * @param color		kolor gracza wykonujacego ruch
	 * @param moveTime	czas na ruch
	 * @param gameTime	czas do konca rozgrywki
	 */
	public void getMove(Color color, int moveTime, int gameTime);
	
	/**
	 * GUI nakazuje graczowi bezzwlocznie dac odpowiedz na kolejny ruch.
	 */
	public void forceMove();
	
	/**
	 * GUI nakazuje przerwac wyszukiwanie ruchow.
	 */
	public void stop();
	
	/**
	 * GUI wysyla informacje o zakonczeniu gry z okreslonym wynikiem.
	 * @param result	wynik gry
	 */
	public void endGame(Result result);
	
	/**
	 * GUI wysyla polecenie zakonczenia gry.
	 */
	public void quit();
}
