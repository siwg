import java.awt.Color;
import java.awt.Font;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.Image;
import java.awt.Paint;
import java.awt.Point;
import java.awt.RadialGradientPaint;
import java.awt.Rectangle;
import java.awt.TexturePaint;
import java.awt.geom.Point2D;
import java.awt.geom.Rectangle2D;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;

import javax.imageio.ImageIO;
import javax.swing.JPanel;

public class Board extends JPanel {

	private static final long serialVersionUID = -4892917806894699643L;
	private static final int rows = 19;
	private static final int fieldSize = 30;
	private static final int width = (rows-1)*fieldSize + 100;
	private static final int height = (rows-1)*fieldSize + 100;
	private static final int fieldEpsilon = fieldSize/2;
	private static final int backgroundColor = 0x005000;
	private static final int fontColor = 0xFFFA96;
	private static final int boardColor = 0xD2691E;
	private static final int dotJump = 6;
	private boolean endGame;
	private int winMode;
	private Image img;
	int player = 1;
	FieldType[][] fields;
	
	public Board() {
		super();
		setSize(width, height);
		setMaximumSize(getSize());
		setMinimumSize(getSize());
		fields = new FieldType[rows][rows];
		endGame = false;
		clear();
	}
	
	
	@Override
	public void paint(Graphics g) {
		if (img != null) {
			Image tmp = createImage(width, height);
			Graphics g2 = tmp.getGraphics();
			g2.drawImage(img, 0, 0, this);
			for (int a=0; a < rows; ++a)
				for (int b = 0; b < rows; ++b) {
					drawPiece(g2, fields[a][b], a, b);
				}
			if (endGame) {
				g2.setFont(new Font ("Serif", Font.PLAIN, 60));
				//g2.setColor(Color.magenta);
				//g2.fillRect(width/4, height/4, width/2, height/4);
				//g2.setColor(Color.cyan);
				if (winMode == Consts.WHITEWON) {
					//g2.setColor(Color.BLACK);
					//g2.fillRect(width/4, height/4, width/2, height/4);
					g2.setColor(Color.WHITE);
					g2.drawString("White Wins", width/2 - 140, height*13/32);
				} else if (winMode == Consts.BLACKWON) {
					//g2.setColor(Color.WHITE);
					//g2.fillRect(width/4, height/4, width/2, height/4);
					g2.setColor(Color.BLACK);
					g2.drawString("Black Wins", width/2 - 140, height*13/32);
				} else {
					//g2.setColor(Color.darkGray);
					//g2.fillRect(width/4, height/4, width/2, height/4);
					g2.setColor(Color.gray);
					g2.drawString("Draw", width/2 - 20, height*13/32);
				}
			}
			
			g.drawImage(tmp, 0, 0, this);
		}
		else
			drawBoard();
	}
	
	public void drawBoard() {
		img = createImage(width, height);
		Graphics g = img.getGraphics();
		//region background
		g.setColor(new Color(backgroundColor));
		g.fillRect(0, 0, width, height);
		//endregion background
		//region gameboard
		BufferedImage mImage = null;
		Paint paint = new Color(boardColor);
		try {
			mImage = ImageIO.read(new File("wood2.jpg"));
		} catch (IOException e) {
			mImage = null;
			e.printStackTrace();
		}
		if (mImage != null) {
			Rectangle2D tr = new Rectangle2D.Double(0, 0,
			        mImage.getWidth(), mImage.getHeight());
			paint = new TexturePaint(mImage, tr);
		} 
		Graphics2D g2 = (Graphics2D)g;
		g2.setPaint(paint);
		g2.fill(new Rectangle(50, 50, width-100, height-100));
		
		//region linie
		g.setColor(Color.black);
		for (int a=0; a<rows; ++a) {
			g.drawLine(50+ a*fieldSize, 50, 50+ a*fieldSize, height-50);
			g.drawLine(50, 50+ a*fieldSize, width-50, 50+ a*fieldSize);
		}
		for (int a=(((rows+1)/2)-1)%dotJump; a < rows; a += dotJump )
			for (int b=(((rows+1)/2)-1)%dotJump; b < rows; b += dotJump )
				g.fillRect(50+ a*fieldSize - 5,
							50+ b*fieldSize - 5,
							10,
							10);
		//endregion linie
		//endregion gameboard
		//region desc
		if (rows < 27) {
			Character c = 'A';
			g2.setFont(new Font ("Serif", Font.PLAIN, 20));
			g2.setColor(new Color(fontColor));
			for (int a = 0; a < rows; ++a) {
				g2.drawString(c.toString(), 20, 55+ a*fieldSize);
				++c;
			}
			for (int a = 0; a < rows; ++a) {
				g2.drawString(Integer.toString(a+1), 50-5+ a*fieldSize, height - 20);
			}
		}
		//endregion desc
	}
	
	private void drawPiece(Graphics g, FieldType ft, int x, int y) {
		Graphics2D g2 = (Graphics2D) g;
		switch(ft) {
			case BLACK :
				Paint p = new RadialGradientPaint((Point2D)new Point(50-(fieldEpsilon/4) + x*fieldSize,
						50-(fieldEpsilon/4) + y*fieldSize), (float) fieldEpsilon,
						new float[]{0.1f, 0.99f},
						new Color[]{new Color(125, 125, 125), new Color(0, 0, 0)});
				g2.setPaint(p);
				break;
			case WHITE :
				Paint p2 = new RadialGradientPaint((Point2D)new Point(50-(fieldEpsilon/4) + x*fieldSize,
						50-(fieldEpsilon/4) + y*fieldSize), (float) fieldEpsilon,
						new float[]{0.1f, 0.99f},
						new Color[]{new Color(255, 255, 255), new Color(150, 150, 150)});
				g2.setPaint(p2);
				break;
			case EMPTY :
				return;
		}
		g2.fillOval(50 + x*fieldSize-fieldEpsilon,
								50 + y*fieldSize-fieldEpsilon,
								2*fieldEpsilon, 2*fieldEpsilon);
		//repaint();
	}
	

	public void clear() {
		for (int a=0; a < rows; ++a)
			for (int b = 0; b < rows; ++b)
				fields[a][b] = FieldType.EMPTY;
	}
	
	protected Point getFieldPosition(int x, int y) {
		for (int a=0; a < rows; ++a)
			for (int b=0; b < rows; ++b)
				if (x > 50 + a*fieldSize - fieldEpsilon 
						&& x < 50 + a*fieldSize + fieldEpsilon )
					if (y > 50 + b*fieldSize - fieldEpsilon 
							&& y < 50 + b*fieldSize + fieldEpsilon)
						return new Point(a, b);
		return null;
	}
	
	public FieldType getFieldType(int x, int y) {
		return fields[x][y];
	}
	
	public void setField(FieldType ft, int x, int y) {
		fields[x][y] = ft;
	}


	public void showResult(int mode) {
		endGame = true;
		winMode = mode;
		repaint();
	}
	
	@Override
	public String toString() {
		String s = "{";
		for (int y = 0; y  < rows; ++y) {
			for (int x = 0; x < rows; ++x)
				if (x == rows-1)
					s += getFieldType(x, y).toString();
				else
					s += getFieldType(x, y).toString() + ",";
			if (y == rows-1)
				s += "}";
			else
				s += "|";
		}
		return s;
	}
}
