import java.awt.Point;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Map;



public class GameMechanics implements Mechanics {

	private MyChessTimer timer1, timer2;
	private ChessClockGui chessClockGui;
	private Board board;
	private boolean player; 
	private int move;
	private boolean isPlayer1Human;
	private boolean isPlayer2Human;
	private boolean gameEnd;
	private PlayerWrapper pw1;
	private PlayerWrapper pw2;
	private Thread t;
	private ArrayList<int[]> moves;
	
	public GameMechanics(ChessClockGui ccg, Board b, boolean _isPlayer1Human,
			boolean _isPlayer2Human, String program1, String program2, final int gameTime) {
		chessClockGui = ccg;
		board = b;
		player = true;
		moves = new ArrayList<int[]>();
		move = 0;
		this.isPlayer1Human = _isPlayer1Human;
		this.isPlayer2Human = _isPlayer2Human;
		//makeMove("c4b6");
		if (!isPlayer1Human)
			try {
				pw1 = new PlayerWrapper(this, program1);
				pw1.sendInit();
				pw1.sendStarGame(board, Consts.WHITE, gameTime);
			} catch (IOException e1) {
				e1.printStackTrace();
				return;
			}
			
		if (!isPlayer2Human)
			try {
				pw2 = new PlayerWrapper(this, program2);
				pw2.sendInit();
				pw2.sendStarGame(board, Consts.BLACK, gameTime);
			} catch (IOException e1) {
				e1.printStackTrace();
				return;
			}
		board.addMouseListener(new MouseListener() {
			
			@Override
			public void mouseReleased(MouseEvent e) {}
			
			@Override
			public void mousePressed(MouseEvent e) {}
			
			@Override
			public void mouseExited(MouseEvent e) {}
			
			@Override
			public void mouseEntered(MouseEvent e) {}
			
			@Override
			public void mouseClicked(MouseEvent e) {
				clickControl(e.getX(), e.getY());
			}
		});
		timer1 = new MyChessTimer(gameTime, new ActionListener() {		
			@Override
			public void actionPerformed(ActionEvent e) {
				chessClockGui.setTimer(long2timeString(timer1.getTimeLeft()), true);
				if (timer1.getTimeLeft() <= 0)
					callEndGame(Consts.BLACKWON);
			}
		});
		timer2 = new MyChessTimer(gameTime, new ActionListener() {		
			@Override
			public void actionPerformed(ActionEvent e) {
				chessClockGui.setTimer(long2timeString(timer2.getTimeLeft()), false);
				if (timer2.getTimeLeft() <= 0)
					callEndGame(Consts.WHITEWON);
			}
		});
		chessClockGui.setTimer(long2timeString(timer1.getTimeLeft()), true);
		chessClockGui.setTimer(long2timeString(timer2.getTimeLeft()), false);

		t = new Thread(new Runnable() {		
			@Override
			public void run() {
				synchronized (GameMechanics.this) {
					timer1.start();
					while (!gameEnd) {
						if (player) {
							if (isPlayer1Human) {
								if (move != 0)
									try {
										GameMechanics.this.wait();
									} catch (InterruptedException e) {}
								if (!gameEnd)
									try {
										GameMechanics.this.wait();
									} catch (InterruptedException e) {}
							} else {
								try {
									pw1.sendGetMove(Consts.WHITE, (timer1.getTimeLeft()/1000),
											(timer1.getTimeLeft()/1000));
								} catch (IOException e) {
									e.printStackTrace();
								}
								try {
									GameMechanics.this.wait();
								} catch (InterruptedException e) {
									e.printStackTrace();
								}
							} 
							
						} else {
							if (isPlayer2Human) {
								if (move != 0)
									try {
										GameMechanics.this.wait();
									} catch (InterruptedException e) {}
								if (!gameEnd)
									try {
										GameMechanics.this.wait();
									} catch (InterruptedException e) {}
							} else {
								try {
									pw2.sendGetMove(Consts.BLACK, (timer2.getTimeLeft()/1000),
											(timer2.getTimeLeft()/1000));
								} catch (IOException e) {
									e.printStackTrace();
								}
								try {
									GameMechanics.this.wait();
								} catch (InterruptedException e) {
									e.printStackTrace();
								}
							} 
						}	
						if (!gameEnd) {
							player = !player;
							chessClockGui.setPlayer(player);
							if (player) {
								timer2.stop();
								if (!isPlayer1Human)
									try {
										pw1.sendMove(Consts.BLACK, moves2String());
									} catch (IOException e) {
										e.printStackTrace();
									}
								checkGameEnd();
								if (gameEnd)
									return;
								timer1.start();
							} else {
								timer1.stop();
								if (!isPlayer2Human)
									try {
										pw2.sendMove(Consts.WHITE, moves2String());
									} catch (IOException e) {
										e.printStackTrace();
									}
								checkGameEnd();
								if (gameEnd)
									return;
								timer2.start();
							}
						}
					}
				}
			}
		});
		t.start();
	}
	
	private String moves2String() {
		String s = "";
		if (move > 1) {
			int [] tmp = moves.get(move-2);
			s += ((char)('a' + tmp[1])) + Integer.toString(tmp[0]);
		}
		int [] tmp = moves.get(move-1);
		s += ((char)('a' + tmp[1])) + Integer.toString(tmp[0]);
		return s;
	}
	
	private boolean makeMove(int x, int y) {
		if (gameEnd)
			return false;
		if (board.getFieldType(x, y) == FieldType.EMPTY) {
			if (player)
				board.setField(FieldType.WHITE, x, y);
			else 
				board.setField(FieldType.BLACK, x, y);
			++move;
			moves.add(new int[]{x,y});
			board.repaint();
			return true;
		}
		return false;
	}
	
	private void clickControl(int x, int y) {
		if (player) {
			if (!isPlayer1Human)
				return;
		} else {
			if (!isPlayer2Human)
				return;
		}
		Point p = board.getFieldPosition(x, y);
		if (p != null) {
			if(makeMove((int)p.getX(), (int)p.getY())) {
				synchronized (GameMechanics.this) {
					GameMechanics.this.notifyAll();
				}
			}
		}
	}
	
	private void callEndGame(int mode) {
		gameEnd = true;
		board.showResult(mode);
		if (!isPlayer1Human)
			try {
				pw1.sendEndGame(mode);
			} catch (IOException e1) {
				e1.printStackTrace();
			}
		if (!isPlayer2Human)
			try {
				pw2.sendEndGame(mode);
			} catch (IOException e1) {
				e1.printStackTrace();
			}
		t.interrupt();
		try {
			t.join();
		} catch (InterruptedException e) {
			e.printStackTrace();
		}
		timer1.stop();
		timer2.stop();
		if (pw1 != null)
			pw1.shutDown();
		if (pw2 != null)
			pw2.shutDown();
	}
	
	private static String long2timeString(long time) {
		time = time/100;
		long decsec = time % 10;
		time = time/10;
		long sec = time % 60;
		long minutes = time/60;
		return Long.toString(minutes) + ":" + ((sec<10) ? "0" : "") 
		+ Long.toString(sec) + "." + Long.toString(decsec);	
	}



	public void surrenderCalled() {
		if (!gameEnd) {
			if (isPlayer1Human && isPlayer2Human) {
				callEndGame(player ? Consts.BLACKWON : Consts.WHITEWON);
			} else {
				if (isPlayer1Human) {
					callEndGame(Consts.BLACKWON);
				} else if (isPlayer2Human) {
					callEndGame(Consts.WHITEWON);
				}
			}
		}
	}

	public void forceMove() {
		if (gameEnd)
			return;
		if (player) {
			if (pw1 != null)
				try {
					pw1.sendForceMove();
				} catch (IOException e) {
					e.printStackTrace();
				}
		} else {
			if (pw2 != null)
				try {
					pw2.sendForceMove();
				} catch (IOException e) {
					e.printStackTrace();
				}
		}
	}

	@Override
	public void handleConnect(PlayerWrapper pw, String name) {
		if (pw == pw1)
			chessClockGui.setPlayer1Name(name);
		if (pw == pw2)
			chessClockGui.setPlayer2Name(name);
	}

	@Override
	public void handleConcade(PlayerWrapper pw) {
		if (pw == pw1)
			callEndGame(Consts.BLACKWON);
		if (pw == pw2)
			callEndGame(Consts.WHITEWON);
	}

	@Override
	public void handleBestMove(PlayerWrapper pw, String move) {
		synchronized (GameMechanics.this) {
			String [] s = move.split("\\d");
			String [] t = move.split("\\D");
			int tmp = 1;
			for (int a = 0; a < s.length; ++a)
				if (!s[a].equals("")) {
					int y = Integer.parseInt(t[tmp]);
					char c = s[a].charAt(0);
					int x = ((int)(c - 'a'));

					System.out.println("x="+y+"y="+x);
					makeMove(y, x);
					++tmp;
				}
			GameMechanics.this.notify();
		}
	}

	@Override
	public void handleError(PlayerWrapper pw, String errorMessage) {
		
	}

	@Override
	public void handleInfo(PlayerWrapper pw, Map<String, String> options) {
		
	}

	@Override
	public void forceStop() {
		
	}

	private void checkGameEnd() {
		int empty = 0;
		int white = 0, black = 0;
		for (int x = 0; x < 19; ++x) {
			white = 0; black = 0;
			for (int y = 0; y < 19; ++y) {
				switch (board.getFieldType(x, y)) {
				case WHITE:
					++white;
					black = 0;
					break;
				case BLACK:
					++black;
					white = 0;
					break;
				case EMPTY:
					white = 0;
					black = 0;
					++empty;
					break;
				default:
					break;
				}
				if (black > 5) {
					callEndGame(Consts.BLACKWON);
					return;
				}
				if (white > 5) {
					callEndGame(Consts.WHITEWON);
					return;
				}
				
			}
			
		}
		
		for (int y = 0; y < 19; ++y) {
			white = 0; black = 0;
			for (int x = 0; x < 19; ++x) {
				switch (board.getFieldType(x, y)) {
				case WHITE:
					++white;
					black = 0;
					break;
				case BLACK:
					++black;
					white = 0;
					break;
				case EMPTY:
					white = 0;
					black = 0;
					++empty;
					break;
				default:
					break;
				}
				if (black > 5) {
					callEndGame(Consts.BLACKWON);
					return;
				}
				if (white > 5) {
					callEndGame(Consts.WHITEWON);
					return;
				}
				
			}
			
		}
		
		for (int x = 0; x < 19; ++x) {
			white = 0; black = 0;
			for (int y = 0; (x+y) < 19; ++y) {
				switch (board.getFieldType((x+y), y)) {
				case WHITE:
					++white;
					black = 0;
					break;
				case BLACK:
					++black;
					white = 0;
					break;
				case EMPTY:
					white = 0;
					black = 0;
					++empty;
					break;
				default:
					break;
				}
				if (black > 5) {
					callEndGame(Consts.BLACKWON);
					return;
				}
				if (white > 5) {
					callEndGame(Consts.WHITEWON);
					return;
				}
				
			}
			
		}
		
		for (int x = 0; x < 19; ++x) {
			white = 0; black = 0;
			for (int y = 0; y < 19 && (x-y) > -1; ++y) {
				switch (board.getFieldType((x-y), y)) {
				case WHITE:
					++white;
					black = 0;
					break;
				case BLACK:
					++black;
					white = 0;
					break;
				case EMPTY:
					white = 0;
					black = 0;
					++empty;
					break;
				default:
					break;
				}
				if (black > 5) {
					callEndGame(Consts.BLACKWON);
					return;
				}
				if (white > 5) {
					callEndGame(Consts.WHITEWON);
					return;
				}
				
			}
			
		}
		if (empty == 0)
			callEndGame(Consts.DRAW);
	}

	
}
	

