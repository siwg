import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Font;

import javax.swing.Box;
import javax.swing.JLabel;
import javax.swing.JPanel;


public class ChessClockGui extends JPanel{

	private static final long serialVersionUID = -2113891898341895500L;

	private JLabel name1;
	private JLabel name2;
	private JLabel timer1;
	private JLabel timer2;
	private JPanel p1 = new JPanel();
	private JPanel p2 = new JPanel();
	
	public ChessClockGui(String name1, String name2, boolean firstStarts) {
		this.setLayout(new BorderLayout(0, 0));
		Box box = Box.createHorizontalBox();
		this.name1 = new JLabel(name1);
		this.name2 = new JLabel(name2);
		this.timer1 = new JLabel();
		this.timer2 = new JLabel();
		this.name1.setFont(new Font ("Serif", Font.PLAIN, 36));
		this.name1.setBackground(Color.BLACK);
		this.name1.setForeground(Color.WHITE);
		this.name2.setFont(new Font ("Serif", Font.PLAIN, 36));
		this.name2.setBackground(Color.WHITE);
		this.name2.setForeground(Color.BLACK);
		this.timer1.setFont(new Font ("Serif", Font.PLAIN, 36));
		this.timer1.setBackground(Color.BLACK);
		this.timer1.setForeground(Color.WHITE);
		this.timer2.setFont(new Font ("Serif", Font.PLAIN, 36));
		this.timer2.setBackground(Color.WHITE);
		this.timer2.setForeground(Color.BLACK);
		p1 = new JPanel();
		p1.setBackground(Color.BLACK);
		p2 = new JPanel();
		p2.setBackground(Color.WHITE);
		Box box1 = Box.createHorizontalBox();
		box1.add(this.name1);
		box1.add(Box.createHorizontalStrut(25));
		box1.add(timer1);
		p1.add(box1);
		Box box2 = Box.createHorizontalBox();
		box2.add(this.name2);
		box2.add(Box.createHorizontalStrut(25));
		box2.add(timer2);
		p2.add(box2);
		box.add(p1);
		box.add(p2);	
		add(box, BorderLayout.CENTER);
		setPlayer(firstStarts);
	}
	
	public void setPlayer(boolean firstPlayer) {
		
	}
	
	public void setTimer(String time, boolean firstPlayer) {
		if (firstPlayer)
			timer1.setText(time);
		else
			timer2.setText(time);
		timer1.repaint();
	}

	public void setPlayer1Name(String name) {
		name1.setText(name);
		name1.repaint();
	}
	
	public void setPlayer2Name(String name) {
		name2.setText(name);
		name2.repaint();
	}
	
}
