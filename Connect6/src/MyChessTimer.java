import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.Timer;


public class MyChessTimer {

	private long innerTime;
	private Timer timer;
	
	public MyChessTimer(long time, ActionListener al) {
		innerTime = time;
		timer = new Timer(100, al);
		timer.setRepeats(true);
		timer.addActionListener(new ActionListener() {
			
			@Override
			public void actionPerformed(ActionEvent arg0) {
				innerTime-=100;
			}
		});
	}
	
	public void stop() {
		timer.stop();
	}
	
	public void start() {
		timer.start();
	}
	
	public long getTimeLeft() {
		return innerTime;
	}
	
}
