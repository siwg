import java.util.Map;


public interface Mechanics {

	void forceMove();

	void surrenderCalled();

	void handleConnect(PlayerWrapper pw, String name);

	void handleConcade(PlayerWrapper pw);

	void handleBestMove(PlayerWrapper pw, String move);

	void handleError(PlayerWrapper pw, String errorMessage);

	void handleInfo(PlayerWrapper pw, Map<String, String> options);

	void forceStop();

}
