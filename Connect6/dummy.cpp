#include <stdio.h>
 
int main() {
	char c;
	while (true) {
		scanf("%c", &c);
		printf("%c", c);
		fflush(stdout);
		if (c == 'x')
			break;
	}
	printf("I'm closed!\n");
	return 0;
}
