import java.util.ArrayList;

public class Arbiter {
	Tournament tournament;
	
	Arbiter(ArrayList<String> a) {
		tournament = new RoundRobinTournament(a);
	}
	
	public String play() {
		ResultsHandler resultsHandler = new ResultsHandler(tournament);
		while (!tournament.isFinished()) {
			Pair p = tournament.getNextPair();
			// TODO: switch from .isFinished() to null as "end of tournament" marker?
//			if (p != null) {
				new Thread(new Match(p, resultsHandler)).start();
//			}
		}
		return tournament.getWinner();
	}
}
