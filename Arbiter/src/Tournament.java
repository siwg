public interface Tournament {
	/*
	 * Returns the pair of programs that should
	 * play the next match. Might block thread until
	 * next pair is not determined.
	 */
	public Pair getNextPair();
	
	/*
	 * Saves the result of a match.
	 */
	public void setResult(Pair pair, MatchResult r);
	
	/*
	 * Checks whether it is known that there will be
	 * no more matches.
	 */
	public boolean isFinished();
	
	/*
	 * Returns winning program.
	 */
	public String getWinner();
}	
