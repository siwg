
public enum PlayerType {
	WHITE {
		public String toString() {
			return "white";
		}
	},
	BLACK {
		public String toString() {
			return "black";
		}
	},
	ANALYST {
		public String toString() {
			return "analyst";
		}
	},
}
