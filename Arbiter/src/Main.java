import java.util.ArrayList;
import java.util.Arrays;

public class Main {

	/**
	 * @param args
	 */
	public static void main(String[] args) {
		// TODO Auto-generated method stub
		Arbiter arbiter = new Arbiter(new ArrayList<String>(Arrays.asList(args)));
		arbiter.play();
	}

}
