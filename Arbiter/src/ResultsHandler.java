
public class ResultsHandler {
	private Tournament tournament;
	
	public ResultsHandler(Tournament tournament) {
		this.tournament = tournament;
	}
	
	/*
	 * Saves the result of given match.
	 */
	public void saveResult(Pair pair, MatchResult result) {
		// save the results internally
		tournament.setResult(pair, result);
	}
	
	/*
	 * Generates the tournament report taking into account all
	 * saved results until the moment of generation.
	 */
	public void generateReport() {
		
	}
}
