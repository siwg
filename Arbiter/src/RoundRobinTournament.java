import java.util.ArrayList;
import java.util.LinkedList;
import java.util.ListIterator;
import java.util.concurrent.LinkedBlockingQueue;

public class RoundRobinTournament implements Tournament {
	int number_of_players;
	int round;
	ArrayList<String> players;
	String firstPlayer;
	LinkedList<String> blackPlayers;
	LinkedList<String> whitePlayers;
//	Queue<Pair> pairQueue;
	LinkedBlockingQueue<Pair> pairQueue;
	String winner;
	
	int [][]data;

	RoundRobinTournament(ArrayList<String> players) {
		this.blackPlayers = new LinkedList<String>();
		this.whitePlayers = new LinkedList<String>();
		this.players = players;
		firstPlayer = players.get(0);

		number_of_players = players.size();
		data = new int[number_of_players][number_of_players];
		if (number_of_players % 2 == 1) {
			players.add("BYE");
			number_of_players++;
		}
		
		for (int i = 1; i < players.size()/2; i++) {
			this.whitePlayers.addLast(players.get(number_of_players - i));
			this.blackPlayers.addLast(players.get(i));
		}
		this.whitePlayers.addLast(players.get(number_of_players/2));
		int round = 0;

		pairQueue = new LinkedBlockingQueue<Pair>();
		try {
			pairQueue.put(new Pair("./sucker1", "./sucker2"));
		} catch (InterruptedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	public ArrayList<Pair> getPairs() {
		round++;
		ArrayList<Pair> res =  new ArrayList<Pair>();
		ListIterator<String> blackIt = blackPlayers.listIterator(0);
		ListIterator<String> whiteIt = whitePlayers.listIterator(0);
		if (round % 2 == 1) {
			res.add(new Pair(firstPlayer, whiteIt.next()));
		}
		else {
			res.add(new Pair(whiteIt.next(), firstPlayer));
		}
		while (blackIt.hasNext() && whiteIt.hasNext()) {
			res.add(new Pair(blackIt.next(), whiteIt.next()));
		}
		blackPlayers.addFirst(whitePlayers.pollFirst());
		whitePlayers.addLast(blackPlayers.pollLast());
	//	printPairs(res);
		return res;
	}
	public void setResults(ArrayList<Match> results) {
		for(Match m : results) {
			int i1 = players.indexOf(m.pair.black);	
			int i2 = players.indexOf(m.pair.white);

			data[i1][i2] = m.result;
			data[i2][i1] = 1 - m.result < 0 ? 2 : 1 - m.result;	
		}
		return;
	}
	public String getResults() {
		return "togo";
	}
	public boolean isFinished() {
		return round == number_of_players - 1;
	}
	private void printPairs(ArrayList<Pair> r) {
		System.out.println("Round: " + round);
		for(Pair p : r) {
			System.out.println(p.black+" "+ p.white);
		}
		System.out.println("");
	}

	@Override
	public Pair getNextPair() {
		try {
			return pairQueue.take();
		} catch (InterruptedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return null;
	}

	@Override
	public void setResult(Pair pair, MatchResult r) {
		// Enqueue new pairs or set winner based on match result
//		pairQueue.put(e);
		synchronized (winner) {
			winner = "abc";
		}
	}

	@Override
	public String getWinner() {
		synchronized (winner) {
			while (winner == null) {
				try {
					winner.wait();
				} catch (InterruptedException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
			}
			return winner;
		}
	}
}
