import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.util.concurrent.LinkedBlockingQueue;

public class PlayerWrapper {

	public static final int MOVE = 1;
	public static final int INFO = 2;

	private String path;
	private Process p;
	private BufferedReader in;
	private OutputStream out;
	private Thread t;
	private LinkedBlockingQueue<String> moveQueue;

	public PlayerWrapper (String programPath) throws IOException {
		path = programPath;
		moveQueue = new LinkedBlockingQueue<String>();
		String[] command = {programPath};
		ProcessBuilder pb = new ProcessBuilder(command);
		p = pb.start();
		in = new BufferedReader(new InputStreamReader(p.getInputStream()));
		out = p.getOutputStream();	
		t = new Thread(new Runnable() {

			@Override
			public void run() {
				String s;
				while(true) {
					try {
						s = in.readLine();
						if (s == null) {
							break;
						}
						else {
							System.out.println(path + " says: " + s);
							moveQueue.put(s);
						}
					} catch (IOException e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
					} catch (InterruptedException e) {
						
					}
				}
			}
		});
		t.start();
	}

	public String readLine(int MODE) {
		if (MODE == MOVE) {
			try {
				return moveQueue.take();
			} catch (InterruptedException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
		return "";
	}

	public void shutDown() {
		try {
			send("quit\n");
			send("x\n");
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		t.stop();
	}

	public void sendInit() throws IOException {
		send("init\n");
	}

	public void sendStartGame(String board, PlayerType player, int gameTime) throws IOException {
		send(String.format("startgame %s %s %s\n", board, player, gameTime));
	}

	public void sendMove(PlayerType player, String move) throws IOException {
		send(String.format("move %s %s\n", player, move));
	}

	public void sendFeedback(boolean on) throws IOException {
		send(String.format("feedback %s\n", (on ? "on" : "off")));
	}

	public void sendGetMove(PlayerType player, long moveTime, long gameTime) throws IOException {
		send(String.format("getmove %s %s %s\n", player, moveTime, gameTime));
	}

	public void sendForceMove() throws IOException {
		send("forcemove\n");
	}

	public void sendEndGame(MatchResult result) throws IOException {
		send(String.format("endgame %s\n", result));
	}

	private void send(String txt) throws IOException {
		System.out.println(String.format("Sending \"%s\" to %s", txt, path));
		out.write(txt.getBytes());
		out.flush();
	}
}
