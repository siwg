public class Match implements Runnable {
	public Pair pair;
	private PlayerWrapper blackWrapper, whiteWrapper;
	int result; /* 1 - p1 won, 0 - p2 won, 2 - draw */
	History history;
	private ResultsHandler resultsHandler;

	Match(Pair pair, ResultsHandler resultsHandler) {
		this.pair = pair;
		this.resultsHandler = resultsHandler;
	}

	@Override
	public void run() {
		try {
			blackWrapper = new PlayerWrapper(pair.black);
			blackWrapper.sendInit();
			blackWrapper.sendStartGame("0", PlayerType.BLACK, 123);
			
			whiteWrapper = new PlayerWrapper(pair.white);
			whiteWrapper.sendInit();
			whiteWrapper.sendStartGame("0", PlayerType.WHITE, 123);
			
			String response;
			
			while (true) {
				whiteWrapper.sendGetMove(PlayerType.WHITE, 123, 123);
				response = whiteWrapper.readLine(1);
				
				blackWrapper.sendMove(PlayerType.WHITE, response.split(" ")[1]);
				blackWrapper.sendGetMove(PlayerType.BLACK, 123, 123);
				response = blackWrapper.readLine(1);
				
				whiteWrapper.sendMove(PlayerType.BLACK, response.split(" ")[1]);
				
				// break when game over
				break;
			}
			
			resultsHandler.saveResult(pair, MatchResult.WHITE_WIN);
		}
		catch (Exception e) {
			System.out.print(e.getStackTrace());
		}
	}
}
