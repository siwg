
public enum MatchResult {
	WHITE_WIN {
		public String toString() {
			return "white";
		}
	},
	BLACK_WIN {
		public String toString() {
			return "black";
		}
	},
	DRAW {
		public String toString() {
			return "draw";
		}
	}
}
