import java.awt.Point;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import java.io.IOException;
import java.util.ArrayList;



public class GameMechanics {

	private boolean player; 
	private int move;
	private boolean gameEnd;
	private PlayerWrapper pw1;
	private PlayerWrapper pw2;
	private Thread t;
	private ArrayList<int[]> moves;
	private int gameTime;
	
	public GameMechanics(String program1, String program2, final int gameTime) {

		this.gameTime = gameTime;
		player = true;
		moves = new ArrayList<int[]>();
		move = 0;
//		
//			try {
//				pw1 = new PlayerWrapper(program1);
//				pw1.sendInit();
////				pw1.sendStartGame("0", Consts.WHITE, gameTime);
//			} catch (IOException e1) {
//				// TODO Auto-generated catch block
//				e1.printStackTrace();
//				return;
//			}
//			
//			try {
//				pw2 = new PlayerWrapper(program2);
//				pw2.sendInit();
//				//TODO:board 
////				pw2.sendStartGame("0", Consts.BLACK, gameTime);
//				//System.out.println(pw2.readLine(0));
//			} catch (IOException e1) {
//				// TODO Auto-generated catch block
//				e1.printStackTrace();
//				return;
//			}
//		t = new Thread(new Runnable() {		
//			@Override
//			public void run() {
//				synchronized (GameMechanics.this) {
//					while (!gameEnd) {
//						if (player) {
//							try {
////								pw1.sendGetMove(Consts.WHITE, gameTime, gameTime);
//							} catch (IOException e) {
//								e.printStackTrace();
//							}
//								//getmoveorsmth
//							if (move == 0)
//								++move;
//							else
//								move += 2;
//						} 
//							
//						else {
//							try {
////								pw2.sendGetMove(Consts.BLACK, gameTime, gameTime);
//								} 
//							catch (IOException e) {
//								e.printStackTrace();
//							}
//							String s = pw2.readLine(PlayerWrapper.MOVE);
//							/*
//							makeMove(1, 1);
//							makeMove(1, 2);
//							*/
//							if (move ==0)
//								++move;
//							else
//								move+=2;
//							 
//						}	
//					
//					}
//				}
//			}
//		});
//		t.start();
	}
	
	
	private void callEndGame(int mode) {
		gameEnd = true;
//		pw1.sendEndGame(mode);
//		pw2.sendEndGame(mode);
		t.interrupt();
		try {
			t.join();
			//System.out.println("He join my crew");
		} catch (InterruptedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		if (pw1 != null)
			pw1.shutDown();
		if (pw2 != null)
			pw2.shutDown();
	}
	
	private static String long2timeString(long time) {
		time = time/100;
		long decsec = time % 10;
		time = time/10;
		long sec = time % 60;
		long minutes = time/60;
		return Long.toString(minutes) + ":" + ((sec<10) ? "0" : "") 
		+ Long.toString(sec) + "." + Long.toString(decsec);	
	}

	public void forceMove() {
		if (gameEnd)
			return;
		if (player) {
			if (pw1 != null)
				try {
					pw1.sendForceMove();
				} catch (IOException e) {
					e.printStackTrace();
				}
		} else {
			if (pw2 != null)
				try {
					pw2.sendForceMove();
				} catch (IOException e) {
					e.printStackTrace();
				}
		}
	}
	
}
	

